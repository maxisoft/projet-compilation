package memory;

import javafx.collections.FXCollections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.EmptyStackException;

/**
 * This test cover the ObservableStack class.
 *
 * @author maxime
 */
public class ObservableStackTest {
    private ObservableStack<Object> stack;

    @Before
    public void setUp() throws Exception {
        stack = new ObservableStack<>();
    }

    @Test
    public void testEmpty() throws Exception {
        Assert.assertTrue(stack.isEmpty());
    }

    @Test
    public void testPop() throws Exception {
        final Object o1 = new Object();
        final Object o2 = new Object();
        stack.add(o2);
        stack.add(o1);
        Assert.assertEquals(o1, stack.pop());
        Assert.assertEquals(o2, stack.pop());
        Assert.assertTrue(stack.isEmpty());
    }

    @Test(expected = EmptyStackException.class)
    public void testEmptyPop() throws Exception {
        stack.pop();
    }

    @Test
    public void testPeek() throws Exception {
        final Object o = new Object();
        stack.add(new Object());
        stack.add(o);
        Assert.assertEquals(o, stack.peek());
        Assert.assertFalse(stack.isEmpty());
    }

    @Test(expected = EmptyStackException.class)
    public void testEmptyPeek() throws Exception {
        stack.peek();
    }

    @Test
    public void testPush() throws Exception {
        final Object o1 = new Object();
        final Object o2 = new Object();
        stack.push(o2);
        Assert.assertFalse(stack.isEmpty());
        stack.push(o1);
        Assert.assertEquals(2, stack.size());
        Assert.assertEquals(FXCollections.observableArrayList(o2, o1), stack);
    }
}