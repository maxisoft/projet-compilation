/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author olivier
 */
public class Heap {

    private final int HEAPSIZE = 2048;
    private int freeSize;
    private int heap[];
    private DataTable dataTable;
    private FreeAddressTable addressTable;

    public Heap() {
        heap = new int[HEAPSIZE];
        dataTable = new DataTable();
        addressTable = new FreeAddressTable();
        addressTable.getFreeBlocks().add(new HeapBlock(1, HEAPSIZE));
    }

    public int getHeapSize() {
        return HEAPSIZE;
    }

    public int[] getHeap() {
        return heap;
    }

    public int getFreeSize() {
        return freeSize;
    }

    public void setHeap(int[] heap) {
        this.heap = heap;
    }

    public void setFreeSize(int freeSize) {
        this.freeSize = freeSize;
    }

    public int StoreArray(Integer[] array, String ident) {
        int address;
        HeapBlock block = addressTable.getFreeBlock(array.length);
        address = block.getAddress();
        TreeSet<HeapBlock> listBlocks = (TreeSet<HeapBlock>) block.cutBlock(array.length);
        listBlocks.pollFirst();
        addressTable.resizeBlock(block, listBlocks);

        for (int i = 0; i < array.length; i++) {
            heap[address + i] = array[i];
        }
        int key = dataTable.getTable().size() + 1;

        dataTable.getTable().add(new Data(key, address, array.length, ident));
        garbageCollector();
        return address;
    }

    public int declareArray(String ident, int size) {
        HeapBlock block = addressTable.getFreeBlock(size);
        int address = block.getAddress();
        TreeSet<HeapBlock> listBlocks = (TreeSet<HeapBlock>) block.cutBlock(size);
        listBlocks.pollFirst();
        addressTable.resizeBlock(block, listBlocks);
        int key = dataTable.getTable().size() + 1;
        dataTable.getTable().add(new Data(key, address, size, ident));
        garbageCollector();
        return address;
    }

    public void freeBlock(String ident) {
        int address = getAddressBlock(ident);
        Iterator<Data> iterator = dataTable.getTable().iterator();
        while (iterator.hasNext()) {
            Data d = iterator.next();
            if (d.getIdent().equals(ident)) {
                dataTable.removeData(d);
                HeapBlock block = new HeapBlock(d.getAddress(), d.getSize());
                addressTable.getFreeBlocks().add(block);
                garbageCollector();
                break;
            }
        }
    }

    public void garbageCollector() {

        if (addressTable.getFreeBlocks().size() > 1) {
            TreeSet<HeapBlock> list = new TreeSet<HeapBlock>(addressTable.getFreeBlocks());
            Iterator<HeapBlock> iterator = list.iterator();
            HeapBlock first = iterator.next();
            HeapBlock second;
            while (iterator.hasNext()) {
                second = iterator.next();
                if (first.getAddress() + first.getSize() == second.getAddress()) {
                    addressTable.getFreeBlocks().remove(first);
                    addressTable.getFreeBlocks().remove(second);
                    HeapBlock newBlock = new HeapBlock(first.getAddress(), first.getSize() + second.getSize());
                    addressTable.getFreeBlocks().add(newBlock);
                    garbageCollector();
                }
            }
        }
    }

    public void affectValue(String ident, int index, int value) {
        Iterator<Data> iterator = dataTable.getTable().iterator();
        boolean found = false;
        while (iterator.hasNext()) {

            Data d = iterator.next();
            if (d.getIdent().equals(ident)) {
                found = true;
                checkIndex(d, index);
                heap[d.getAddress() + index] = value;
                break;
            }
        }
        if (!found) {
            throw new MemoryException("error, trying to set value to a non existant array");
        }
    }

    private void checkIndex(Data d, int index){
        if (index < 0){
            throw new ArrayIndexOutOfBoundsException("negative value");
        }
        if (index >= d.getSize()){
            throw new ArrayIndexOutOfBoundsException(String.format("index %d > size %d", index, d.getSize()));
        }
    }

    public Integer getValueOf(String ident, int index) {
        Iterator<Data> iterator = dataTable.getTable().iterator();
        while (iterator.hasNext()) {
            Data d = iterator.next();
            if (d.getIdent().equals(ident)) {
                checkIndex(d, index);
                return heap[d.getAddress() + index];
            }
        }
        throw new MemoryException("error, trying to get value from a non existant array");
    }

    public int getAddressBlock(String ident) {
        Iterator<Data> iterator = dataTable.getTable().iterator();
        while (iterator.hasNext()) {
            Data d = iterator.next();
            if (d.getIdent().equals(ident)) {
                return d.getAddress();
            }
        }
        return -1;
    }

    public int synonimArrays(String ident1, String ident2) {
        Data d1 = null, d2 = null;
        Iterator<Data> iterator = dataTable.getTable().iterator();
        while (iterator.hasNext()) {
            Data d = iterator.next();
            if (d.getIdent().equals(ident1)) {
                d1 = d;
            } else if (d.getIdent().equals(ident2)) {
                d2 = d;
            }
        }
        try {
            d1.setAddress(d2.getAddress());
            d1.setSize(d2.getSize());
            return d1.getAddress();
        } catch (NullPointerException e) {
            throw new MemoryException("error unknown array");
        }
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    @Override
    public String toString() {
        return addressTable.toString() + "\n\n" + dataTable.toString();
    }
}
