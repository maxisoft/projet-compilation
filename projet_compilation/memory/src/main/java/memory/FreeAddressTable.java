/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author olivier
 */
public class FreeAddressTable {

    private Set<HeapBlock> freeBlocks;

    public FreeAddressTable() {
        freeBlocks = new TreeSet<HeapBlock>();
    }

    public Set<HeapBlock> getFreeBlocks() {
        return freeBlocks;
    }

    public void setFreeBlocks(Set<HeapBlock> freeBlocks) {
        this.freeBlocks = freeBlocks;
    }

    public HeapBlock getFreeBlock(int size) {
        Iterator<HeapBlock> iterator = freeBlocks.iterator();
        HeapBlock b = null;
        while (iterator.hasNext()) {
            HeapBlock block = iterator.next();
            if (b == null && block.getSize() >= size) {
                b = block;
            } else if (block.getSize() >= size && block.getSize() < b.getSize()) {
                b = block;
            }
        }
        return b;
    }

    public void resizeBlock(HeapBlock block, Set<HeapBlock> blocks) {
        Iterator<HeapBlock> iterator = blocks.iterator();
        freeBlocks.remove(block);
        freeBlocks.addAll(blocks);
    }

    @Override
    public String toString() {
        String res = "";
        Iterator<HeapBlock> iterator = freeBlocks.iterator();
        while (iterator.hasNext()) {
            res += iterator.next().toString();
        }
        return "FreeAddressTable{\n" + res + "\n}";
    }

}
