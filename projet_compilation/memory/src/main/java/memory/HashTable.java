/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

/**
 *
 * @author olivier
 */
public class HashTable {

    private final static int B = 256;
    private final static int N = 503; //the hash will provide numbers between 0 and 502

    public static int hash(String s) {
        int value = 0;
        for (int i = 0; i < s.length(); i++) {
            value = (value * B + s.charAt(i)) % N;
        }
        return value;
    }

    public static int getN() {
        return N;
    }
}
