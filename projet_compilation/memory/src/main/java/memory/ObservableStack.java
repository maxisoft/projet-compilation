package memory;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.beans.Observable;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

/**
 * @param <E>
 * @author maxime
 */
public class ObservableStack<E> extends ObservableListWrapper<E> implements StackInterface<E> {

    public ObservableStack() {
        this(new ArrayList<>());
    }

    public ObservableStack(List<E> list) {
        super(list);
    }

    public ObservableStack(List<E> list, Callback<E, Observable[]> extractor) {
        super(list, extractor);
    }

    @Override
    public E pop() {
        int len = size();
        checkNonEmpty(len);
        return remove(len - 1);
    }

    @Override
    public E peek() {
        int len = size();
        checkNonEmpty(len);
        return get(len - 1);
    }

    @Override
    public void push(E item) {
        add(item);
    }

    private void checkNonEmpty(int length) {
        if (length == 0) {
            throw new EmptyStackException();
        }
    }
}
