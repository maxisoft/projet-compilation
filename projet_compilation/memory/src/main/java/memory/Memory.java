package memory;

import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author olivier
 */
public class Memory {

    private Quadruplets dictionary[];
    private ObservableSet<String> idents;
    private Heap heap;

    private Pile pile;

    public Memory() {
        pile = new Pile();
        idents = FXCollections.observableSet();
        dictionary = new Quadruplets[HashTable.getN()];
        heap = new Heap();
    }

    public ObservableSet<String> getIdents() {
        return idents;
    }

    /*
     * add a quadruplet to the correct list of quadruplet
     * if no such list exists, function creates the list before add
     */
    public Pile getPile() {
        return pile;
    }

    public Quadruplets[] getDictionary() {
        return dictionary;
    }

    public Heap getHeap() {
        return heap;
    }

    public void addQuad(Quadruplet q) {
        int index = HashTable.hash(q.getIdent());
        if (dictionary[index] == null) {
            dictionary[index] = new Quadruplets();
        }
        dictionary[index].addQuadruplet(q);
        pile.push(q);
        idents.add(q.getIdent());

    }

    public void addSymbol(String ident, Quadruplet quad) {
        int index = HashTable.hash(ident);
        if (dictionary[index] == null) {
            dictionary[index] = new Quadruplets();
        }
        dictionary[index].addQuadruplet(quad);
        idents.add(ident);
    }

    public void removeQuad(Quadruplet q) {
        int index = HashTable.hash(q.getIdent());
        if (dictionary[index] == null) {
            throw new MemoryException("erreur no list of objects with this name : ", q);
        } else {
            dictionary[index].deleteQuadruplet(q);
            if (dictionary[index].getQuadruplets().isEmpty()) {
                dictionary[index] = null;
                idents.remove(q.getIdent());
            }
        }
        Quadruplet q1 = pile.pop();
        if (!q1.equals(q)) {
            throw new MemoryException("erreur quadruplet is not in stack : ", q);
        }
    }

    public Quadruplet pop() {
        Quadruplet quad = pile.pop();
        if (quad.getIdent() != null) {
            int index = HashTable.hash(quad.getIdent());
            if (dictionary[index] != null) {
                dictionary[index].deleteQuadruplet(quad);
                if (dictionary[index].getQuadruplets().isEmpty()) {
                    dictionary[index] = null;
                    idents.remove(quad.getIdent());
                }
            }
        }
        return quad;
    }

    public void pushValue(int value) {
        pile.push(new Quadruplet(null, value, null, null));
    }

    public void push(Quadruplet quad) {
        pile.push(quad);
    }

    public Quadruplet searchQuad(String ident) {
        Quadruplet q;
        int index = HashTable.hash(ident);
        if (dictionary[index] == null) {
            return null;
        }
        q = dictionary[index].getQuadruplets().get(dictionary[index].getQuadruplets().size() - 1);
        return q;
    }

    public Quadruplet getLastSymbol(String ident) {

        int index = HashTable.hash(ident);
        if (dictionary[index] == null) {
            return null;
        }
        return dictionary[index].getLast();
    }

    public Quadruplets getSymbols(String ident) {
        int index = HashTable.hash(ident);
        if (dictionary[index] == null) {
            return null;
        }
        return dictionary[index];
    }

    public Quadruplet removeLastSymbol(String ident) {
        int index = HashTable.hash(ident);
        if (dictionary[index] == null) {
            return null;
        }
        Quadruplet q = dictionary[index].getLast();
        dictionary[index].deleteQuadruplet(q);
        if (dictionary[index].getQuadruplets().isEmpty()) {
            dictionary[index] = null;
            idents.remove(q.getIdent());
        }
        return q;
    }

    public void setVCST(Quadruplet q) {
        int index = HashTable.hash(q.getIdent());
        if (dictionary[index] == null) {
            throw new MemoryException("erreur no list of objects with this name : ", q);
        } else if (dictionary[index].getQuadruplets().get(0).getObj() != Obj.CONSTANT) {
            throw new MemoryException("erreur not a constant : ", q);

        } else {
            dictionary[index].setVSCT(q);
        }
        pile.setVCST(q);
    }

    public boolean isHigherRangeDecl(Quadruplet q) {
        Pile p = new Pile();
        while (!this.pile.isEmpty()) {
            Quadruplet quad = pile.pop();
            p.push(quad);
            if (quad.getObj() == Obj.METHOD) {
                while (!p.isEmpty()) {
                    pile.push(p.pop());
                }
                return true;
            } else if (quad.getIdent().equals(q.getIdent())) {
                while (!p.isEmpty()) {
                    pile.push(p.pop());
                }
                return false;
            }
        }
        return false;
    }

    @Override
    public String toString() {

        String res = "";
        for (int i = 0; i < dictionary.length; i++) {
            if (dictionary[i] != null) {
                res += dictionary[i].toString() + "\n";
            }
        }
        return "memory dico {\n" + res + "\n} \n" + pile.toString();
    }
}
