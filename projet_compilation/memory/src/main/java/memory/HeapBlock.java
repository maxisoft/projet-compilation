/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author olivier
 */
public class HeapBlock implements Comparable<HeapBlock> {

    private int address, size;

    /**
     *
     */
    public HeapBlock() {
    }

    public HeapBlock(int address, int size) {
        this.address = address;
        this.size = size;
    }

    /**
     *
     * @return
     */
    public int getAddress() {
        return address;
    }

    /**
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     *
     * @param address
     */
    public void setAddress(int address) {
        this.address = address;
    }

    /**
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     *
     * @param size
     * @return a list of blocks
     */
    public Set<HeapBlock> cutBlock(int size) {
        Set<HeapBlock> blocks = new TreeSet<HeapBlock>();
        int blockSize = this.size - size;
        int blockAddress = this.address + size;

        blocks.add(new HeapBlock(this.address, size));
        while (blockSize > 0) {
            int i = 1;
            while (i * 2 < blockSize) {
                i *= 2;
            }
            HeapBlock block = new HeapBlock(blockAddress, i);
            blocks.add(block);
            blockSize -= i;
            blockAddress += i;
        }
        return blocks;
    }

    @Override
    public int compareTo(HeapBlock o) {
        if (this.address > o.getAddress()) {
            return 1;
        } else if (this.address == o.getAddress()) {
            return 0;
        }
        return -1;
    }

    @Override
    public String toString() {
        return "\tHeapBlock{" + "address=" + address + ", size=" + size + "}\n";
    }
}
