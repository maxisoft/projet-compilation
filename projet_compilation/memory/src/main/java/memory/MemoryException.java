/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

/**
 *
 * @author olivier
 */
public class MemoryException extends RuntimeException {

    Quadruplet quadruplet;

    public MemoryException() {
        this("");
    }

    public MemoryException(String message) {
        this(message, null);
    }

    public MemoryException(String message, Quadruplet quadruplet) {
        super(message);
        this.quadruplet = quadruplet;
    }

    public Quadruplet getQuadruplet() {
        return quadruplet;
    }
}
