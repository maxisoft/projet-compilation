/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

import java.util.Stack;

/**
 *
 * @author olivier
 */
public class Pile extends ObservableStack<Quadruplet> {

    public Pile() {
    }

    public void swap() {

        Quadruplet first = this.pop();
        Quadruplet second = this.pop();
        this.push(first);
        this.push(second);
    }

    void setVCST(Quadruplet quad) {
        Pile p = new Pile();
        while (this.size() > 0) {
            Quadruplet q = this.pop();
            if (quad.isSameVCST(q)) {
                q.setObj(quad.getObj());
                q.setValue(quad.getValue());
            }
            p.push(q);
        }
        while (p.size() > 0) {
            Quadruplet q = p.pop();
            this.push(q);
        }
    }

    @Override
    public String toString() {
        String res = "Pile{\n";

        Pile p = new Pile();
        while (this.size() > 0) {
            Quadruplet q = this.pop();
            res += q.toString() + "\n";
            p.push(q);
        }
        while (p.size() > 0) {
            Quadruplet q = p.pop();
            //System.out.println(q.toString());
            this.push(q);
        }

        res += "\n}";
        return res;
    }
}
