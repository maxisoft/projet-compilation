/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

import fr.univfcomte.edu.ast.JajaType;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author olivier
 */
public class Quadruplet {

    private final StringProperty ident;
    private final ObjectProperty<Integer> value;
    private final ObjectProperty<Obj> obj; //variable or method
    private final ObjectProperty<JajaType> kind; //int boolean or void

    public Quadruplet(String ident, Integer value, Obj obj, JajaType kind) {
        this.ident = new SimpleStringProperty(ident);
        this.value = new SimpleObjectProperty<>(value);
        this.obj = new SimpleObjectProperty<>(obj);
        this.kind = new SimpleObjectProperty<>(kind);
    }

    public Quadruplet(String ident, Obj obj, JajaType kind) {
        this(ident, null, obj, kind);
    }

    public String getIdent() {
        return ident.get();
    }

    public Integer getValue() {
        return value.get();
    }

    public Obj getObj() {
        return obj.get();
    }

    public JajaType getKind() {
        return kind.get();
    }

    public void setIdent(String ident) {
        this.ident.set(ident);
    }

    public void setValue(Integer value) {
        this.value.set(value);
    }

    public void setObj(Obj obj) {
        this.obj.set(obj);
    }

    public void setKind(JajaType kind) {
        this.kind.set(kind);
    }

    public StringProperty identProperty() {
        return ident;
    }
    public ObjectProperty<Integer> valueProperty(){
        return value;
    }
    public ObjectProperty<Obj> objectProperty() {
        return obj;
    }
    public ObjectProperty<JajaType> kindProperty() {
        return kind;
    }

    public boolean isEqual(Quadruplet q) {
        return this.getIdent().equals(q.getIdent()) && this.getValue().equals(q.getValue())
                && this.getObj() == q.getObj() && this.getKind() == q.getKind();
    }

    @Override
    public String toString() {
        return "{" + "ident=" + getIdent() + ", Valeur=" + getValue() + ", Obj=" + getObj() + ", Type=" + getKind() + '}';
    }

    public boolean isSameVCST(Quadruplet q) {
        return q.getIdent().equals(this.getIdent()) && this.getValue() == null
                && this.getObj() == Obj.VCONSTANT && q.getKind() == getKind();
    }

    public boolean equals(Quadruplet q) {
        if (q.getIdent().equals(this.getIdent())
                && q.getObj() == getObj() && q.getKind() == getKind()) {
            if (q.getValue() == null || q.getValue().equals(this.getValue())) {
                return true;
            }
        }
        return false;
    }
}
