package memory;

import java.util.List;

/**
 * @author maxime
 */
public interface StackInterface<E> extends List<E> {
    E pop();

    E peek();

    void push(E item);
}
