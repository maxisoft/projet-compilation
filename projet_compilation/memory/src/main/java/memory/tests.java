/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

/**
 *
 * @author olivier
 */
public class tests {

    public static void main(String args[]) {
        /*Memory mem = new Memory();
         Quadruplet q = new Quadruplet("fdq", Obj.VCONSTANT, JajaType.INT);
         mem.addQuad(new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT));
         mem.addQuad(new Quadruplet("plop", 5, Obj.VARIABLE, JajaType.INT));
         mem.addQuad(new Quadruplet("var", 0, Obj.VARIABLE, JajaType.BOOLEAN));
         mem.addQuad(q);
         mem.addQuad(new Quadruplet("fdqs_876Thfjdsk6GH", 5, Obj.VARIABLE, JajaType.INT));
         mem.addQuad(new Quadruplet("fdsgrfvd", 0, Obj.VARIABLE, JajaType.BOOLEAN));
         mem.addQuad(new Quadruplet("oikjhnb", 5, Obj.VARIABLE, JajaType.INT));
         mem.addQuad(new Quadruplet("yjfd_3", Obj.VCONSTANT, JajaType.INT));
         mem.addQuad(new Quadruplet("grfqsgrf", 0, Obj.VARIABLE, JajaType.BOOLEAN));
         mem.addQuad(new Quadruplet("fdqs_876Thfjdsk6GH", 54, Obj.VARIABLE, JajaType.INT));
         System.out.println("\n\n avnat setVCST " + mem.toString() + "\n");
         q.setValue(5);
         q.setObj(Obj.CONSTANT);
         mem.setVCST(q);
         System.out.println("\n\n après setVCST " + mem.toString());
         mem.removeQuad(new Quadruplet("fdqs_876Thfjdsk6GH", 54, Obj.VARIABLE, JajaType.INT));
         mem.removeQuad(new Quadruplet("grfqsgrf", 0, Obj.VARIABLE, JajaType.BOOLEAN));
         mem.removeQuad(new Quadruplet("yjfd_3", Obj.VCONSTANT, JajaType.INT));
         mem.removeQuad(new Quadruplet("oikjhnb", 5, Obj.VARIABLE, JajaType.INT));
         mem.removeQuad(new Quadruplet("fdsgrfvd", 0, Obj.VARIABLE, JajaType.BOOLEAN));
         mem.removeQuad(new Quadruplet("fdqs_876Thfjdsk6GH", 5, Obj.VARIABLE, JajaType.INT));
         mem.removeQuad(q);
         mem.removeQuad(new Quadruplet("var", 0, Obj.VARIABLE, JajaType.BOOLEAN));
         mem.removeQuad(new Quadruplet("plop", 5, Obj.VARIABLE, JajaType.INT));
         mem.removeQuad(new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT));
         System.out.println("après vidage de la mémoire " + mem.toString());
         */

        Heap heap = new Heap();
        System.out.println(heap.toString());
        Integer testArray[] = new Integer[15];
        for (int i = 0; i < 15; i++) {
            int nbr = i;
            testArray[i] = nbr;
        }
        Integer testArray2[] = new Integer[70];
        testArray2 = new Integer[40];
        testArray = new Integer[512];
        heap.declareArray("plip", 512);
        heap.declareArray("plap", 512);
        heap.declareArray("plop", 75);
        System.out.println("\n\n\ntest liberation mémoire 3 tabs  plop\n\n" + heap.toString());
        heap.freeBlock("plip");
        System.out.println("\n\n\ntest liberation mémoire 2 tabs \n\n" + heap.toString());
        heap.StoreArray(testArray, "test");
        System.out.println("\n\n\ntest liberation mémoire 3 tabs \n\n" + heap.toString());
        heap.affectValue("test", 3, 5);
        System.out.println(heap.getValueOf("plop", 3));
        heap.freeBlock("plop");
        heap.freeBlock("plap");
        heap.freeBlock("test");
        System.out.println("\n\n\ntest liberation mémoire \n\n" + heap.toString());

    }

}
