/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

import com.google.common.collect.Iterables;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author olivier
 */
public class Quadruplets {

    private ObservableList<Quadruplet> quadruplets;
    private String identObjs; //ident of stocked items for looking

    public Quadruplets() {
        quadruplets = FXCollections.observableArrayList();
        identObjs = null;
    }

    public String getIdentObjs() {
        return identObjs;
    }

    public ObservableList<Quadruplet> getQuadruplets() {
        return quadruplets;
    }

    public void setIdentObjs(String identObjs) {
        this.identObjs = identObjs;
    }

    public void addQuadruplet(Quadruplet q) {
        if (this.identObjs == null) {
            identObjs = q.getIdent();
        }
        quadruplets.add(q);
    }

    /*
     * delete the last instance of the object
     */
    public void deleteQuadruplet(Quadruplet q) {
        for (int i = quadruplets.size() - 1; i >= 0; i--) {
            if (quadruplets.get(i).isEqual(q)) {
                quadruplets.remove(i);
                break;
            }
        }
    }

    /*
     *Set the value of a vconstant et changes obj from VCONSTANT to CONSTANT
     */
    void setVSCT(Quadruplet quad) {
        Iterator<Quadruplet> iterator = quadruplets.iterator();
        while (iterator.hasNext()) {
            Quadruplet q = iterator.next();
            q.setObj(Obj.CONSTANT);
            q.setValue(quad.getValue());
        }
    }

    Quadruplet getLast() {
        return Iterables.getLast(quadruplets);
    }

    @Override
    public String toString() {
        Iterator<Quadruplet> iterator = quadruplets.iterator();
        String res = "";
        while (iterator.hasNext()) {
            res += iterator.next().toString() + "\t";
        }
        return res;
    }
}
