/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;


import java.util.Iterator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author olivier
 */
public class DataTable {
   private ObservableList<Data> table;
   

    public DataTable() {
         table = FXCollections.observableArrayList();
      
    }
public ObservableList<Data> getTable() {
        return table;
    }
    

    public void setTable(ObservableList<Data> table) {
        this.table = table;
    }

    public void removeData(Data d) {
        this.table.remove(d);
    }

    @Override
    public String toString() {

        String res = "";

        Iterator<Data> iterator = table.iterator();
        while (iterator.hasNext()) {
            res += iterator.next().toString() + "\n";
        }

        return "DataTable{\n" + res + "}";
    }
}
