/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory;

import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author olivier
 */
public class Data {

    private final IntegerProperty key, address, size;
     private final StringProperty ident;

    
    
public Data( Integer key, Integer address, Integer size, String ident) {
        this.ident = new SimpleStringProperty(ident);
         this.key = new SimpleIntegerProperty(key);
           this.address = new SimpleIntegerProperty(address);
             this.size = new SimpleIntegerProperty(size);
         
        
    }

    public Integer getKey() {
        return key.get();
    }

    public Integer  getAddress() {
        return address.get();
    }

    public String getIdent() {
        return ident.get();
    }

    public Integer getSize() {
        return size.get();
    }

    
   public void setKey(Integer key) {
        this.key.set(key);
    }
   public void setSize(Integer size) {
        this.size.set(size);
    }
   public void setAddress(Integer address) {
        this.address.set(address);
    }
   public void setIdent(String ident) {
        this.ident.set(ident);
    }

   public StringProperty identProperty() {
        return ident;
    }
    public IntegerProperty keyProperty(){
        return key;
    }
  public IntegerProperty sizeProperty(){
        return size;
    }
    public IntegerProperty adresseProperty(){
        return address;
    }
      

    @Override
    public String toString() {
        return "\tData{" + "key=" + getKey() + ", address=" + getAddress() + ", ident=" + getIdent()  + "}\n";
    }
}
