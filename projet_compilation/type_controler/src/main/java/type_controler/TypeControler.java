package type_controler;

import fr.univfcomte.edu.ast.JajaAffect;
import fr.univfcomte.edu.ast.JajaAffectArray;
import fr.univfcomte.edu.ast.JajaBoolean;
import fr.univfcomte.edu.ast.JajaCall;
import fr.univfcomte.edu.ast.JajaCallExpr;
import fr.univfcomte.edu.ast.JajaClass;
import fr.univfcomte.edu.ast.JajaDecls;
import fr.univfcomte.edu.ast.JajaElse;
import fr.univfcomte.edu.ast.JajaExpr;
import fr.univfcomte.edu.ast.JajaExprs;
import fr.univfcomte.edu.ast.JajaIdent;
import fr.univfcomte.edu.ast.JajaIdentArray;
import fr.univfcomte.edu.ast.JajaIf;
import fr.univfcomte.edu.ast.JajaIncrement;
import fr.univfcomte.edu.ast.JajaInstrs;
import fr.univfcomte.edu.ast.JajaMain;
import fr.univfcomte.edu.ast.JajaMeth;
import fr.univfcomte.edu.ast.JajaNumber;
import fr.univfcomte.edu.ast.JajaParam;
import fr.univfcomte.edu.ast.JajaParams;
import fr.univfcomte.edu.ast.JajaReturn;
import fr.univfcomte.edu.ast.JajaType;
import fr.univfcomte.edu.ast.JajaVar;
import fr.univfcomte.edu.ast.JajaVarArray;
import fr.univfcomte.edu.ast.JajaVars;
import fr.univfcomte.edu.ast.JajaWhile;
import fr.univfcomte.edu.ast.expr.JajaAnd;
import fr.univfcomte.edu.ast.expr.JajaDiv;
import fr.univfcomte.edu.ast.expr.JajaEq;
import fr.univfcomte.edu.ast.expr.JajaMinus;
import fr.univfcomte.edu.ast.expr.JajaMult;
import fr.univfcomte.edu.ast.expr.JajaNot;
import fr.univfcomte.edu.ast.expr.JajaNotSetExpr;
import fr.univfcomte.edu.ast.expr.JajaOr;
import fr.univfcomte.edu.ast.expr.JajaPlus;
import fr.univfcomte.edu.ast.expr.JajaSup;
import fr.univfcomte.edu.ast.expr.JajaUnaryMinus;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import memory.Memory;
import memory.Obj;
import memory.Quadruplet;
import visitor.AstVisitor;
import visitor.TypeException;

/**
 *
 * @author Olivier
 */
public class TypeControler implements AstVisitor<JajaType, Void> {

    private Memory memory;
    private List<JajaMeth> meths;

    public TypeControler() {
        memory = new Memory();
        meths = new ArrayList<JajaMeth>();
    }

    @Override
    public JajaType visit(JajaAffect visitable, Void data) throws TypeException {

        String ident = visitable.getIdent();
        JajaType type = memory.searchQuad(ident).getKind();
        if (type == visitable.getExpr().accept(this)) {
            return type;
        }
        throw new TypeException(type + " was expected, but found : " + memory.searchQuad(ident).getKind(), visitable);
    }

    @Override
    public JajaType visit(JajaCall visitable, Void data) throws TypeException {
        String ident = visitable.getIdent();
        int nbParams;
        try {
            nbParams = visitable.getJajaParams().size();
        } catch (NullPointerException e) {
            nbParams = 0;
        }
        Iterator<JajaMeth> iterator = meths.iterator();
        while (iterator.hasNext()) {
            JajaMeth meth = iterator.next();
            if (meth.getIdentifier().equals(visitable.getIdent()) && meth.getParams().size() == nbParams) {
                boolean methFound = true;
                for (int i = 0; i < nbParams; i++) {
                    if (meth.getParams().get(i).getType() != visitable.getJajaParams().get(i).accept(this)) {
                        methFound = false;
                        break;
                    }
                }
                if (methFound) {
                    visitable.setJajaMeth(meth);
                    return meth.getType();
                }
            }
        }
        System.out.println(visitable.getIdent());
        throw new TypeException("error no method with this signature found", visitable);
    }

    /**
     *
     * @param visitable
     * @param data
     * @return
     * @throws TypeException
     */
    @Override
    public JajaType visit(JajaClass visitable, Void data) throws TypeException {
        visitable.getJajaDecls().accept(this);
        System.out.println("class");
        visitable.getJajaMain().accept(this);
        System.out.println("class");
        return null;
    }

    @Override
    public JajaType visit(JajaDecls visitable, Void data) throws TypeException {
        visitable.forEach(decl -> decl.accept(this));
        return null;
    }

    @Override
    public JajaType visit(JajaElse visitable, Void data) throws TypeException {
        JajaType type = visitable.getJajaInstrs().accept(this);
        return type;
    }

    @Override
    public JajaType visit(JajaIf visitable, Void data) throws TypeException {
        JajaType typeExpr = visitable.getExpr().accept(this);
        if (typeExpr == JajaType.BOOLEAN) {
            JajaType typeInstrs = visitable.getJajaInstrs().accept(this);
            JajaType typeElse = visitable.getJajaElse().accept(this);
            if (typeInstrs == JajaType.RETBOOLEAN || typeInstrs == JajaType.RETINT || typeInstrs == JajaType.RETVOID) {
                if (typeInstrs == typeElse) {
                    return typeInstrs;
                }
                return null;
            }
        }
        return null;
    }

    @Override
    public JajaType visit(JajaIncrement visitable, Void data) throws TypeException {
        String ident = visitable.getIdent();
        JajaType type = memory.searchQuad(ident).getKind();
        if (type == JajaType.INT) {
            return type;
        }
        throw new TypeException("error : increment on variable type " + type, visitable);
    }

    @Override
    public JajaType visit(JajaMain visitable, Void data) throws TypeException {
        System.out.println("main");
        memory.addQuad(new Quadruplet("methmain", Obj.METHOD, JajaType.VOID));
        visitable.getJajaVars().accept(this);
        System.out.println("main");
        visitable.getJajaInstrs().accept(this);
        return null;
    }

    @Override
    public JajaType visit(JajaMeth visitable, Void data) throws TypeException {

        meths.add(visitable);
        memory.addQuad(new Quadruplet(visitable.getIdentifier(), Obj.METHOD, visitable.getType()));
        for (int i = 0; i < visitable.getParams().size(); i++) {
            JajaParam param = visitable.getParams().get(i);
            Quadruplet q = new Quadruplet(param.getIdentifier(), 0, Obj.VARIABLE, param.getType());
            memory.addQuad(q);
        }
        visitable.getJajaVars().accept(this);
        JajaType typeReturn = visitable.getJajaInstrs().accept(this);
        if (visitable.getType() != JajaType.VOID) {
            if (this.typeToReturn(visitable.getType()) != typeReturn) {
                throw new TypeException("error method was declared type " + visitable.getType() + ", but " + typeReturn + " was found", visitable);
            }
        } else if (typeReturn != null) {
            throw new TypeException("error method was declared type void, but return statement found", visitable);
        }

        return null;
    }

    @Override
    public JajaType visit(JajaParam visitable, Void data) throws TypeException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JajaType visit(JajaReturn visitable, Void data) throws TypeException {
        return this.typeToReturn(visitable.getExpr().accept(this));
    }

    @Override
    public JajaType visit(JajaVar visitable, Void data) throws TypeException {
        System.out.println("var");
        String ident = visitable.getIdentifier();
        JajaType type = visitable.getType();
        JajaType typeExpr;
        Quadruplet quad = memory.searchQuad(ident);
        System.out.println("var");
        if (type == JajaType.VOID) {
            throw new TypeException("error, variable of type void iine : " + visitable.getLine(), visitable);
        }
        if (quad == null || memory.isHigherRangeDecl(quad)) {

            /*
             if value is set to null, the variable has no value, else it's set to null
             */
            System.out.println("var");
            Integer value = null;
            value = 0;
            Quadruplet q = new Quadruplet(ident, value, Obj.VARIABLE, type);
            memory.addQuad(q);
            System.out.println("var");
            typeExpr = visitable.getValueExpr().accept(this);
            System.out.println("var");
            if (typeExpr != null && type != typeExpr) {
                throw new TypeException("erreur type variable : " + ident + "\n\ttype variable : " + type + "\n\ttype expr : " + typeExpr, visitable);
            }
            System.out.println("var");
            return type;
        }
        throw new TypeException("error : variable " + ident + " already defined", visitable);
    }

    @Override
    public JajaType visit(JajaVars visitable, Void data) throws TypeException {
        visitable.forEach(var -> var.accept(this));
        System.out.println("vars");
        return null;
    }

    @Override
    public JajaType visit(JajaWhile visitable, Void data) throws TypeException {
        JajaType typeExpr = visitable.getExpr().accept(this);
        JajaType typeInstrs = visitable.getJajaInstrs().accept(this);
        if (typeExpr == JajaType.BOOLEAN) {
            return this.ReturnToType(typeInstrs);
        }
        throw new TypeException("error boolean expression expected", visitable);
    }

    @Override
    public JajaType visit(JajaAnd visitable, Void data) throws TypeException {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.BOOLEAN)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.BOOLEAN;
    }

    @Override
    public JajaType visit(JajaDiv visitable, Void data) throws TypeException {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.INT)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.INT;
    }

    @Override
    public JajaType visit(JajaEq visitable, Void data) throws TypeException {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.INT)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.BOOLEAN;
    }

    @Override
    public JajaType visit(JajaMinus visitable, Void data) throws TypeException {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.INT)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.INT;
    }

    @Override
    public JajaType visit(JajaMult visitable, Void data) throws TypeException {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.INT)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.INT;
    }

    @Override
    public JajaType visit(JajaOr visitable, Void data) throws TypeException {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.BOOLEAN)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.BOOLEAN;
    }

    @Override
    public JajaType visit(JajaPlus visitable, Void data) throws TypeException {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.INT)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.INT;
    }

    @Override
    public JajaType visit(JajaSup visitable, Void data) {
        for (int i = 0; i < visitable.getSonsExprs().size(); i++) {
            if (!(visitable.getSonsExprs().get(i).accept(this) == JajaType.INT)) {
                throw new TypeException("error line " + visitable.getSonsExprs().get(i).getLine() + ", operator cannot be applied to " + visitable.getSonsExprs().get(i).accept(this), visitable);
            }
        }
        return JajaType.BOOLEAN;
    }

    @Override
    public JajaType visit(JajaUnaryMinus visitable, Void data) throws TypeException {
        if (visitable.getSonsExprs().get(0).accept(this) == JajaType.INT) {
            return JajaType.INT;
        }
        throw new TypeException("error operator unary minus \"-\" cannot be applied to type different of int", visitable);
    }

    @Override
    public JajaType visit(JajaInstrs visitable, Void data) throws TypeException {
        JajaType type;
        for (int i = 0; i < visitable.size(); i++) {
            type = visitable.get(i).accept(this);

            if (type == JajaType.RETBOOLEAN || type == JajaType.RETINT || type == JajaType.RETVOID) {
                if (i < visitable.size() - 1) {
                    throw new TypeException("error : unreachable statement : " + visitable.get(i + 1).getLine(), visitable);
                }
                return type;
            }
        }
        return null;
    }

    @Override
    public JajaType visit(JajaExprs jajaExprs, Void data) throws TypeException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JajaType visit(JajaNotSetExpr jajaNotSetExpr, Void data) throws TypeException {
        return null;
    }

    @Override
    public JajaType visit(JajaParams jajaParams, Void data) throws TypeException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JajaType visit(JajaBoolean jajaBoolean, Void data) throws TypeException {
        return JajaType.BOOLEAN;
    }

    @Override
    public JajaType visit(JajaIdent visitable, Void data) throws TypeException {
        Quadruplet q = memory.searchQuad(visitable.getValue());
        if (q == null) {
            throw new TypeException("error : undeclared variable : " + visitable, visitable);
        } else if (q.getValue() == null) {
            throw new TypeException("error : undefined variable : " + visitable, visitable);
        }
        return q.getKind();
    }

    @Override
    public JajaType visit(JajaNumber jajaNumber, Void data) throws TypeException {
        return JajaType.INT;
    }

    @Override
    public JajaType visit(JajaVarArray jajaVarArray, Void data) {
        return null;
    }

    @Override
    public JajaType visit(JajaAffectArray jajaAffectArray, Void data) {
        return null;
    }

    private JajaType visitExpr(JajaExpr visitable, String data) throws TypeException {
        JajaType type0, type1;
        type0 = visitable.getSonsExprs().get(0).accept(this);
        type1 = visitable.getSonsExprs().get(1).accept(this);

        if (type0 == type1) {
            if (((visitable instanceof JajaAnd || visitable instanceof JajaOr)
                    && type0 == JajaType.BOOLEAN)
                    || ((visitable instanceof JajaPlus || visitable instanceof JajaMinus
                    || visitable instanceof JajaMult || visitable instanceof JajaDiv)
                    && type0 == JajaType.INT)
                    || ((visitable instanceof JajaEq || visitable instanceof JajaSup)
                    && type0 == JajaType.INT)) {
                return type0;

            }
            throw new TypeException("error type expression : " + visitable.getOperator() + "\t line : " + visitable.getLine(), visitable);
        }
        throw new TypeException("error type expression : " + visitable.getOperator() + "\t line : " + visitable.getLine(), visitable);
    }

    @Override
    public JajaType visit(JajaNot visitable, Void data) {
        if (visitable.getSonsExprs().get(0).accept(this) == JajaType.BOOLEAN) {
            return JajaType.BOOLEAN;
        }
        throw new TypeException("error operator not \"!\" cannot be applied to type different of boolean", visitable);
    }

    private JajaType typeToReturn(JajaType type) {

        if (type == JajaType.INT) {
            return JajaType.RETINT;
        } else if (type == JajaType.BOOLEAN) {
            return JajaType.RETBOOLEAN;
        } else if (type == JajaType.VOID) {
            return JajaType.RETVOID;
        }
        return type;
    }

    private JajaType ReturnToType(JajaType type) {
        if (type == JajaType.RETINT) {
            return JajaType.INT;
        } else if (type == JajaType.RETBOOLEAN) {
            return JajaType.BOOLEAN;
        } else if (type == JajaType.RETVOID) {
            return JajaType.VOID;
        }
        return type;
    }

    @Override
    public JajaType visit(JajaCallExpr visitable, Void data) {
        String ident = visitable.getIdent();
        int nbParams;
        try {
            nbParams = visitable.getJajaParams().size();
        } catch (NullPointerException e) {
            nbParams = 0;
        }
        Iterator<JajaMeth> iterator = meths.iterator();
        while (iterator.hasNext()) {
            JajaMeth meth = iterator.next();
            if (meth.getIdentifier().equals(visitable.getIdent()) && meth.getParams().size() == nbParams) {
                boolean methFound = true;
                for (int i = 0; i < nbParams; i++) {
                    if (meth.getParams().get(i).getType() != visitable.getJajaParams().get(i).accept(this)) {
                        methFound = false;
                        break;
                    }
                }
                if (methFound) {
                    visitable.setJajaMeth(meth);
                    return meth.getType();
                }
            }
        }
        System.out.println(visitable.getIdent());
        throw new TypeException("error no method with this signature found", visitable);
    }

    @Override
    public JajaType visit(JajaIdentArray jajaIdentArray, Void data) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return null;
    }
}
