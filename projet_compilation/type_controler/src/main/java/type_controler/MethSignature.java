/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type_controler;

import fr.univfcomte.edu.ast.JajaType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olivier
 */
public class MethSignature {

    private JajaType typeReturn;
    private String ident;
    private List<JajaType> typeParams;

    public MethSignature() {
        typeParams = new ArrayList<JajaType>();
    }

    public JajaType getTypeReturn() {
        return typeReturn;
    }

    public String getIdent() {
        return ident;
    }

    public List<JajaType> getTypeParams() {
        return typeParams;
    }

    public void setTypeReturn(JajaType typeReturn) {
        this.typeReturn = typeReturn;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public void setTypeParams(List<JajaType> typeParams) {
        this.typeParams = typeParams;
    }

    public void addTypeParam(JajaType type) {
        this.typeParams.add(type);
    }
}
