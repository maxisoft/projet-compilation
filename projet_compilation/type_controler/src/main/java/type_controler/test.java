/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type_controler;

import fr.univfcomte.edu.analyzer.ParseException;
import fr.univfcomte.edu.ast.Analyzer;
import fr.univfcomte.edu.ast.Compilateur;
import fr.univfcomte.edu.ast.JajaClass;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import visitor.TypeException;

/**
 *
 * @author olivier
 */
public class test {

    public static void main(String[] args) throws FileNotFoundException, URISyntaxException, ParseException {

        URL resource = Compilateur.class.getClassLoader().getResource("input.txt");
        if (resource == null) {
            throw new NullPointerException("no input.txt file");
        }
        System.out.print(resource.toString());
        File file = new File(resource.toURI());
        Analyzer parser = new Analyzer(file);
        TypeControler visitor = new TypeControler();
        System.out.println("\n\n\n\n controleur type : \n\n");
        JajaClass astRoot = parser.getASTRoot();
        try {
            astRoot.accept(visitor);
        } catch (TypeException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("\n\n");
    }
}
