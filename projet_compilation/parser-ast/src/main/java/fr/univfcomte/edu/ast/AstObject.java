package fr.univfcomte.edu.ast;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import compiler.visitor.CompilerAstVisitable;
import controler.visitor.ControlerAstVisitable;
import gui.astviewer.AstViewerAstVisitable;
import visitor.interpreter.InterpreterAstVisitor;

/**
 * @author maxime
 */
public abstract class AstObject implements CompilerAstVisitable, ControlerAstVisitable, AstViewerAstVisitable,InterpreterAstVisitor {

    // TODO change Integer -> int when every subclasses has line property
    protected Integer line;
    // TODO change Integer -> int when every subclasses has column property
    protected Integer column;
    
    
   
   
    public Integer getLine() {
        return line;
    }

    public void setLine(int line) {
        if (line < 0) {
            throw new IllegalArgumentException("line argument must me greater than 0");
        }
        this.line = line;
    }

    public Integer getColumn() {
        return column;
    }


    public void setColumn(int column) {
        if (column < 0) {
            throw new IllegalArgumentException("column argument must me greater than 0");
        }
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AstObject)) return false;

        AstObject astObject = (AstObject) o;

        if (column != null ? !column.equals(astObject.column) : astObject.column != null) return false;
        if (line != null ? !line.equals(astObject.line) : astObject.line != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = line != null ? line.hashCode() : 0;
        result = 31 * result + (column != null ? column.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("line", line)
                .add("column", column)
                .toString();
    }
}
