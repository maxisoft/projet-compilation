package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.JajaArrayInterface;
import fr.univfcomte.edu.ast.JajaExpr;

/**
 * @author maxime
 */
public class ArrayExprBuilderImpl extends ExprBuilderImpl implements ArrayExprBuilder {
    private JajaExpr expr;

    @Override
    public JajaExpr getIndexExpr() {
        return expr;
    }

    @Override
    public ExprBuilderImpl setIndexExpr(JajaExpr expr){
        this.expr = expr;
        return this;
    }

    @Override
    public JajaExpr build() {
        JajaExpr ret = super.build();
        if (!(ret instanceof JajaArrayInterface)){
            throw new BuildException(new IllegalStateException("You must set a correct type hint"), this);
        }
        ((JajaArrayInterface) ret).setIndexExpr(expr);
        return ret;
    }
}
