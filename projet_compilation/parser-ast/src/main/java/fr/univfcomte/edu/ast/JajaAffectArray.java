/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;

/**
 * @author maxime
 */
public class JajaAffectArray extends JajaInstr implements JajaArrayInterface {

    private String ident;
    private JajaExpr index;
    private JajaExpr expr;
    private String operator;


    public static JajaAffectArray createFrom(JajaAffect jajaAffect, JajaExpr index) {
        JajaAffectArray ret = new JajaAffectArray();
        ret.setIdent(jajaAffect.getIdent());
        ret.setOperator(jajaAffect.getOperator());
        ret.setExpr(jajaAffect.getExpr());
        ret.setIndexExpr(index);
        return ret;
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    @Override
    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }

    @Override
    public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public JajaExpr getExpr() {
        return expr;
    }

    public void setExpr(JajaExpr expr) {
        this.expr = expr;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public JajaExpr getIndexExpr() {
        return index;
    }

    @Override
    public void setIndexExpr(JajaExpr indexExpr) {
        this.index = indexExpr;
    }
}
