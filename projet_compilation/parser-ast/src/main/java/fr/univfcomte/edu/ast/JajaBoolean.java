/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;


/**
 *
 * @author olivier
 */
public class JajaBoolean extends JajaPrimExpr {

    private Boolean value;

    public JajaBoolean() {
    }

    public Boolean getBooleanValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        super.setValue(value);
        setValue(Boolean.valueOf(value));
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }
    
    public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    
    
}
