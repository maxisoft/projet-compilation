package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;

public class JajaIdent extends JajaPrimExpr {

    public void setIdent(String ident) {
        this.value = ident;
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }

    public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }
}
