package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.JajaExpr;

/**
 * @author maxime
 */
public interface ArrayExprBuilder extends ExprBuilder {
    JajaExpr getIndexExpr();

    ExprBuilderImpl setIndexExpr(JajaExpr expr);
}
