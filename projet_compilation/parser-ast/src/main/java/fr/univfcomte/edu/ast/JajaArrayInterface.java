package fr.univfcomte.edu.ast;

/**
 * @author maxime
 */
public interface JajaArrayInterface {
    JajaExpr getIndexExpr();

    void setIndexExpr(JajaExpr indexExpr);
}
