/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import java.util.Iterator;


/**
 *
 * @author olivier
 */
public abstract class JajaExpr extends AstObject {

    protected JajaExprs sonsExprs;
    protected String value;
    protected String operator;

    public JajaExpr() {
        sonsExprs = new JajaExprs();
    }
    
    
    public static boolean isUnary(JajaExpr expr) {
        return expr.getOperator() != null && expr.getSonsExprs().size() == 1;
    }

    public String getValue() {
        return value;
    }

    public JajaExprs getSonsExprs() {
        return sonsExprs;
    }

    public String getOperator() {
        return operator;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setSonsExprs(JajaExprs sonsExprs) {
        this.sonsExprs = sonsExprs;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        String res = "";

        if (value != null && !value.equals("")) {
            return value;
        }
        Iterator<JajaExpr> iterator = this.sonsExprs.iterator();
        while (iterator.hasNext()) {
            res += iterator.next().toString() + ")\n";
        }
        return res + "\n";
    }
}
