package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.JajaExpr;
import fr.univfcomte.edu.ast.JajaExprs;

import static java.util.Objects.requireNonNull;

/**
 * Created by maxime on 17/12/2014.
 */
public class ExprBuilderWrapper implements ExprBuilderWithTypeHint {
    private JajaExpr expr;

    public ExprBuilderWrapper(JajaExpr expr){
        setExpr(expr);
    }

    private void setExpr(JajaExpr expr){
        this.expr = requireNonNull(expr);
    }
    @Override
    public Integer getLine() {
        return expr.getLine();
    }

    @Override
    public AstObjectBuilder setLine(Integer line) {
        expr.setLine(line);
        return this;
    }

    @Override
    public Integer getColumn() {
        return expr.getColumn();
    }

    @Override
    public AstObjectBuilder setColumn(Integer column) {
        expr.setColumn(column);
        return this;
    }

    @Override
    public JajaExpr build() {
        return expr;
    }

    @Override
    public JajaExprs getSonsExprs() {
        return expr.getSonsExprs();

    }

    @Override
    public ExprBuilder setSonsExprs(JajaExprs sonsExprs) {
        expr.setSonsExprs(sonsExprs);
        return this;
    }

    @Override
    public ExprBuilder addSonExprs(JajaExpr sonExpr) {
        expr.getSonsExprs().add(sonExpr);
        return this;
    }

    @Override
    public ExprBuilder addSonExprs(ExprBuilder sonExprBuilder) {
        expr.getSonsExprs().add(sonExprBuilder.build());
        return this;
    }

    @Override
    public String getValue() {
        return expr.getValue();
    }

    @Override
    public ExprBuilder setValue(String value) {
        expr.setValue(value);
        return this;
    }

    @Override
    public String getOperator() {
        return expr.getOperator();
    }

    @Override
    public ExprBuilder setOperator(String operator) {
        expr.setOperator(operator);
        return this;
    }

    @Override
    public void setTypeHint(Class<? extends JajaExpr> typeHint) {

    }
}
