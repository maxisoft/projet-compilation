package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.JajaExpr;

/**
 * Created by maxime on 17/12/2014.
 */
public interface ExprBuilderWithTypeHint extends ExprBuilder{
    void setTypeHint(Class<? extends JajaExpr> typeHint);
}
