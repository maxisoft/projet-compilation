/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import fr.univfcomte.edu.ast.expr.JajaNotSetExpr;
import visitor.AstVisitor;


/**
 *
 * @author olivier
 */
public class JajaVar extends JajaDecl {

    private JajaExpr valueExpr;

    public JajaVar() {
        valueExpr = new JajaNotSetExpr();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }
    
    public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    public JajaExpr getValueExpr() {
        return valueExpr;
    }


    public void setValueExpr(JajaExpr valueExpr) {
        this.valueExpr = valueExpr;
    }

    @Override
    public String toString() {
        String res = super.toString();
        if (valueExpr != null) {
            res += " \tvalueExpr=\n" + valueExpr.toString() + '}';
        }
        return res;
    }

   
}
