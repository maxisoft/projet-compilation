/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaCallExpr extends JajaPrimExpr {

    private JajaExprs jajaParams;
    private JajaMeth jajaMeth;

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    @Override
    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    @Override
    public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    public JajaExprs getJajaParams() {
        return jajaParams;
    }

    public String getIdent() {
        return getValue();
    }

    public JajaMeth getJajaMeth() {
        return jajaMeth;
    }

    public void setJajaParams(JajaExprs jajaParams) {
        this.jajaParams = jajaParams;
    }

    public void setIdent(String ident) {
        setValue(ident);
    }

    public void setJajaMeth(JajaMeth jajaMeth) {
        this.jajaMeth = jajaMeth;
    }
}
