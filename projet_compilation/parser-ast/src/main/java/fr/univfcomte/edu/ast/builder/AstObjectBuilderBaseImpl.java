package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.AstObject;

/**
 * @author maxime
 */
public abstract class AstObjectBuilderBaseImpl implements AstObjectBuilder {

    private Integer line;
    private Integer column;


    @Override
    public Integer getLine() {
        return line;
    }

    @Override
    public AstObjectBuilder setLine(Integer line) {
        this.line = line;
        return this;
    }

    @Override
    public Integer getColumn() {
        return column;
    }

    @Override
    public AstObjectBuilder setColumn(Integer column) {
        this.column = column;
        return this;
    }

    protected void updateAttributes(AstObject astObject) {
        if (getLine() != null) {
            astObject.setLine(getLine());
        }
        if (getColumn() != null) {
            astObject.setColumn(getColumn());
        }

    }
}
