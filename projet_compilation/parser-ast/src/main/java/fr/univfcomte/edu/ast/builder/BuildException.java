package fr.univfcomte.edu.ast.builder;

import static java.util.Objects.requireNonNull;

/**
 * @author maxime
 */
public class BuildException extends RuntimeException{
    private final AstObjectBuilder builder;

    public BuildException(AstObjectBuilder builder) {
        this.builder = requireNonNull(builder);
    }

    public BuildException(String message, AstObjectBuilder builder) {
        super(message);
        this.builder = builder;
    }

    public BuildException(String message, Throwable cause, AstObjectBuilder builder) {
        super(message, cause);
        this.builder = builder;
    }

    public BuildException(Throwable cause, AstObjectBuilder builder) {
        super(cause);
        this.builder = builder;
    }

    public BuildException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, AstObjectBuilder builder) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.builder = builder;
    }

    public AstObjectBuilder getBuilder() {
        return builder;
    }
}
