package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.AstObject;

/**
 * @author maxime
 */
public interface AstObjectBuilder {
    Integer getLine();

    AstObjectBuilder setLine(Integer line);

    Integer getColumn();

    AstObjectBuilder setColumn(Integer column);

    AstObject build();
}
