package fr.univfcomte.edu.ast.expr.annotations;

import java.lang.annotation.*;

/**
 * @author maxime
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Expr {
    String value();
    boolean unary() default false;
}
