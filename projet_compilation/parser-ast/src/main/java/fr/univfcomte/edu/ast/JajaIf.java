/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;


/**
 *
 * @author olivier
 */
public class JajaIf extends JajaInstr {

    private JajaInstrs jajaInstrs;
    private JajaExpr expr = null;
    private JajaElse jajaElse = null;

    public JajaIf() {
        jajaInstrs = new JajaInstrs();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }
    
     public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    public JajaInstrs getJajaInstrs() {
        return jajaInstrs;
    }

    public JajaExpr getExpr() {
        return expr;
    }

    public JajaElse getJajaElse() {
        if (jajaElse == null) {
            jajaElse = new JajaElse();
        }
        return jajaElse;
    }

    public void setJajaInstrs(JajaInstrs jajaInstrs) {
        this.jajaInstrs = jajaInstrs;
    }

    public void setExpr(JajaExpr expr) {
        this.expr = expr;
    }

    public void setJajaElse(JajaElse jajaElse) {
        this.jajaElse = jajaElse;
    }

   
}
