package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.JajaExpr;
import fr.univfcomte.edu.ast.JajaExprs;

import java.util.List;

/**
 * @author maxime
 */
public interface ExprBuilder extends AstObjectBuilder {
    @Override
    JajaExpr build();

    JajaExprs getSonsExprs();

    ExprBuilder setSonsExprs(JajaExprs sonsExprs);

    ExprBuilder addSonExprs(JajaExpr sonExpr);

    ExprBuilder addSonExprs(ExprBuilder sonExprBuilder);

    String getValue();

    ExprBuilder setValue(String value);

    String getOperator();

    ExprBuilder setOperator(String operator);



}
