/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaMeth extends JajaDecl {

    private JajaParams params;
    private JajaInstrs jajaInstrs;
    private JajaVars jajaVars;

    public JajaMeth() {
        params = new JajaParams();
        jajaInstrs = new JajaInstrs();
        jajaVars = new JajaVars();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }

    public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    public JajaParams getParams() {
        return params;
    }

    public JajaInstrs getJajaInstrs() {
        return jajaInstrs;
    }

    public JajaVars getJajaVars() {
        return jajaVars;
    }

    public void setParams(JajaParams params) {
        this.params = params;
    }

    public void setJajaInstrs(JajaInstrs jajaInstrs) {
        this.jajaInstrs = jajaInstrs;
    }

    public void setJajaVars(JajaVars jajaVars) {
        this.jajaVars = jajaVars;
    }

}
