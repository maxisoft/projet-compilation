/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;


/**
 *
 * @author olivier
 */
public class JajaMain extends AstObject {

    private JajaVars jajaVars;
    private JajaInstrs jajaInstrs;

    /**
     * ****************
     * CONSTRUCTORS ***************
     */
    public JajaMain() {
        jajaVars = new JajaVars();
        jajaInstrs = new JajaInstrs();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }
    
     public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
       return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    /**
     * ****************
     * GETTERS ***************
     */
    public JajaVars getJajaVars() {
        return jajaVars;
    }

    public JajaInstrs getJajaInstrs() {
        return jajaInstrs;
    }

    /**
     * ****************
     * SETTERS ***************
     */
    public void setJajaVars(JajaVars jajaVars) {
        this.jajaVars = jajaVars;
    }

    public void setJajaInstrs(JajaInstrs jajaInstrs) {
        this.jajaInstrs = jajaInstrs;
    }

    
  
}
