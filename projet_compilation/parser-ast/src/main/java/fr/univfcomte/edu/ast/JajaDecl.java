/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;


/**
 *
 * @author olivier
 */
public abstract class JajaDecl extends AstObject {

    protected String identifier;
    private boolean finalDecl;
    protected JajaType type;

    public JajaDecl() {

    }

    public String getIdentifier() {
        return identifier;
    }

    public JajaType getType() {
        return type;
    }

    public String getTypeString() {
        return type.toString().toLowerCase();
    }

    public boolean isFinalDecl() {
        return finalDecl;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setFinalDecl(boolean finalDecl) {
        this.finalDecl = finalDecl;
    }

    void setType(JajaType jajaType) {
        this.type = jajaType;
    }

    void setType(String image) {
        this.type = JajaType.valueOf(image.toUpperCase());
    }

    @Override
    public String toString() {
        boolean var = this instanceof JajaVar;
        return "JajaDecl{" + "line=" + line + ", column=" + column + ", identifier=" + identifier + ",    " + var + "}\n";
    }
    
}
