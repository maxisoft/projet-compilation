package fr.univfcomte.edu.ast.expr.helpers;

import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.JajaExpr;
import fr.univfcomte.edu.ast.expr.annotations.Expr;
import org.reflections.Reflections;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author maxime
 */
public class OperatorToExpr {
    private static OperatorToExpr instance;
    public static synchronized OperatorToExpr getInstance() {
        if (instance == null){
            instance = new OperatorToExpr();
        }
        return instance;
    }

    private final Map<String, Class<? extends JajaExpr>> unaryOpMap;
    private final Map<String, Class<? extends JajaExpr>> binaryOpMap;

    protected OperatorToExpr() {
        this.unaryOpMap = new HashMap<>();
        this.binaryOpMap = new HashMap<>();
        analyseClasspath();
    }

    public Map<String, Class<? extends JajaExpr>> getUnaryOpMap() {
        return Collections.unmodifiableMap(unaryOpMap);
    }

    public Map<String, Class<? extends JajaExpr>> getBinaryOpMap() {
        return Collections.unmodifiableMap(binaryOpMap);
    }

    @SuppressWarnings("unchecked")
    public synchronized void analyseClasspath() {
        Reflections reflections = new Reflections(AstObject.class.getPackage().getName());
        Set<Class<?>> annotatedExprClasses = reflections.getTypesAnnotatedWith(Expr.class);
        for (Class<?> klass : annotatedExprClasses) {
            Expr annotation = klass.getAnnotation(Expr.class);
            Map<String, Class<? extends JajaExpr>> targetMap = annotation.unary() ? unaryOpMap : binaryOpMap;

            if (JajaExpr.class.isAssignableFrom(klass)) {
                boolean alreadyInMap = targetMap.put(annotation.value(), (Class<? extends JajaExpr>) klass) != null;
                if (alreadyInMap) {
                    throw new RuntimeException(String.format("There's already an Expr with the operator \"%s\"",
                            annotation.value()));
                }
            } else {
                throw new IllegalStateException(String.format("Class \"%s\" don't inherit from \"%s\"",
                        klass.getCanonicalName(),
                        JajaExpr.class.getCanonicalName()));
            }
        }
    }
}
