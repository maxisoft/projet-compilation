/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaCall extends JajaInstr {

    private JajaExprs jajaParams;
    private String ident;
    private JajaMeth jajaMeth;

    public JajaCall() {
        jajaParams = new JajaExprs();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaType accept(AstVisitor<? extends JajaType, Void> visitor) {
        return visitor.visit(this, null);
    }

    public Object accept(AstVisitor<? extends Object, Object> visitor, Object data) {
        return visitor.visit(this, data);
    }

    @Override
    public ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data) {
        return visitor.visit(this, data);
    }

    public JajaExprs getJajaParams() {
        return jajaParams;
    }

    public String getIdent() {
        return ident;
    }

    public JajaMeth getJajaMeth() {
        return jajaMeth;
    }

    public void setJajaParams(JajaExprs jajaParams) {
        this.jajaParams = jajaParams;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public void setJajaMeth(JajaMeth jajaMeth) {
        this.jajaMeth = jajaMeth;
    }
}
