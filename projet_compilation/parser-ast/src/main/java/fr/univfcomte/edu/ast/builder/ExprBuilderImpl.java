package fr.univfcomte.edu.ast.builder;

import fr.univfcomte.edu.ast.JajaExpr;
import fr.univfcomte.edu.ast.JajaExprs;
import fr.univfcomte.edu.ast.JajaPrimExpr;
import fr.univfcomte.edu.ast.expr.JajaNotSetExpr;
import fr.univfcomte.edu.ast.expr.helpers.OperatorToExpr;

import java.util.Map;

/**
 * @author maxime
 */
public class ExprBuilderImpl extends AstObjectBuilderBaseImpl implements ExprBuilderWithTypeHint {
    private JajaExprs sonsExprs;
    private String value;
    private String operator;
    private Class<? extends JajaExpr> typeHint;
    private Throwable buildException;
    private OperatorToExpr op2Expr;
    private boolean throwExceptionIfBuildFail = true;

    public ExprBuilderImpl() {
        this.op2Expr = OperatorToExpr.getInstance();
        sonsExprs = new JajaExprs();
    }

    @Override
    public JajaExprs getSonsExprs() {
        return sonsExprs;
    }

    @Override
    public ExprBuilder addSonExprs(JajaExpr sonExpr) {
        getSonsExprs().add(sonExpr);
        return this;
    }

    @Override
    public ExprBuilder addSonExprs(ExprBuilder sonExprBuilder) {
        return addSonExprs(sonExprBuilder.build());
    }

    public Class<? extends JajaExpr> deduceType() {
        if (typeHint != null) {
            return getTypeHint();
        }
        if (getOperator() == null) {
            return getValue() != null ? JajaPrimExpr.class : JajaNotSetExpr.class ;
        }
        Map<String, Class<? extends JajaExpr>> targetMap = isUnary() ?
                op2Expr.getUnaryOpMap() :
                op2Expr.getBinaryOpMap();
        return targetMap.get(getOperator());
    }

    private boolean isUnary() {
        return getOperator() != null && getSonsExprs().size() == 1;
    }

    public ExprBuilder setSonsExprs(JajaExprs sonsExprs) {
        this.sonsExprs = sonsExprs;
        return this;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public ExprBuilder setValue(String value) {
        this.value = value;
        return this;
    }

    @Override
    public String getOperator() {
        return operator;
    }

    @Override
    public ExprBuilder setOperator(String operator) {
        this.operator = operator;
        return this;
    }

    public Class<? extends JajaExpr> getTypeHint() {
        return typeHint;
    }

    @Override
    public void setTypeHint(Class<? extends JajaExpr> typeHint) {
        this.typeHint = typeHint;
    }

    public Throwable getBuildException() {
        return buildException;
    }

    public boolean isThrowExceptionIfBuildFail() {
        return throwExceptionIfBuildFail;
    }

    public void setThrowExceptionIfBuildFail(boolean throwExceptionIfBuildFail) {
        this.throwExceptionIfBuildFail = throwExceptionIfBuildFail;
    }

    protected void updateAttributes(JajaExpr astObject) {
        super.updateAttributes(astObject);
        astObject.setSonsExprs(getSonsExprs());
        astObject.setValue(getValue());
        astObject.setOperator(getOperator());
    }

    @Override
    public JajaExpr build() {
        Class<? extends JajaExpr> targetCls = deduceType();
        JajaExpr ret = null;
        buildException = null;
        try {
            ret = targetCls.newInstance();
            updateAttributes(ret);
        } catch (Exception e) {
            handleBuildException(e);
        }
        return ret;
    }

    private void handleBuildException(Exception e){
        buildException = e;
        if (isThrowExceptionIfBuildFail()){
            throw new BuildException(e, this);
        }
    }
}
