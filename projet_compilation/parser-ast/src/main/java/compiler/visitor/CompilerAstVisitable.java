package compiler.visitor;

import visitor.AstVisitor;

/**
 * An interface that define an accept method according to the compiler's visitor pattern.<br>
 * Based on {@link visitor.AstVisitable}
 * @author maxime
 */
public interface CompilerAstVisitable{
    ReturnInterface accept(AstVisitor<? extends ReturnInterface, DataInterface> visitor, DataInterface data);
}
