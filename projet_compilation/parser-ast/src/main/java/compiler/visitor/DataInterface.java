package compiler.visitor;

import fr.univfcomte.edu.ast.AstObject;

import java.io.Serializable;

/**
 * @author maxime
 */
public interface DataInterface extends Serializable {
    Integer getParamIndex();
    AstObject getParent();
}
