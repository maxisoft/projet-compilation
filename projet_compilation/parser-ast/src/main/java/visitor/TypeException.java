/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import fr.univfcomte.edu.ast.AstObject;

/**
 *
 * @author olivier
 */
public class TypeException extends RuntimeException {

    private AstObject astObject;

    public TypeException() {
        this("", null);
    }

    public TypeException(String message, AstObject astObject) {
        super(message);
        this.astObject = astObject;
    }

    public AstObject getAstObject() {
        return astObject;
    }
}
