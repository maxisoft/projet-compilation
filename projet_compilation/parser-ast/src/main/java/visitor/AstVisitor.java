package visitor;

import fr.univfcomte.edu.ast.*;
import fr.univfcomte.edu.ast.expr.*;

/**
 * @param <R> the return type of this visitor's methods. Use {@link
 *            Void} for visitors that do not need to return results.
 * @param <P> the type of the additional parameter to this visitor's methods.
 * Use {@link Void} for visitors that do not need an additional parameter.
 *
 * @author maxime
 */
public interface AstVisitor<R, P> {

    R visit(JajaAffect visitable, P data);

    R visit(JajaCall visitable, P data);

    R visit(JajaCallExpr visitable, P data);

    R visit(JajaClass visitable, P data);

    R visit(JajaDecls visitable, P data);

    R visit(JajaElse visitable, P data);

    R visit(JajaIf visitable, P data);

    R visit(JajaIncrement visitable, P data);

    R visit(JajaMain visitable, P data);

    R visit(JajaMeth visitable, P data);

    R visit(JajaParam visitable, P data);

    R visit(JajaReturn visitable, P data);

    R visit(JajaVar visitable, P data);

    R visit(JajaVars visitable, P data);

    R visit(JajaWhile visitable, P data);

    R visit(JajaAnd jajaAnd, P data);

    R visit(JajaDiv jajaDiv, P data);

    R visit(JajaEq jajaEq, P data);

    R visit(JajaMinus jajaMinus, P data);

    R visit(JajaMult jajaMult, P data);

    R visit(JajaNot jaJaNot, P data);

    R visit(JajaOr jajaOr, P data);

    R visit(JajaPlus jajaPlus, P data);

    R visit(JajaSup jajaSup, P data);

    R visit(JajaUnaryMinus jajaUnaryMinus, P data);

    R visit(JajaInstrs jajaInstrs, P data);

    R visit(JajaExprs jajaExprs, P data);

    R visit(JajaNotSetExpr jajaNotSetExpr, P data);

    R visit(JajaParams jajaParams, P data);

    R visit(JajaBoolean jajaBoolean, P data);

    R visit(JajaIdent jajaIdent, P data);

    R visit(JajaIdentArray jajaIdentArray, P data);

    R visit(JajaNumber jajaNumber, P data);

    R visit(JajaVarArray jajaVarArray, P data);

    R visit(JajaAffectArray jajaAffectArray, P data);

}
