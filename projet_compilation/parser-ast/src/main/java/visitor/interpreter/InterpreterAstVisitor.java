/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor.interpreter;

import visitor.AstVisitor;

/**
 *
 * @author IkramAhmed
 */
public interface InterpreterAstVisitor {
     public Object accept(AstVisitor<? extends Object, Object> visitor, Object data);
}
