package gui.astviewer;

import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import visitor.AstVisitor;

/**
 * An interface that define an accept method according to the compiler's visitor pattern.<br>
 * Based on {@link visitor.AstVisitable}
 * @author maxime
 */
public interface AstViewerAstVisitable {
    public void accept(AstVisitor<? extends Void, String> visitor, String data);
}
