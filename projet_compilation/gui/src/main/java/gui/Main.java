package gui;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;


public class Main extends Application {

    private Controller controller;
    private FXMLLoader fxmlLoader;
    private File file;

  

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage, File file) throws Exception {
        this.file = file;
        start(primaryStage);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL fxmlLocation = getClass().getClassLoader().getResource("main.fxml");
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(fxmlLocation);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = fxmlLoader.load();

        controller = fxmlLoader.getController();

        primaryStage.setTitle("Compilateur MiniJaja-JajaCode");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
    
}
