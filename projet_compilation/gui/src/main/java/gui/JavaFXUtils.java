package gui;

import javafx.application.Platform;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Utils class for javafx and related.
 *
 * @author maxime
 */
public final class JavaFXUtils {
    private JavaFXUtils() {
    }

    public static void runAndWait(Runnable runnable) throws InterruptedException, ExecutionException {
        FutureTask future = new FutureTask<>(runnable, null);
        Platform.runLater(future);
        future.get();
    }

    public static <T> FutureTask<T> runAsync(Callable<T> callable) {
        FutureTask<T> future = new FutureTask<>(callable);
        Platform.runLater(future);
        return future;
    }

    public static <T> T runAndWait(Callable<T> callable) throws ExecutionException, InterruptedException {
        return runAsync(callable).get();
    }
}
