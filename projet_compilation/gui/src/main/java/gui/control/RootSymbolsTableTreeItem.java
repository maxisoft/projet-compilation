package gui.control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.collections.SetChangeListener;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import memory.Memory;
import memory.Quadruplet;

import java.util.Collections;
import java.util.TreeMap;

/**
 * @author maxime
 */
public class RootSymbolsTableTreeItem extends TreeItem<Quadruplet> {
    final private ObservableMap<String, TreeItem<Quadruplet>> symbolTreeItem = FXCollections.observableMap(Collections.synchronizedMap(new TreeMap<>()));
    final private Memory memory;

    public RootSymbolsTableTreeItem(Memory memory) {
        this.memory = memory;
        bindIdents();
    }

    public RootSymbolsTableTreeItem(Quadruplet value, Memory memory) {
        super(value);
        this.memory = memory;
        bindIdents();
    }

    public RootSymbolsTableTreeItem(Quadruplet value, Node graphic, Memory memory) {
        super(value, graphic);
        this.memory = memory;
        bindIdents();
    }

    private void bindIdents() {
        memory.getIdents().addListener((SetChangeListener<String>) c -> {
            String s = c.getElementAdded();
            if (s != null) {
                TreeItem<Quadruplet> quadrupletTreeItem = symbolTreeItem.get(s);
                if (quadrupletTreeItem == null) {
                    quadrupletTreeItem = new IntermediateSymbolsTableTreeItem(memory.getSymbols(s));
                    symbolTreeItem.put(s, quadrupletTreeItem);
                    getChildren().add(quadrupletTreeItem);
                }
            }

            s = c.getElementRemoved();
            if (s != null) {
                TreeItem<Quadruplet> quadrupletTreeItem = symbolTreeItem.get(s);
                if (quadrupletTreeItem != null &&
                        (quadrupletTreeItem.getChildren().size() == 1 || quadrupletTreeItem.getChildren().isEmpty())) {
                    symbolTreeItem.remove(s);
                    getChildren().remove(quadrupletTreeItem);
                } else if (quadrupletTreeItem == null) {
                    symbolTreeItem.remove(s);
                }
            }
        });
    }
}
