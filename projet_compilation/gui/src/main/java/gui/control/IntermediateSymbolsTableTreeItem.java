package gui.control;

import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import memory.Quadruplet;
import memory.Quadruplets;
import org.fxmisc.easybind.EasyBind;

/**
 * @author maxime
 */
public class IntermediateSymbolsTableTreeItem extends TreeItem<Quadruplet> {

    public IntermediateSymbolsTableTreeItem(Quadruplets quadruplets) {
        super(createQuadFromIdent(quadruplets));
        bindToQuadruplets(quadruplets);
    }

    public IntermediateSymbolsTableTreeItem(Node graphic, Quadruplets quadruplets) {
        super(createQuadFromIdent(quadruplets), graphic);
        bindToQuadruplets(quadruplets);
    }

    public static Quadruplet createQuadFromIdent(Quadruplets quadruplets) {
        return new Quadruplet(quadruplets.getIdentObjs(), null, null, null);
    }

    private void bindToQuadruplets(Quadruplets quadruplets){
        EasyBind.listBind(getChildren(), EasyBind.map(quadruplets.getQuadruplets(), TreeItem::new));
    }
}
