package gui.editor;

import java.util.*;

/**
 * Class that define shared objects between java and javascript.<p>
 * Used by the {@link CodeEditor}.

 *
 * @author maxime
 */
@SuppressWarnings("unused")
public class JavaScriptInterface {
    private String code;
    private String clipboardContent;
    private Set<Integer> breakpoints;

    public JavaScriptInterface(String code) {
        this.code = code;
        breakpoints = Collections.synchronizedSet(new TreeSet<>());
    }

    public JavaScriptInterface() {
        this("");
    }

    public synchronized String getCode() {
        return code;
    }

    public synchronized void setCode(String code) {
        this.code = code;
    }

    public String getClipboardContent() {
        return clipboardContent;
    }

    public void setClipboardContent(String clipboardContent) {
        this.clipboardContent = clipboardContent;
    }

    public boolean addBreakPoint(int line){
        return breakpoints.add(line);
    }

    public boolean removeBreakPoint(int line){
        return breakpoints.remove(line);
    }

    public Set<Integer> getBreakpoints() {
        return breakpoints;
    }
}
