package gui.editor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.scene.input.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;

import java.util.Collections;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * @author maxime
 */
public class CodeEditor {
    private final static ObjectMapper mapper = new ObjectMapper();
    final private WebView webView;
    final private JavaScriptInterface javaScriptInterface;
    private BooleanProperty loaded = new SimpleBooleanProperty(false);
    private BooleanProperty readOnly;
    private Annotation annotation;

    public CodeEditor(WebView webView) {
        this(webView, false);
    }

    public CodeEditor(WebView webView, boolean readOnly) {
        this(webView, new SimpleBooleanProperty(readOnly));
    }

    public CodeEditor(WebView webView, BooleanProperty readOnly) {
        this.readOnly = readOnly;
        this.webView = requireNonNull(webView);
        javaScriptInterface = new JavaScriptInterface();
        injectInterface();
        this.webView.addEventFilter(KeyEvent.KEY_PRESSED, this::handleCopyPaste);
        //noinspection ConstantConditions
        getEngine().load(getClass().getClassLoader().getResource("ace/editor.html").toExternalForm());
        getEngine().getLoadWorker().stateProperty().addListener(
                (ObservableValue<? extends Worker.State> ov, Worker.State oldState,
                 Worker.State newState) -> {
                    if (newState == Worker.State.SUCCEEDED) {
                        onWebViewReady();
                    }
                });
    }


    protected void onWebViewReady() {
        try {
            loadCode("");
        } catch (JSException ignore) {
        }
        //bind readOnly Property to javascript editor
        readOnly.addListener((observable, oldValue, newValue) -> updateEditorReadOnly(newValue));
        //call it
        try {
            updateEditorReadOnly(readOnly.get());
        } catch (JSException ignore) {
        }


        loaded.setValue(true);
    }

    public WebView getWebView() {
        return webView;
    }

    public JavaScriptInterface getJavaScriptInterface() {
        return javaScriptInterface;
    }

    public void loadCode(String code) {
        javaScriptInterface.setCode(code);
        executeJavascript("editor.setValue($ji.getCode(), -1)");
    }

    public String getCode() {
        executeJavascript("$ji.setCode(editor.getValue())");
        return javaScriptInterface.getCode();
    }

    public void setDebugMarker(Integer line) {
        executeJavascript("setDebugMarker(" + line + ")");
    }

    public void removeDebugMarker() {
        setDebugMarker(null);
    }

    private void updateAnnotation() throws JsonProcessingException {
        executeJavascript("editor.session.clearAnnotations()");
        if (annotation != null) {
            executeJavascript("editor.session.setAnnotations(" +
                    mapper.writeValueAsString(new Annotation[]{annotation}) +
                    ")");
        } else {
            executeJavascript("editor.session.setAnnotations([])");
        }
    }

    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
        try {
            updateAnnotation();
        } catch (JsonProcessingException e) {
            e.printStackTrace(); //TODO
        }
    }

    public WebEngine getEngine() {
        return webView.getEngine();
    }

    private void injectInterface() {
        JSObject win = executeJavascript("window");
        win.setMember("$ji", javaScriptInterface);
    }

    @SuppressWarnings("unchecked")
    public <T> T executeJavascript(String code) {
        return (T) getEngine().executeScript(code);
    }

    private void handleCopyPaste(KeyEvent event) {
        if (event.getCode().equals(KeyCode.C) || event.getCode().equals(KeyCode.X) && event.isControlDown()) {
            String selectedText = executeJavascript("editor.session.getTextRange(editor.getSelectionRange())");
            if (selectedText == null || selectedText.isEmpty()) {
                return;
            }
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final ClipboardContent content = new ClipboardContent();
            content.putString(selectedText);
            clipboard.setContent(content);
        } else if (event.getCode().equals(KeyCode.V) && event.isControlDown()) {
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            String content = (String) clipboard.getContent(DataFormat.PLAIN_TEXT);
            if (content != null) {
                javaScriptInterface.setClipboardContent(content);
                executeJavascript("editor.insert($ji.getClipboardContent())");
                javaScriptInterface.setClipboardContent("");
            }
        }
    }

    public boolean isLoaded() {
        return loaded.get();
    }

    public void setReadOnly(boolean b) {
        readOnly.setValue(b);
    }

    private void updateEditorReadOnly(boolean b) {
        executeJavascript("editor.setReadOnly(" + String.valueOf(b) + ");");
    }

    public Set<Integer> getBreakpoints(){
        return Collections.unmodifiableSet(getJavaScriptInterface().getBreakpoints());
    }

    public void focusLine(int line){
        executeJavascript("focusLine(" + line + ")");
    }

}
