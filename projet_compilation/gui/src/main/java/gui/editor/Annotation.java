package gui.editor;

/**
* @author maxime
*/
public class Annotation {
    private int row;
    private int column;
    private String text;
    private String type;

    public Annotation(int row, int column, String text, String type) {
        this.row = row;
        this.column = column;
        this.text = text;
        this.type = type;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
