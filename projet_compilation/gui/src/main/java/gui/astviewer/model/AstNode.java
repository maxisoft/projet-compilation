package gui.astviewer.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AstNode {

    private StringProperty type;
    private StringProperty value;
    private StringProperty operator;

    public String getType() {
        return typeProperty().get();
    }

    public void setType(String value) {
        typeProperty().set(value);
    }

    public StringProperty typeProperty() {
        if (type == null) type = new SimpleStringProperty(this, "");
        return type;
    }

    public String getValue() {
        return valueProperty().get();
    }

    public void setValue(String value) {
        valueProperty().set(value);
    }

    public StringProperty valueProperty() {
        if (value == null) value = new SimpleStringProperty(this, "");
        return value;
    }

    public String getOperator() {
        return operatorProperty().get();
    }

    public void setOperator(String value) {
        operatorProperty().set(value);
    }

    public StringProperty operatorProperty() {
        if (operator == null) operator = new SimpleStringProperty(this, "");
        return operator;
    }
}
