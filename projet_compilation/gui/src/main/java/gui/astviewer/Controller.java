package gui.astviewer;

import gui.astviewer.model.AstNode;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;

public class Controller {
    @FXML
    private TreeTableView<AstNode> astTreeTableView;

    public TreeTableView<AstNode> getAstTreeTableView() {
        return astTreeTableView;
    }

    @FXML
    @SuppressWarnings("unchecked")
    private void initialize(){
        ObservableList<TreeTableColumn<AstNode, ?>> columns = astTreeTableView.getColumns();
        columns.forEach(column -> {
            switch (column.getText()) {
                case "AstType":
                    column.setCellValueFactory(new TreeItemPropertyValueFactory("type"));
                    break;
                case "Id/Value":
                    column.setCellValueFactory(new TreeItemPropertyValueFactory("value"));
                    break;
                case "Type/Operator":
                    column.setCellValueFactory(new TreeItemPropertyValueFactory("operator"));
                    break;
            }
        });
    }
}
