package gui.astviewer;

import com.google.common.io.Files;
import fr.univfcomte.edu.analyzer.ParseException;
import fr.univfcomte.edu.ast.Analyzer;
import fr.univfcomte.edu.ast.JajaClass;
import gui.astviewer.model.AstNode;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

public class AstViewer extends Application {


    public static Stage showAst(String code) throws IOException, ParseException {
        return showAst(code, new Stage());
    }
    
    @SuppressWarnings("unchecked")
    public static Stage showAst(String code, Stage primaryStage) throws IOException, ParseException {
        URL fxmlLocation = AstViewer.class.getClassLoader().getResource("astviewer/main.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(fxmlLocation);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = fxmlLoader.load();

        Analyzer parser = new Analyzer(code);
        JajaClass astRoot = parser.getASTRoot();

        ConvertToJavaFXModel visitor = new ConvertToJavaFXModel();
        astRoot.accept(visitor, null);

        Controller controller = fxmlLoader.getController();

        TreeTableView<AstNode> astTreeTableView = controller.getAstTreeTableView();
        astTreeTableView.setRoot(visitor.getRoot());


        primaryStage.setTitle("AST");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        return primaryStage;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL resource = getClass().getClassLoader().getResource("input.txt");
        if (resource == null) {
            throw new NullPointerException("no input.txt file");
        }
        File file = new File(resource.toURI());
        String content = Files.toString(file, Charset.defaultCharset());
        showAst(content, primaryStage);
    }
}
