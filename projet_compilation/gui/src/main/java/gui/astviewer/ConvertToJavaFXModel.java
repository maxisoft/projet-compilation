package gui.astviewer;

import fr.univfcomte.edu.ast.*;
import fr.univfcomte.edu.ast.expr.*;
import gui.astviewer.model.AstNode;
import javafx.scene.control.TreeItem;
import visitor.AstVisitor;

import java.util.StringJoiner;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Strings.nullToEmpty;

/**
 * @author maxime
 */
public class ConvertToJavaFXModel implements AstVisitor<Void, String> {

    private TreeItem<AstNode> root;
    private TreeItem<AstNode> currentParent;

    private static TreeItem<AstNode> createAstNodeTreeItem(AstObject visitable, TreeItem<AstNode> parent) {
        AstNode astNode = new AstNode();
        astNode.setType(visitable.getClass().getSimpleName());
        TreeItem<AstNode> astNodeTreeItem = new TreeItem<>(astNode);
        if (parent != null) {
            parent.getChildren().add(astNodeTreeItem);
        }
        return astNodeTreeItem;
    }

    private static String prependString(String addit, String accu) {
        StringJoiner sj = new StringJoiner(" / ");
        sj.add(addit);
        if (!isNullOrEmpty(accu)) {
            sj.add(accu);
        }
        return sj.toString();
    }

    public static String appendString(String addit, String accu) {
        StringJoiner sj = new StringJoiner(" / ");
        if (!isNullOrEmpty(accu)) {
            sj.add(accu);
        }
        sj.add(addit);
        return sj.toString();
    }

    public TreeItem<AstNode> getRoot() {
        return root;
    }

    @Override
    public Void visit(JajaAffect visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setOperator(visitable.getOperator());
        node.getValue().setValue(visitable.getIdent());
        visitable.getExpr().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaCall visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setValue(visitable.getIdent());
        visitable.getJajaParams().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaClass visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        root = node;
        visitable.getJajaDecls().accept(this, data);
        visitable.getJajaMain().accept(this, data);
        vhelper.visitEnd();
        return null;

    }

    @Override
    public Void visit(JajaDecls visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setValue(appendString("size=" + visitable.size(), data));
        visitable.forEach(decl -> decl.accept(this, data));
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaElse visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        visitable.getJajaInstrs().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaIf visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        visitable.getExpr().accept(this, data);
        visitable.getJajaInstrs().accept(this, data);
        JajaElse jajaElse = visitable.getJajaElse();
        if (jajaElse != null) {
            jajaElse.accept(this, data);
        }
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaIncrement visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setValue(visitable.getIdent());
        node.getValue().setOperator(visitable.getOperator());
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaMain visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setValue("main");
        visitable.getJajaVars().accept(this, data);
        visitable.getJajaInstrs().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaMeth visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setValue(visitable.getIdentifier());
        visitable.getParams().accept(this, data);
        visitable.getJajaVars().accept(this, nullToEmpty(data));
        visitable.getJajaInstrs().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaParam visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        AstNode value = node.getValue();
        value.setValue(visitable.getIdentifier());
        value.setOperator(visitable.getTypeString());
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaReturn visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        visitable.getExpr().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaVar visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        AstNode value = node.getValue();
        value.setValue(visitable.getIdentifier());
        value.setOperator(visitable.getTypeString());
        JajaExpr valueExpr = visitable.getValueExpr();
        valueExpr.accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaVars visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setValue(appendString("size=" + visitable.size(), data));
        visitable.forEach(var -> var.accept(this, null));
        vhelper.visitEnd();
        return null;
    }

    private Void visitExpr(JajaExpr visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        AstNode value = node.getValue();
        value.setValue(nullToEmpty(visitable.getValue()));
        value.setOperator(visitable.getOperator());
        visitable.getSonsExprs().forEach(expr -> expr.accept(this, data));
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaWhile visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        visitable.getExpr().accept(this, data);
        visitable.getJajaInstrs().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaAnd jajaAnd, String data) {
        return visitExpr(jajaAnd, data);
    }

    @Override
    public Void visit(JajaDiv jajaDiv, String data) {
        return visitExpr(jajaDiv, data);
    }

    @Override
    public Void visit(JajaEq jajaEq, String data) {
        return visitExpr(jajaEq, data);
    }

    @Override
    public Void visit(JajaMinus jajaMinus, String data) {
        return visitExpr(jajaMinus, data);
    }

    @Override
    public Void visit(JajaMult jajaMult, String data) {
        return visitExpr(jajaMult, data);
    }

    @Override
    public Void visit(JajaNot jaJaNot, String data) {
        return visitExpr(jaJaNot, data);
    }

    @Override
    public Void visit(JajaOr jajaOr, String data) {
        return visitExpr(jajaOr, data);
    }

    @Override
    public Void visit(JajaPlus jajaPlus, String data) {
        return visitExpr(jajaPlus, data);
    }

    @Override
    public Void visit(JajaSup jajaSup, String data) {
        return visitExpr(jajaSup, data);
    }

    @Override
    public Void visit(JajaUnaryMinus jajaUnaryMinus, String data) {
        visitExpr(jajaUnaryMinus, data);
        return null;
    }

    @Override
    public Void visit(JajaInstrs jajaInstrs, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(jajaInstrs);
        node.getValue().setValue(appendString("size=" + jajaInstrs.size(), data));
        jajaInstrs.forEach(instr -> instr.accept(this, null));
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaExprs jajaExprs, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(jajaExprs);
        jajaExprs.forEach(expr -> expr.accept(this, data));
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaNotSetExpr jajaNotSetExpr, String data) {
        //DO NOTHING
        return null;
    }

    @Override
    public Void visit(JajaParams jajaParams, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(jajaParams);
        node.getValue().setValue(appendString("size=" + jajaParams.size(), data));
        jajaParams.forEach(param -> param.accept(this, null));
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaBoolean jajaBoolean, String data) {
        return visitExpr(jajaBoolean, data);
    }

    @Override
    public Void visit(JajaIdent jajaIdent, String data) {
        return visitExpr(jajaIdent, data);
    }

    @Override
    public Void visit(JajaNumber jajaNumber, String data) {
        return visitExpr(jajaNumber, data);
    }

    @Override
    public Void visit(JajaVarArray jajaVarArray, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(jajaVarArray);
        AstNode value = node.getValue();
        value.setValue(jajaVarArray.getIdentifier());
        value.setOperator(jajaVarArray.getTypeString());
        jajaVarArray.getIndexExpr().accept(this, data);
        JajaExpr valueExpr = jajaVarArray.getValueExpr();
        valueExpr.accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaAffectArray jajaAffectArray, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(jajaAffectArray);
        node.getValue().setOperator(jajaAffectArray.getOperator());
        node.getValue().setValue(jajaAffectArray.getIdent());
        jajaAffectArray.getIndexExpr().accept(this, data);
        jajaAffectArray.getExpr().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaCallExpr visitable, String data) {
        VisitorHelper vhelper = new VisitorHelper();
        TreeItem<AstNode> node = vhelper.createNodeChild(visitable);
        node.getValue().setValue(visitable.getIdent());
        visitable.getJajaParams().accept(this, data);
        vhelper.visitEnd();
        return null;
    }

    @Override
    public Void visit(JajaIdentArray jajaIdentArray, String data) {
        return visitExpr(jajaIdentArray, data);
    }

    private class VisitorHelper {

        private TreeItem<AstNode> oldParent;

        public TreeItem<AstNode> createNodeChild(AstObject visitable) {
            oldParent = currentParent;
            currentParent = createAstNodeTreeItem(visitable, oldParent);
            return currentParent;
        }

        public TreeItem<AstNode> visitEnd() {
            currentParent = oldParent;
            return oldParent;
        }
    }
}
