package gui;

import com.google.common.io.Files;
import compiler.Compiler;
import compiler.model.Instr;
import fr.univfcomte.edu.analyzer.ParseException;
import fr.univfcomte.edu.ast.Analyzer;
import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.JajaClass;
import fr.univfcomte.edu.ast.builder.AstObjectBuilder;
import fr.univfcomte.edu.ast.builder.BuildException;
import gui.astviewer.AstViewer;
import gui.control.RootSymbolsTableTreeItem;
import gui.editor.Annotation;
import gui.editor.CodeEditor;
import interpreter.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import memory.Memory;
import memory.Quadruplet;
import org.controlsfx.dialog.ExceptionDialog;
import type_controler.TypeControler;
import visitor.TypeException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.ListIterator;
import java.util.StringJoiner;
import java.util.concurrent.Callable;
import memory.Data;

@SuppressWarnings("unused")
public class Controller {

    @FXML
    private TreeTableView<Quadruplet> HachageTreeTableView;
    @FXML
    private TreeTableView<Quadruplet> HachageTreeTableView1;

    @FXML
    private Button saveBtn;

    @FXML
    private WebView miniJajaCodeWebView;
    @FXML
    private WebView jajaCodeCodeWebView;
    @FXML
    private Button consolleClearBtn;
     @FXML
    private Button btnItemCompile;
    @FXML 
    private Button jajaCodeEvalNextButton;
    @FXML
    private Button jajaCodeEvalBlocButton;
    @FXML
    private Button jajaCodeRefreshButton;

    @FXML
    private TableView<Data> tasJajaCode;

    @FXML
    private TitledPane TPpile;

    @FXML
    private TitledPane dropcomparaison;
    @FXML
    private TableView<Quadruplet> pileTable;
    @FXML
    private TableView<Quadruplet> pileTable1;
    @FXML
    private TableColumn<Quadruplet, String> ident;
    @FXML
    private TableColumn<Quadruplet, String> value;
    @FXML
    private TableColumn<Quadruplet, String> object;
    @FXML
    private TableColumn<Quadruplet, String> type;
    @FXML
    private TreeTableColumn<Quadruplet, String> treeident;
    @FXML
    private TreeTableColumn<Quadruplet, String> treevaleur;
    @FXML
    private TreeTableColumn<Quadruplet, String> treeobj;
    @FXML
    private TableColumn<Data, String> identTas;
    @FXML
    private TableColumn<Data, String> ValeurTas;
    
     @FXML
    private TableColumn<Quadruplet, String> identPileMjj;
    @FXML
    private TableColumn<Quadruplet, String> valuePileMjj;
    @FXML
    private TableColumn<Quadruplet, String> objectPileMjj;
    
    @FXML
    private TableColumn<Quadruplet, String> typePileMjj;

    @FXML
    private TreeTableColumn<Quadruplet, String> typeTsMjj;
    
    @FXML
    private TreeTableColumn<Quadruplet, String> identTsMjj;
    @FXML
    private TreeTableColumn<Quadruplet, String> valeurTsMjj;
    @FXML
    private TreeTableColumn<Quadruplet, String> objTsMjj;
    @FXML
    private TableColumn<Data, String> identTasMjj;
    @FXML
    private TableColumn<Data, String> valeurTasMjj;
    @FXML
    private TreeTableColumn<Quadruplet, String> treetype;
    private CodeEditor miniJajaCodeEditor;
    private CodeEditor jajaCodeEditor;
    private Delegate delegate;

    public TreeTableView<Quadruplet> getTreeTableView() {
        return HachageTreeTableView;
    }

    @FXML
    void save(ActionEvent event) throws ParseException, IOException {
    }

    @FXML
    void comparaisonMemoire(ActionEvent event) {
        dropcomparaison.setAnimated(true);
        dropcomparaison.setExpanded(true);
    }

    @FXML
    void buttonSave(ActionEvent event) throws ParseException, IOException {

        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try (FileWriter fileWriter = new FileWriter(file)) {
                fileWriter.write(miniJajaCodeEditor.getCode());
            } catch (IOException ex) {
                System.out.println("erreur d'enregistrement du fichier :" + ex);
            }
        }
    }

    @FXML
    void ouvrir(ActionEvent event) throws ParseException, IOException {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showOpenDialog(null);

        if (file != null) {
            miniJajaCodeEditor.loadCode(Files.toString(file, Charset.defaultCharset()));
        }
    }

    @FXML
    private void jajaCodeCompile(ActionEvent event) {
        try {
            delegate.jajaCodeCompile();
        } catch (Exception e) {
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.show();
        }

    }

    @FXML
    void ViewAst(ActionEvent event) {
        try {
            AstViewer.showAst(miniJajaCodeEditor.getCode());
        } catch (Exception e) {
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.show();
        }

    }

    @FXML
    private void fileDragDrop(DragEvent event) throws Exception {
        Dragboard dragboard = event.getDragboard();
        boolean success = false;
        if (dragboard.hasFiles()) {
            success = true;
            List<File> files = dragboard.getFiles();
            if (files != null && !files.isEmpty()) {
                miniJajaCodeEditor.loadCode(Files.toString(files.get(0), Charset.defaultCharset()));
            }
        }
        event.setDropCompleted(success);
        event.consume();
    }

    @FXML
    private void minijajaEvalNext(ActionEvent actionEvent) {
        try {
            delegate.minijajaEvalNext();
        } catch (Exception e) {
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.show();
        }
    }

    @FXML
    private void jajaCodeEvalNext(ActionEvent actionEvent) {
        try {
            delegate.jajaCodeEvalNext();
        } catch (Exception e) {
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.show();
        }
    }

    @FXML
    private void jajaCodeEvalUntilBreakpoint(ActionEvent actionEvent) {
        try {
            delegate.jajaCodeEvalUntilBreakpoint();
        } catch (Exception e) {
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.show();
        }
    }

    @FXML
    private void jajaCodeRefreshButton(ActionEvent event) {
        try {
            delegate.setJajaCodeInterpreter(null);
            delegate.jajaCodeCompile();
            delegate.jajaCodeEvalNext();
        } catch (Exception e) {
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.show();
        }

    }

    @FXML
    private void fileDragOver(DragEvent event) {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        } else {
            event.consume();
        }
    }

    @FXML
    @SuppressWarnings("unchecked")
    private void initialize() {
        miniJajaCodeEditor = new CodeEditor(miniJajaCodeWebView);
        jajaCodeEditor = new CodeEditor(jajaCodeCodeWebView, true);
        jajaCodeRefreshButton.setDisable(false);
        ident.setCellValueFactory(new PropertyValueFactory<>("ident"));
        identPileMjj.setCellValueFactory(new PropertyValueFactory<>("ident"));
        value.setCellValueFactory(new PropertyValueFactory<>("value"));
        valuePileMjj.setCellValueFactory(new PropertyValueFactory<>("value"));
        object.setCellValueFactory(new PropertyValueFactory<>("obj"));
        objectPileMjj.setCellValueFactory(new PropertyValueFactory<>("obj"));
        type.setCellValueFactory(new PropertyValueFactory<>("kind"));
        typePileMjj.setCellValueFactory(new PropertyValueFactory<>("kind"));

        treeident.setCellValueFactory(new TreeItemPropertyValueFactory<>("ident"));
        identTsMjj.setCellValueFactory(new TreeItemPropertyValueFactory<>("ident"));
        treevaleur.setCellValueFactory(new TreeItemPropertyValueFactory<>("value"));
        valeurTsMjj.setCellValueFactory(new TreeItemPropertyValueFactory<>("value"));
        treeobj.setCellValueFactory(new TreeItemPropertyValueFactory<>("obj"));
        objTsMjj.setCellValueFactory(new TreeItemPropertyValueFactory<>("obj"));
        treetype.setCellValueFactory(new TreeItemPropertyValueFactory<>("kind"));
        typeTsMjj.setCellValueFactory(new TreeItemPropertyValueFactory<>("kind"));
        
        
        identTas.setCellValueFactory(new PropertyValueFactory<>("ident"));
        ValeurTas.setCellValueFactory(new PropertyValueFactory<>("address"));

        delegate = new Delegate();
        TestLoadTas();
    }

    public void TestLoadTas() {
        // Button btn = new Button();
        // tilepaneTas.setPadding(new Insets(10, 10, 10, 10));
        // tilepaneTas.setVgap(4);
        // tilepaneTas.setHgap(4);

        // for (int i = 0; i < 500; i++) {

        //   btn = new Button(i + "\n Null");
        //  btn.setPrefSize(80, 30);
        //   btn.setStyle("-fx-font: 8 arial; -fx-base: #737272;");
        // btn.setTextAlignment(TextAlignment.CENTER);

        //  tilepaneTas.getChildren().add(btn);

        //  }


    }

    //
//        try {
//            List<Quadruplet> quadruplets = Arrays.<Quadruplet>asList(
//                    new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test1", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test2", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test2", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT),
//                    new Quadruplet("test", 5, Obj.VARIABLE, JajaType.INT));
//
//            TreeItem<String> rootNode = new TreeItem<String>("");
//            rootNode.setExpanded(true);
//
//            for (Quadruplet quadruplet : quadruplets) {
//                TreeItem<String> itemQuadruplet = new TreeItem<String>(quadruplet.toString());
//                boolean found = false;
//
//                for (TreeItem<String> depNode : rootNode.getChildren()) {
//                    if (depNode.getValue().contentEquals(quadruplet.getIdent())) {
//                        depNode.getChildren().add(itemQuadruplet);
//                        found = true;
//                        break;
//                    }
//                }
//
//                if (!found) {
//                    TreeItem<String> depNode = new TreeItem<String>(quadruplet.getIdent());
//                    rootNode.getChildren().add(depNode);
//                    depNode.getChildren().add(itemQuadruplet);
//                }
//            }
//            treeView.setRoot(rootNode);
//
//        } catch (Exception exc) {
//            System.out.println("Error: " + exc.getMessage());
//        }
//    }
    public Button getSaveBtn() {
        return saveBtn;
    }

    public CodeEditor getMiniJajaCodeEditor() {
        return miniJajaCodeEditor;
    }

    public CodeEditor getJajaCodeEditor() {
        return jajaCodeEditor;
    }

    public Button getConsoleClearBtn() {
        return consolleClearBtn;
    }

    public final class Delegate { //TODO rename

        private Interpreter jajaCodeInterpreter;
        private List<Instr> instructions;
        private InterpreterRunnable minijajaInterpreter;
        private Thread minijajaThread;

        public JajaClass getAstRoot() throws ParseException {
            Analyzer parser = new Analyzer(miniJajaCodeEditor.getCode());
            return parser.getASTRoot();
        }

        public void checkType(JajaClass root) {
            TypeControler typeControler = new TypeControler();
            root.accept(typeControler);
        }

        public JajaClass prepareMiniJajaCode() {
            CodeEditor editor = getMiniJajaCodeEditor();
            JajaClass astRoot = null;
            try {
                astRoot = getAstRoot();
                editor.setAnnotation(null);
            } catch (ParseException parseException) {
                Annotation annotation = new Annotation(parseException.currentToken.next.beginLine - 1,
                        parseException.currentToken.next.beginColumn - 1,
                        parseException.getMessage(),
                        "error");
                editor.setAnnotation(annotation);
            } catch (BuildException buildException) {
                AstObjectBuilder builder = buildException.getBuilder();
                Annotation annotation = new Annotation(builder.getLine() - 1,
                        builder.getColumn() - 1,
                        buildException.getMessage(),
                        "error");
                editor.setAnnotation(annotation);
            }

            if (astRoot != null) {
                try {
                    checkType(astRoot);
                } catch (TypeException typeException) {
                    AstObject astObject = typeException.getAstObject();
                    if (astObject != null && astObject.getLine() != null && astObject.getColumn() != null){
                        Annotation annotation = new Annotation(astObject.getLine() - 1,
                                astObject.getColumn() - 1,
                                typeException.getMessage(),
                                "error");
                        editor.setAnnotation(annotation);
                    }

                }catch (Exception e){
                    ExceptionDialog dlg = new ExceptionDialog(e);
                    dlg.show();
                }
            }
            return astRoot;
        }


        public void jajaCodeCompile() {
                JajaClass astRoot = prepareMiniJajaCode();

                Compiler compiler = new Compiler();

                astRoot.accept(compiler, null);
                setInstruction(compiler.getInstructions());

                jajaCodeEvalNextButton.setOpacity(1);
                jajaCodeEvalNextButton.setDisable(false);
                jajaCodeRefreshButton.setOpacity(0.9);
                jajaCodeEvalBlocButton.setDisable(false);
                jajaCodeEvalBlocButton.setOpacity(1);
                TPpile.setAnimated(true);
                TPpile.setExpanded(true);

        }

        public void jajaCodeEvalNext() {
            Interpreter interpreter = getJajaCodeInterpreter();
            try {
                interpreter.evalNextExpr();
                System.out.println(interpreter.getMemory().getIdents());
            } catch (StopInterpretation stopInterpretation) {
                setJajaCodeInterpreter(null);
                jajaCodeEvalNextButton.setOpacity(0.4);
                jajaCodeEvalNextButton.setDisable(true);
                jajaCodeRefreshButton.setDisable(false);
                jajaCodeRefreshButton.setOpacity(1);
            }
            jajaCodeEditor.setDebugMarker(interpreter.getPC() - 1);
            jajaCodeEditor.focusLine(interpreter.getPC() - 1);
        }

        public void jajaCodeEvalUntilBreakpoint() {
            do {
                jajaCodeEvalNext();
            } while (jajaCodeInterpreter != null &&
                    !jajaCodeEditor.getBreakpoints().contains(getJajaCodeInterpreter().getPC() - 1));
        }

        private void setInstruction(List<Instr> instructions) {
            this.instructions = instructions;

            //update the jajaCodeEditor
            StringJoiner stringJoiner = new StringJoiner("\n");
            ListIterator<Instr> it = this.instructions.listIterator();
            it.next(); //start at 1
            while (it.hasNext()) {
                stringJoiner.add(it.next().toJajaCode());
            }
            jajaCodeEditor.loadCode(stringJoiner.toString());
            jajaCodeEditor.setReadOnly(true);
        }

        public List<Instr> getInstructions() {
            if (instructions == null) {
                jajaCodeCompile();
            }
            return instructions;
        }

        public Memory getMiniJajaMemory() {
            //TODO
            return null;
        }

        public Memory getJajaCodeMemory() throws ParseException {
            if (jajaCodeInterpreter == null) {
                return null;
            }
            return getJajaCodeInterpreter().getMemory();
        }

        public Interpreter getJajaCodeInterpreter() {
            if (jajaCodeInterpreter == null) {
                setJajaCodeInterpreter(new InterpreterImpl(getInstructions()));
                //change the default PC value (1) in order to start @ 0
                jajaCodeInterpreter.setPC(0);
            }
            return jajaCodeInterpreter;
        }

        private void setJajaCodeInterpreter(Interpreter interpreter) {
            this.jajaCodeInterpreter = interpreter;
            if (interpreter != null) {
                pileTable.setItems(interpreter.getMemory().getPile());
                RootSymbolsTableTreeItem root = new RootSymbolsTableTreeItem(interpreter.getMemory());
                HachageTreeTableView.setRoot(root);
                tasJajaCode.setItems(interpreter.getMemory().getHeap().getDataTable().getTable());
            }
        }

        public InterpreterRunnable getMinijajaInterpreter() {
            if (minijajaInterpreter == null) {
                setminiJajaInterpreter(new InterpreterRunnable(prepareMiniJajaCode()));
            }
            return minijajaInterpreter;
        }

        public void minijajaEvalNext() {
            InterpreterRunnable interpreter = getMinijajaInterpreter();
            if (minijajaThread == null || !minijajaThread.isAlive()){

                minijajaThread = new Thread(interpreter);
                minijajaThread.setDaemon(true);
                minijajaThread.start();
            }
            interpreter.continueInterpretation();
        }

        public void setminiJajaInterpreter(InterpreterRunnable interpreter) {
            this.minijajaInterpreter = interpreter;
            if (minijajaInterpreter != null) {
                interpreter.getStateHandler().endedProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue){
                        System.out.println("Interpretation ended");
                        setminiJajaInterpreter(null);
                    }
                });
                interpreter.getStateHandler().currentLineProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        if (newValue != null){
                            JavaFXUtils.runAsync(() -> {
                                miniJajaCodeEditor.setDebugMarker((Integer) newValue - 1);
                                miniJajaCodeEditor.focusLine((Integer) newValue - 1);
                                return null;
                            });

                        }
                    }
                });
                pileTable1.setItems(interpreter.getMemory().getPile());
                RootSymbolsTableTreeItem root = new RootSymbolsTableTreeItem(interpreter.getMemory());
                HachageTreeTableView1.setRoot(root);
            }
        }
    }
}
