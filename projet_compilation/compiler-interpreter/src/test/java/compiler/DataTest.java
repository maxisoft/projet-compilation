package compiler;

import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.expr.JajaNotSetExpr;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataTest {
    public static final AstObject PARENT = new JajaNotSetExpr();
    private Data data;
    @Before
    public void setUp() throws Exception {
        data = new Data(PARENT);
    }

    @Test
    public void testGetParamIndex() throws Exception {
        Assert.assertEquals(data.getParamIndex(), null);
    }

    @Test
    public void testGetParent() throws Exception {
        Assert.assertEquals(data.getParent(), PARENT);
    }

    @Test
    public void dataMustAcceptNullArg() throws Exception {
        data = new Data(null);
        Assert.assertEquals(data.getParent(), null);
    }
}