package compiler;

import compiler.model.*;
import fr.univfcomte.edu.ast.*;
import fr.univfcomte.edu.ast.expr.JajaDiv;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class CompilerTest {
    private Compiler compiler;
    @Before
    public void setUp() throws Exception {
        compiler = new Compiler();
    }

    @Test
    public void testInitGetInstructions() throws Exception {
        Assert.assertEquals(Arrays.<Instr>asList(new Nop(null)), compiler.getInstructions());
    }

    @Test
    public void testVisitJajaAffect() throws Exception {
        final int value = 5;
        final String ident = "x";
        JajaAffect jajaAffect = new JajaAffect();
        JajaNumber number = new JajaNumber(value);
        jajaAffect.setExpr(number);
        jajaAffect.setIdent(ident);
        jajaAffect.setOperator("=");

        jajaAffect.accept(compiler, null);

        List<Instr> expected = Arrays.asList(
                new Nop(null),
                new Push(number, value),
                new Store(jajaAffect, ident));

        Assert.assertEquals(expected, compiler.getInstructions());
    }

    @Test
    public void testVisitJajaCall() throws Exception {
        final String ident = "f";
        final int value = 5;
        JajaCall jajaCall = new JajaCall();
        jajaCall.setIdent(ident);
        JajaExprs jajaParams = new JajaExprs();
        JajaNumber number = new JajaNumber(value);
        jajaParams.add(number);
        jajaCall.setJajaParams(jajaParams);
        Data parent = new Data(new JajaDiv());

        jajaCall.accept(compiler, parent);

        List<Instr> expected = Arrays.asList(
                new Nop(null),
                new Push(number, value),
                new Invoke(jajaCall, ident),
                new Swap(null),
                new Pop(null),
                new Pop(jajaCall));

        Assert.assertEquals(expected, compiler.getInstructions());
    }

    @Test
    public void testVisitJajaCallWhereParentIsInstrs() throws Exception {
        final String ident = "f";
        final int value = 5;
        JajaCall jajaCall = new JajaCall();
        jajaCall.setIdent(ident);
        JajaExprs jajaParams = new JajaExprs();
        JajaNumber number = new JajaNumber(value);
        jajaParams.add(number);
        jajaCall.setJajaParams(jajaParams);
        Data parent = new Data(new JajaInstrs());

        jajaCall.accept(compiler, parent);

        List<Instr> expected = Arrays.asList(
                new Nop(null),
                new Push(number, value),
                new Invoke(jajaCall, ident),
                new Swap(null),
                new Pop(null),
                new Pop(jajaCall));

        Assert.assertEquals(expected, compiler.getInstructions());
    }

    @Test
    public void testVisitJajaCallWithoutArgs() throws Exception {
        final String ident = "f";
        JajaCall jajaCall = new JajaCall();
        jajaCall.setIdent(ident);
        jajaCall.setJajaParams(new JajaExprs());
        Data parent = new Data(new JajaDiv());

        jajaCall.accept(compiler, parent);
        List<Instr> expected = Arrays.asList(
                new Nop(null),
                new Invoke(jajaCall, ident),
                new Pop(jajaCall));

        Assert.assertEquals(expected, compiler.getInstructions());
    }

    @Test
    public void testVisitJajaClass() throws Exception {

    }

    @Test
    public void testVisit3() throws Exception {

    }

    @Test
    public void testVisit4() throws Exception {

    }

    @Test
    public void testVisit5() throws Exception {

    }

    @Test
    public void testVisit6() throws Exception {

    }

    @Test
    public void testVisit7() throws Exception {

    }

    @Test
    public void testVisit8() throws Exception {

    }

    @Test
    public void testVisit9() throws Exception {

    }

    @Test
    public void testVisit10() throws Exception {

    }

    @Test
    public void testVisit11() throws Exception {

    }

    @Test
    public void testVisit12() throws Exception {

    }

    @Test
    public void testVisit13() throws Exception {

    }

    @Test
    public void testVisit14() throws Exception {

    }

    @Test
    public void testVisit15() throws Exception {

    }

    @Test
    public void testVisit16() throws Exception {

    }

    @Test
    public void testVisit17() throws Exception {

    }

    @Test
    public void testVisit18() throws Exception {

    }

    @Test
    public void testVisit19() throws Exception {

    }

    @Test
    public void testVisit20() throws Exception {

    }

    @Test
    public void testVisit21() throws Exception {

    }

    @Test
    public void testVisit22() throws Exception {

    }

    @Test
    public void testVisit23() throws Exception {

    }

    @Test
    public void testVisit24() throws Exception {

    }

    @Test
    public void testVisit25() throws Exception {

    }

    @Test
    public void testVisit26() throws Exception {

    }

    @Test
    public void testVisit27() throws Exception {

    }

    @Test
    public void testVisit28() throws Exception {

    }

    @Test
    public void testVisit29() throws Exception {

    }

    @Test
    public void testVisit30() throws Exception {

    }
}