package compiler;

import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.expr.JajaNotSetExpr;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataParamIndexTest {
    public static final AstObject PARENT = new JajaNotSetExpr();
    public static final int INDEX = 5;

    private DataParamIndex data;

    @Before
    public void setUp() throws Exception {
        data = new DataParamIndex(PARENT, INDEX);
    }

    @Test
    public void testGetParamIndex() throws Exception {
        Assert.assertEquals(INDEX, (int)data.getParamIndex());
    }

    @Test
    public void dataMustAcceptNullArg() throws Exception {
        data = new DataParamIndex(null, INDEX);
        Assert.assertEquals(data.getParent(), null);
    }
}