package compiler.model;

import interpreter.Interpreter;
import memory.Memory;
import memory.Quadruplet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class DivTest {
    public static final int INTERPRETER_PC = 500;

    private Div aDiv;
    @Mock
    private Interpreter interpreter;
    @Mock
    private Memory memory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        aDiv = new Div(null);
    }

    @Test
    public void testEval() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.when(memory.pop())
                .thenReturn(new Quadruplet(null, 3, null, null))
                .thenReturn(new Quadruplet(null, 5, null, null));
        aDiv.eval(interpreter);
        Mockito.verify(memory).pushValue(5 / 3);
    }

    @Test(expected = ArithmeticException.class)
    public void testEval0Division() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.when(memory.pop())
                .thenReturn(new Quadruplet(null, 0, null, null))
                .thenReturn(new Quadruplet(null, 5, null, null));
        aDiv.eval(interpreter);
    }

    @Test
    public void testEvalPc(){
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(new Quadruplet(null, 5, null, null)).when(memory).pop();
        aDiv.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("div", aDiv.toJajaCode());
    }
}