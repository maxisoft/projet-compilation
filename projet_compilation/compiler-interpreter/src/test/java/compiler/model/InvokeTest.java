package compiler.model;

import fr.univfcomte.edu.ast.JajaType;
import interpreter.Interpreter;
import memory.Memory;
import memory.Obj;
import memory.Quadruplet;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class InvokeTest {
    public static final String INVOKE_PARAM = "f";
    public static final int INTERPRETER_PC = 500;
    public static final int METHOD_ADDRESS = 52;
    private Invoke anInvoke;
    @Mock
    private Interpreter interpreter;
    @Mock
    private Memory memory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        anInvoke = new Invoke(null, INVOKE_PARAM);
    }

    @Test
    public void testGetIdent() throws Exception {
        Assert.assertEquals(INVOKE_PARAM, anInvoke.getIdent());
    }

    @Test(expected = NullPointerException.class)
    public void testCreateInvokeError() throws Exception {
        new Invoke(null, null);
    }

    @Test
    public void testEval() throws Exception {
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(new Quadruplet(INVOKE_PARAM, METHOD_ADDRESS, Obj.METHOD, JajaType.INT)).when(memory).getLastSymbol(INVOKE_PARAM);
        anInvoke.eval(interpreter);
        Mockito.verify(memory).pushValue(INTERPRETER_PC + 1);
        Mockito.verify(interpreter).setPC(METHOD_ADDRESS);
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Invoke.class)
                .suppress(Warning.NULL_FIELDS)
                .withRedefinedSuperclass()
                .verify();
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("invoke(" + INVOKE_PARAM + ")", anInvoke.toJajaCode());
    }

    @Test
    public void testToString() throws Exception {
        Assert.assertEquals("Invoke{astObject=null, ident=f}", anInvoke.toString());
    }
}