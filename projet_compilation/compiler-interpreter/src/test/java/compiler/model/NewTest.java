package compiler.model;

import fr.univfcomte.edu.ast.JajaType;
import memory.Obj;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class NewTest {

    public static final String IDENT = "ident";
    public static final JajaType TYPE = JajaType.INT;
    public static final Obj KIND = Obj.VARIABLE;
    public static final int VALUE = 0;
    public static final String NULL_IDENT = null;
    public static final String EMPTY_IDENT = "";
    public static final JajaType NULL_TYPE = null;
    public static final Obj NULL_KIND = null;

    private New aNew;

    @Before
    public void setUp() throws Exception {
        aNew = new New(null, IDENT, TYPE, KIND, VALUE);
    }

    @Test
    public void testGetIdent() throws Exception {
        Assert.assertEquals(IDENT, aNew.getIdent());
    }

    @Test
    public void testGetType() throws Exception {
        Assert.assertEquals(TYPE, aNew.getType());
    }

    @Test
    public void testGetKind() throws Exception {
        Assert.assertEquals(KIND, aNew.getKind());
    }

    @Test
    public void testGetValue() throws Exception {
        Assert.assertEquals(VALUE, aNew.getValue());
    }

    @Test(expected = NullPointerException.class)
    public void testNewCreationErrorIdentNull() {
        new New(null, NULL_IDENT, TYPE, KIND, VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNewCreationErrorIdentEmpty() {
        new New(null, EMPTY_IDENT, TYPE, KIND, VALUE);
    }

    @Test(expected = NullPointerException.class)
    public void testNewCreationErrorTypeNull() {
        new New(null, IDENT, NULL_TYPE, KIND, VALUE);
    }

    @Test(expected = NullPointerException.class)
    public void testNewCreationErrorKindNull() {
        new New(null, IDENT, TYPE, NULL_KIND, VALUE);
    }
}
