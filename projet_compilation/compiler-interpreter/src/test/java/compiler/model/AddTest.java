package compiler.model;

import interpreter.Interpreter;
import memory.Memory;
import memory.Quadruplet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class AddTest {
    public static final int INTERPRETER_PC = 500;

    private Add anAdd;
    @Mock
    private Interpreter interpreter;
    @Mock
    private Memory memory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        anAdd = new Add(null);
    }

    @Test
    public void testEval() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.when(memory.pop())
                .thenReturn(new Quadruplet(null, 3, null, null))
                .thenReturn(new Quadruplet(null, 5, null, null));
        anAdd.eval(interpreter);
        Mockito.verify(memory).pushValue(5 + 3);
    }

    @Test
    public void testEvalPc(){
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(new Quadruplet(null, 5, null, null)).when(memory).pop();
        anAdd.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("add", anAdd.toJajaCode());
    }


}