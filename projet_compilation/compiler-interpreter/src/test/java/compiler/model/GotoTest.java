package compiler.model;

import interpreter.Interpreter;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class GotoTest {
    public static final int ADDRESS = 1;
    private Goto aGoto;
    @Mock
    private Interpreter interpreter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        aGoto = new Goto(null, ADDRESS);
    }

    @Test
    public void testGetAddress() throws Exception {
        Assert.assertEquals(ADDRESS, aGoto.getAddress());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGotoCreationError() throws Exception {
        new Goto(null, 0);
    }

    @Test
    public void testEval() throws Exception {
        aGoto.eval(interpreter);
        Mockito.verify(interpreter).setPC(aGoto.getAddress());
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Goto.class)
                .withRedefinedSuperclass()
                .verify();
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("goto(" + ADDRESS + ")", aGoto.toJajaCode());
    }

    @Test
    public void testToString() throws Exception {
        Assert.assertEquals("Goto{astObject=null, address=1}", aGoto.toString());
    }

    @Test
    public void testPositiveCanEqual() throws Exception {
        Assert.assertTrue(aGoto.canEqual(new Goto(null, ADDRESS)));
    }

    @Test
    public void testNegativeCanEqual() throws Exception {
        Assert.assertFalse(aGoto.canEqual(new Instr(null) {
            @Override
            public void eval(Interpreter interpreter) {

            }

            @Override
            public String toJajaCode() {
                return null;
            }
        }));
    }
}