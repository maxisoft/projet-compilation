package compiler.model.utils;

import fr.univfcomte.edu.ast.JajaType;
import memory.Obj;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author maxime
 */
public class UtilsTest {

    public static final int INT_VALUE = 1;
    public static final String STRING_VALUE = "test";
    public static final long LONG_VALUE = 1L;
    public static final int INT_ERROR_VALUE = 0;
    public static final long LONG_ERROR_VALUE = 0L;
    public static final String EMPTY_STRING = "";
    public static final String NULL_STRING = null;

    @Test
    public void testCheckStrictPositive() throws Exception {
        int result = Utils.checkStrictPositive(INT_VALUE);
        Assert.assertEquals(INT_VALUE, result);
    }

    @Test
    public void testCheckNonNullOrEmpty() throws Exception {
        String result = Utils.checkNonNullOrEmpty(STRING_VALUE);
        Assert.assertEquals(STRING_VALUE, result);
    }

    @Test
    public void testCheckStrictPositiveLong() throws Exception {
        long result = Utils.checkStrictPositive(LONG_VALUE);
        Assert.assertEquals(LONG_VALUE, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckStrictPositiveError() throws Exception {
        Utils.checkStrictPositive(INT_ERROR_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckStrictPositiveLongError() throws Exception {
        Utils.checkStrictPositive(LONG_ERROR_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckNonNullOrEmptyErrorEmpty() throws Exception {
        Utils.checkNonNullOrEmpty(EMPTY_STRING);
    }

    @Test(expected = NullPointerException.class)
    public void testCheckNonNullOrEmptyErrorNull() throws Exception {
        Utils.checkNonNullOrEmpty(NULL_STRING);
    }

    @Test
    public void testFormatType() throws Exception {
        Assert.assertEquals("int", Utils.formatType(JajaType.INT));
    }

    @Test
    public void testFormatTypeHandleNull() throws Exception {
        Assert.assertEquals(null, Utils.formatType(null));
    }

    @Test
    public void testFormatKind() throws Exception {
        Assert.assertEquals("variable", Utils.formatKind(Obj.VARIABLE));
    }

    @Test
    public void testFormatKindHandleNull() throws Exception {
        Assert.assertEquals(null, Utils.formatKind(null));
    }
}