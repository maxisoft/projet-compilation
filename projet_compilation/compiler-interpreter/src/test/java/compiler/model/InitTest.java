package compiler.model;

import interpreter.Interpreter;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class InitTest {
    public static final int INTERPRETER_PC = 500;
    private Init anInit;
    @Mock
    private Interpreter interpreter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        anInit = new Init(null);
    }

    @Test
    public void testEval() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        anInit.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Init.class)
                .verify();
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("init", anInit.toJajaCode());
    }
}