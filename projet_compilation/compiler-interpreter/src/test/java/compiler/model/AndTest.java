package compiler.model;

import interpreter.Interpreter;
import memory.Memory;
import memory.Quadruplet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class AndTest {
    public static final int INTERPRETER_PC = 500;

    private And anAnd;

    @Mock
    private Interpreter interpreter;
    @Mock
    private Memory memory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        anAnd = new And(null);
    }

    @Test
    public void testEvalFalse() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.when(memory.pop())
                .thenReturn(new Quadruplet(null, 1, null, null))
                .thenReturn(new Quadruplet(null, 0, null, null));
        anAnd.eval(interpreter);
        Mockito.verify(memory).pushValue(0);
    }

    @Test
    public void testEvalTrue() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.when(memory.pop())
                .thenReturn(new Quadruplet(null, 1, null, null))
                .thenReturn(new Quadruplet(null, 1, null, null));
        anAnd.eval(interpreter);
        Mockito.verify(memory).pushValue(1);
    }

    @Test
    public void testEvalPc(){
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(new Quadruplet(null, 5, null, null)).when(memory).pop();
        anAnd.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("and", anAnd.toJajaCode());
    }
}