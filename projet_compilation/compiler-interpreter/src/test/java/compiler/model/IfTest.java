package compiler.model;

import interpreter.Interpreter;
import memory.Memory;
import memory.Quadruplet;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class IfTest {

    public static final int ADDRESS = 1;
    public static final int INTERPRETER_PC = 500;
    public static final Quadruplet QUADRUPLET_TRUE = new Quadruplet(null, 1, null, null);
    public static final Quadruplet QUADRUPLET_FALSE = new Quadruplet(null, 0, null, null);
    private If anIf;
    @Mock
    private Interpreter interpreter;
    @Mock
    private Memory memory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        anIf = new If(null, ADDRESS);
    }

    @Test
    public void testGetAddress() throws Exception {
        Assert.assertEquals(ADDRESS, anIf.getAddress());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfCreationError() throws Exception {
        new If(null, 0);
    }

    @Test
    public void testEvalTrue() throws Exception {
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(QUADRUPLET_TRUE).when(memory).pop();
        anIf.eval(interpreter);
        Mockito.verify(interpreter).setPC(anIf.getAddress());
    }

    @Test
    public void testEvalFalse() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(QUADRUPLET_FALSE).when(memory).pop();
        anIf.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(If.class)
                .withRedefinedSuperclass()
                .verify();
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("if(" + ADDRESS + ")", anIf.toJajaCode());
    }

    @Test
    public void testToString() throws Exception {
        Assert.assertEquals("If{astObject=null, address=1}", anIf.toString());
    }
}