package compiler.model;

import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.JajaNumber;
import interpreter.Interpreter;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InstrTest {
    private Instr anInstr;
    private final static AstObject PARAM = new JajaNumber();

    @Before
    public void setUp() throws Exception {
        anInstr = new Instr(PARAM) {
            @Override
            public void eval(Interpreter interpreter) {

            }

            @Override
            public String toJajaCode() {
                return null;
            }
        };
    }

    @Test
    public void testGetAstObject() throws Exception {
        Assert.assertEquals(PARAM, anInstr.getAstObject());
    }

    @Test
    public void testAcceptNullArgument() {
        new Instr(null) {
            @Override
            public void eval(Interpreter interpreter) {

            }

            @Override
            public String toJajaCode() {
                return null;
            }
        };
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Instr.class)
                .withRedefinedSubclass(Goto.class)
                .verify();
    }

    @Test
    public void testEval() throws Exception {
        System.out.println(anInstr.toString());
        Assert.assertEquals("{astObject="+ PARAM.toString() + "}", anInstr.toString());
    }
}