package compiler.model;

import interpreter.Interpreter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class NopTest {
    public static final int INTERPRETER_PC = 500;

    private Nop aNop;
    @Mock
    private Interpreter interpreter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        aNop = new Nop(null);
    }

    @Test
    public void testEval() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        aNop.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("nop", aNop.toJajaCode());
    }
}