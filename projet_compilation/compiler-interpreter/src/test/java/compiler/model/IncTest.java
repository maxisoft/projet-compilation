package compiler.model;

import fr.univfcomte.edu.ast.JajaType;
import interpreter.Interpreter;
import memory.Memory;
import memory.Obj;
import memory.Quadruplet;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class IncTest {
    public static final String IDENT = "x";
    public static final int INTERPRETER_PC = 500;
    private Inc anInc;

    @Mock
    private Interpreter interpreter;
    @Mock
    private Memory memory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        anInc = new Inc(null, IDENT);
    }

    @Test
    public void testGetIdent() throws Exception {
        Assert.assertEquals(IDENT, anInc.getIdent());
    }

    @Test
    public void testEval() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(new Quadruplet(null, 5, null, null)).when(memory).pop();
        Quadruplet toBeReturned = new Quadruplet(IDENT, 2, Obj.VARIABLE, JajaType.INT);
        Mockito.doReturn(toBeReturned)
                .when(memory).getLastSymbol(IDENT);
        anInc.eval(interpreter);
        Assert.assertEquals(5 + 2, (int) toBeReturned.getValue());
    }

    @Test
    public void testEvalPc() {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(new Quadruplet(null, 5, null, null)).when(memory).pop();
        Mockito.doReturn(new Quadruplet(IDENT, 2, Obj.VARIABLE, JajaType.INT))
                .when(memory).getLastSymbol(IDENT);
        anInc.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("inc(" + IDENT + ")", anInc.toJajaCode());
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Inc.class)
                .suppress(Warning.NULL_FIELDS)
                .withRedefinedSuperclass()
                .verify();
    }

    @Test
    public void testToString() throws Exception {
        Assert.assertEquals("Inc{astObject=null, ident=x}", anInc.toString());
    }
}