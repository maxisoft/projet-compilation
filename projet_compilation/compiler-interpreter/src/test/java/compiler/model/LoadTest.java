package compiler.model;

import fr.univfcomte.edu.ast.JajaType;
import interpreter.Interpreter;
import memory.Memory;
import memory.Obj;
import memory.Quadruplet;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;

public class LoadTest {
    public static final String IDENT = "x";
    public static final int INTERPRETER_PC = 500;

    private Load aLoad;

    @Mock
    private Interpreter interpreter;
    @Mock
    private Memory memory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        aLoad = new Load(null, IDENT);
    }

    @Test
    public void testToString() throws Exception {
        Assert.assertEquals("Load{astObject=null, ident=x}", aLoad.toString());
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("load(" + IDENT + ")", aLoad.toJajaCode());
    }

    @Test
    public void testGetIdent() throws Exception {
        Assert.assertEquals(IDENT, aLoad.getIdent());
    }

    @Test
    public void testEval() throws Exception {
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Quadruplet toBeReturned = new Quadruplet(IDENT, 2, Obj.VARIABLE, JajaType.INT);
        Mockito.doReturn(toBeReturned)
                .when(memory).getLastSymbol(IDENT);
        aLoad.eval(interpreter);
        //TODO
        // 'cause quadruplet has bad equals method
        Mockito.verify(memory).push(any(Quadruplet.class));
    }

    @Test
    public void testEvalPc(){
        Mockito.doReturn(INTERPRETER_PC).when(interpreter).getPC();
        Mockito.doReturn(memory).when(interpreter).getMemory();
        Mockito.doReturn(new Quadruplet(IDENT, 2, Obj.VARIABLE, JajaType.INT))
                .when(memory).getLastSymbol(IDENT);
        aLoad.eval(interpreter);
        Mockito.verify(interpreter).setPC(INTERPRETER_PC + 1);
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Load.class)
                .suppress(Warning.NULL_FIELDS)
                .withRedefinedSuperclass()
                .verify();
    }
}