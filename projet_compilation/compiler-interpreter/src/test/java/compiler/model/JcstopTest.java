package compiler.model;

import interpreter.Interpreter;
import interpreter.StopInterpretation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class JcstopTest {
    private Jcstop aJcstop;

    @Mock
    private Interpreter interpreter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        aJcstop = new Jcstop(null);
    }

    @Test(expected = StopInterpretation.class)
    public void testEval() throws Exception {
        aJcstop.eval(interpreter);
    }

    @Test
    public void testToJajaCode() throws Exception {
        Assert.assertEquals("jcstop", aJcstop.toJajaCode());
    }
}