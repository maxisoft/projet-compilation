package compiler;

import com.google.common.collect.ImmutableList;
import compiler.model.*;
import compiler.visitor.DataInterface;
import compiler.visitor.ReturnInterface;
import fr.univfcomte.edu.ast.*;
import fr.univfcomte.edu.ast.expr.*;
import memory.Obj;
import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maxime
 */
public class Compiler implements AstVisitor<ReturnInterface, DataInterface> {

    private List<Instr> instructions;

    public Compiler() {
        instructions = new ArrayList<>();
        instructions.add(new Nop(null));
    }

    public List<Instr> getInstructions() {
        return ImmutableList.copyOf(instructions);
    }

    private void addInst(Instr instr) {
        instructions.add(instr);
    }

    private void removeAstList(AstListWrapper list) {
        for (Object ignored : list) {
            addInst(new Swap(null));
            addInst(new Pop(null));
        }
    }

    @Override
    public ReturnInterface visit(JajaAffect visitable, DataInterface data) {
        visitable.getExpr().accept(this, new Data(visitable));
        String value = visitable.getIdent();
        if (visitable.getOperator().equals("+=")) { // Inc is in the same node
            addInst(new Inc(visitable, value));
        } else { // assume that's an affect
            addInst(new Store(visitable, value));
        }
        return null;
    }

    @Override
    public ReturnInterface visit(JajaCall visitable, DataInterface data) {
        visitable.getJajaParams().accept(this, new Data(visitable));
        addInst(new Invoke(visitable, visitable.getIdent()));
        removeAstList(visitable.getJajaParams());
        addInst(new Pop(visitable));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaClass visitable, DataInterface data) {
        addInst(new Init(visitable));
        visitable.getJajaDecls().accept(this, new Data(visitable));
        visitable.getJajaMain().accept(this, new Data(visitable));
        removeAstList(visitable.getJajaDecls());
        //remove the main's return value
        addInst(new Pop(null));
        addInst(new Jcstop(null));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaDecls visitable, DataInterface data) {
        for (JajaDecl decl : visitable) {
            decl.accept(this, new Data(visitable));
        }
        return null;
    }

    @Override
    public ReturnInterface visit(JajaElse visitable, DataInterface data) {
        visitable.getJajaInstrs().accept(this, new Data(visitable));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaIf visitable, DataInterface data) {
        visitable.getExpr().accept(this, new Data(visitable));
        int ifAddr = instructions.size();
        //allocate space to store the if instruction
        addInst(null);

        if (visitable.getJajaElse() != null && !visitable.getJajaElse().getJajaInstrs().isEmpty()) {
            visitable.getJajaElse().accept(this, new Data(visitable));
        }
        instructions.set(ifAddr, new If(visitable, instructions.size() + 1));

        int gotoAddr = instructions.size();
        addInst(null);
        visitable.getJajaInstrs().accept(this, new Data(visitable));
        instructions.set(gotoAddr, new Goto(visitable, instructions.size()));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaIncrement visitable, DataInterface data) {
        addInst(new Push(visitable, 1));
        addInst(new Inc(visitable, visitable.getIdent()));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaMain visitable, DataInterface data) {
        visitable.getJajaVars().accept(this, new Data(visitable));
        visitable.getJajaInstrs().accept(this, new Data(visitable));
        // Main is a void method
        addInst(new Push(visitable, 0));
        removeAstList(visitable.getJajaVars());
        return null;
    }

    @Override
    public ReturnInterface visit(JajaMeth visitable, DataInterface data) {
        int endDecl = instructions.size() + 3;
        addInst(new Push(visitable, endDecl));
        addInst(new New(
                visitable,
                visitable.getIdentifier(),
                visitable.getType(),
                Obj.METHOD,
                0));

        final int gotoPosition = instructions.size();
        // allocate space in the array
        addInst(null);

        visitable.getParams().accept(this, new Data(visitable));
        visitable.getJajaVars().accept(this, new Data(visitable));
        visitable.getJajaInstrs().accept(this, new Data(visitable));
        //void methods returns 0
        if (visitable.getType() == JajaType.VOID) {
            addInst(new Push(visitable, 0));
        }
        removeAstList(visitable.getJajaVars());
        addInst(new Swap(null));
        addInst(new Return(null));

        //replace the null with the goto containing the good address
        instructions.set(gotoPosition, new Goto(visitable, instructions.size()));

        return null;
    }

    @Override
    public ReturnInterface visit(JajaParam visitable, DataInterface data) {
        int paramIndex = data.getParamIndex();
        addInst(new New(visitable, visitable.getIdentifier(),
                visitable.getType(),
                Obj.VARIABLE,
                paramIndex));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaReturn visitable, DataInterface data) {
        visitable.getExpr().accept(this, new Data(visitable));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaVar visitable, DataInterface data) {
        visitable.getValueExpr().accept(this, new Data(visitable));
        addInst(new New(visitable,
                visitable.getIdentifier(),
                visitable.getType(),
                visitable.isFinalDecl() ? Obj.CONSTANT : Obj.VARIABLE,
                0));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaVars visitable, DataInterface data) {
        for (JajaVar var : visitable) {
            var.accept(this, new Data(visitable));
        }
        return null;
    }

    @Override
    public ReturnInterface visit(JajaWhile visitable, DataInterface data) {
        int gotoReturn = instructions.size();
        visitable.getExpr().accept(this, new Data(visitable));
        addInst(new Not(visitable));
        int ifAddr = instructions.size();
        addInst(null);
        visitable.getJajaInstrs().accept(this, new Data(visitable));
        instructions.set(ifAddr, new If(visitable, instructions.size() + 1));
        addInst(new Goto(visitable, gotoReturn));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaAnd jajaAnd, DataInterface data) {
        jajaAnd.getSonsExprs().accept(this, new Data(jajaAnd));
        addInst(new And(jajaAnd));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaDiv jajaDiv, DataInterface data) {
        jajaDiv.getSonsExprs().accept(this, new Data(jajaDiv));
        addInst(new Div(jajaDiv));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaEq jajaEq, DataInterface data) {
        jajaEq.getSonsExprs().accept(this, new Data(jajaEq));
        addInst(new Cmp(jajaEq));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaMinus jajaMinus, DataInterface data) {
        jajaMinus.getSonsExprs().accept(this, new Data(jajaMinus));
        addInst(new Sub(jajaMinus));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaMult jajaMult, DataInterface data) {
        jajaMult.getSonsExprs().accept(this, new Data(jajaMult));
        addInst(new Mul(jajaMult));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaNot jaJaNot, DataInterface data) {
        jaJaNot.getSonsExprs().accept(this, new Data(jaJaNot));
        addInst(new Not(jaJaNot));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaOr jajaOr, DataInterface data) {
        jajaOr.getSonsExprs().accept(this, new Data(jajaOr));
        addInst(new Or(jajaOr));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaPlus jajaPlus, DataInterface data) {
        jajaPlus.getSonsExprs().accept(this, new Data(jajaPlus));
        addInst(new Add(jajaPlus));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaSup jajaSup, DataInterface data) {
        jajaSup.getSonsExprs().accept(this, new Data(jajaSup));
        addInst(new Sup(jajaSup));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaUnaryMinus jajaUnaryMinus, DataInterface data) {
        jajaUnaryMinus.getSonsExprs().accept(this, new Data(jajaUnaryMinus));
        addInst(new Neg(jajaUnaryMinus));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaInstrs jajaInstrs, DataInterface data) {
        for (JajaInstr instr : jajaInstrs) {
            instr.accept(this, new Data(jajaInstrs));
        }
        return null;
    }

    @Override
    public ReturnInterface visit(JajaExprs jajaExprs, DataInterface data) {
        for (JajaExpr expr : jajaExprs) {
            expr.accept(this, new Data(jajaExprs));
        }
        return null;
    }

    @Override
    public ReturnInterface visit(JajaNotSetExpr jajaNotSetExpr, DataInterface data) {
        // Anyway put some value into the stack
        addInst(new Push(jajaNotSetExpr, 0));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaParams jajaParams, DataInterface data) {
        int paramIndex = jajaParams.size();
        for (JajaParam param : jajaParams) {
            param.accept(this, new DataParamIndex(jajaParams, paramIndex--));
        }
        return null;
    }

    @Override
    public ReturnInterface visit(JajaBoolean jajaBoolean, DataInterface data) {
        addInst(new Push(jajaBoolean, jajaBoolean.getBooleanValue() ? 1 : 0));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaIdent jajaIdent, DataInterface data) {
        addInst(new Load(jajaIdent, jajaIdent.getValue()));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaNumber jajaNumber, DataInterface data) {
        addInst(new Push(jajaNumber, jajaNumber.getIntegerValue()));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaVarArray jajaVarArray, DataInterface data) {
        jajaVarArray.getIndexExpr().accept(this, new Data(jajaVarArray));
        addInst(new NewArray(jajaVarArray,
                jajaVarArray.getIdentifier(),
                jajaVarArray.getType()));
        return null;
    }

    @Override
    public ReturnInterface visit(JajaAffectArray jajaAffectArray, DataInterface data) {
        jajaAffectArray.getIndexExpr().accept(this, new Data(jajaAffectArray));
        jajaAffectArray.getExpr().accept(this, new Data(jajaAffectArray));
        String value = jajaAffectArray.getIdent();
        if (jajaAffectArray.getOperator().equals("+=")) { // AInc is in the same node
            addInst(new AInc(jajaAffectArray, value));
        } else { // assume that's an affect
            addInst(new AStore(jajaAffectArray, value));
        }
        return null;
    }

    @Override
    public ReturnInterface visit(JajaCallExpr visitable, DataInterface data) {
        visitable.getJajaParams().accept(this, new Data(visitable));
        addInst(new Invoke(visitable, visitable.getIdent()));
        removeAstList(visitable.getJajaParams());
        return null;
    }

    @Override
    public ReturnInterface visit(JajaIdentArray jajaIdentArray, DataInterface data) {
        jajaIdentArray.getIndexExpr().accept(this, new Data(jajaIdentArray));
        addInst(new ALoad(jajaIdentArray, jajaIdentArray.getValue()));
        return null;
    }
}
