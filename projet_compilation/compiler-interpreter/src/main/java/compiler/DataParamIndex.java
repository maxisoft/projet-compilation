package compiler;

import fr.univfcomte.edu.ast.AstObject;

/**
 * @author maxime
 */
public class DataParamIndex extends Data {
    private final int value;

    public DataParamIndex(AstObject parent, int value) {
        super(parent);
        this.value = value;
    }

    @Override
    public Integer getParamIndex() {
        return value;
    }
}
