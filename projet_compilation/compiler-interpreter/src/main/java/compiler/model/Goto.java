package compiler.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

import javax.annotation.Generated;

import static compiler.model.utils.Utils.checkStrictPositive;

/**
 * @author maxime
 */
public final class Goto extends Instr {
    private final int address;

    public Goto(AstObject astObject, int address) {
        super(astObject);
        this.address = checkStrictPositive(address);
    }

    public int getAddress() {
        return address;
    }

    @Override
    public void eval(Interpreter interpreter) {
        interpreter.setPC(address);
    }

    @Override
    public String toJajaCode() {
        return String.format("goto(%d)", getAddress());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("address", address)
                .toString();
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == Goto.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Goto)) return false;
        if (!super.equals(o)) return false;

        Goto aGoto = (Goto) o;

        if (address != aGoto.address) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + address;
        return result;
    }
}
