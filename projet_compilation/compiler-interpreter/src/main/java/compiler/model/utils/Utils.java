package compiler.model.utils;

import com.google.common.base.Preconditions;
import fr.univfcomte.edu.ast.JajaType;
import memory.Obj;

/**
 * @author maxime
 */
public final class Utils {
    private Utils() {}

    public static int checkStrictPositive(int value) {
        Preconditions.checkArgument(value > 0L, "negative value: %s", value);
        return value;
    }

    public static long checkStrictPositive(long value) {
        Preconditions.checkArgument(value > 0L, "negative value: %s", value);
        return value;
    }

    public static String checkNonNullOrEmpty(String string) {
        Preconditions.checkNotNull(string);
        Preconditions.checkArgument(!string.isEmpty(), "the argument string is empty");
        return string;
    }

    public static String formatType(JajaType type){
        if (type == null){
            return null;
        }
        return type.toString().toLowerCase();
    }

    public static String formatKind(Obj kind){
        if (kind == null){
            return null;
        }
        return kind.toString().toLowerCase();
    }
}
