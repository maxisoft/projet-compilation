package compiler.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import compiler.model.utils.Utils;
import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.JajaType;
import interpreter.Interpreter;
import memory.Obj;
import memory.Quadruplet;

import javax.annotation.Generated;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;
import static java.util.Objects.requireNonNull;

/**
 * @author maxime
 */
public final class NewArray extends Instr {
    private final String ident;
    private final JajaType type;

    public NewArray(AstObject astObject, String ident, JajaType type) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
        this.type = requireNonNull(type);
    }

    public String getIdent() {
        return ident;
    }

    public JajaType getType() {
        return type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .add("type", type)
                .toString();
    }


    @Override
    public String toJajaCode() {
        return String.format("newarray(%s,%s)", getIdent(), Utils.formatType(getType()));
    }

    @Override
    public void eval(Interpreter interpreter) {
        int len = interpreter.getMemory().pop().getValue();
        interpreter.getMemory().getHeap().declareArray(ident, len);
        interpreter.getMemory().addQuad(new Quadruplet(ident, 0, Obj.ARRAY, type));
        interpreter.setPC(interpreter.getPC() + 1);
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == NewArray.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewArray)) return false;
        if (!super.equals(o)) return false;

        NewArray newArray = (NewArray) o;
        if (!newArray.canEqual(this)) return false;
        if (!ident.equals(newArray.ident)) return false;
        if (type != newArray.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

}
