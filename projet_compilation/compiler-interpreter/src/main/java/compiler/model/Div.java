package compiler.model;

import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

/**
 * @author maxime
 */
public final class Div extends Instr {
    public Div(AstObject astObject) {
        super(astObject);
    }

    @Override
    public void eval(Interpreter interpreter) {
        int right = interpreter.getMemory().pop().getValue();
        int left = interpreter.getMemory().pop().getValue();
        int computed = left / right;
        interpreter.getMemory().pushValue(computed);
        interpreter.setPC(interpreter.getPC() + 1);
    }

    @Override
    public String toJajaCode() {
        return "div";
    }
}
