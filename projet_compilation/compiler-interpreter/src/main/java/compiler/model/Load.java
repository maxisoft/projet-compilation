package compiler.model;

import com.google.common.base.MoreObjects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;
import memory.Obj;
import memory.Quadruplet;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;

/**
 * @author maxime
 */
public final class Load extends Instr {

    private final String ident;

    public Load(AstObject astObject, String ident) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .toString();
    }

    @Override
    public String toJajaCode() {
        return String.format("load(%s)", getIdent());
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == Load.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Load)) return false;
        if (!super.equals(o)) return false;

        Load load = (Load) o;
        if (!load.canEqual(this)) return false;
        if (!ident.equals(load.ident)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        return result;
    }

    public String getIdent() {
        return ident;
    }

    @Override
    public void eval(Interpreter interpreter) {
        Quadruplet quad = interpreter.getMemory().getLastSymbol(ident);
        Quadruplet newQuad;
        Obj obj = quad.getObj();
        if (obj != null && obj == Obj.ARRAY){
            newQuad = new Quadruplet(quad.getIdent(), obj, quad.getKind());
        }else{
            newQuad = new Quadruplet(null, quad.getValue(), null, null);
        }
        interpreter.getMemory().push(newQuad);
        interpreter.setPC(interpreter.getPC() + 1);
    }
}
