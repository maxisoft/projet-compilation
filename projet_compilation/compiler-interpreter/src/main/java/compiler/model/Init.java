package compiler.model;

import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

import javax.annotation.Generated;

/**
 * @author maxime
 */
public final class Init extends Instr {
    public Init(AstObject astObject) {
        super(astObject);
    }

    @Override
    public void eval(Interpreter interpreter) {
        interpreter.setPC(interpreter.getPC() + 1);
    }

    @Override
    public String toJajaCode() {
        return "init";
    }
}
