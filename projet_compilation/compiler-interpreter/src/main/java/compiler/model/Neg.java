package compiler.model;

import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

/**
 * @author maxime
 */
public final class Neg extends Instr {
    public Neg(AstObject astObject) {
        super(astObject);
    }

    @Override
    public void eval(Interpreter interpreter) {
        int item = interpreter.getMemory().pop().getValue();
        int computed = -item;
        interpreter.getMemory().pushValue(computed);
        interpreter.setPC(interpreter.getPC() + 1);
    }

    @Override
    public String toJajaCode() {
        return "neg";
    }
}
