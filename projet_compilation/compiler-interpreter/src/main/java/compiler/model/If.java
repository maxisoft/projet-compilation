package compiler.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

import javax.annotation.Generated;

import static compiler.model.utils.Utils.checkStrictPositive;

/**
 * @author maxime
 */
public final class If extends Instr {
    private final int address;

    public If(AstObject astObject, int address) {
        super(astObject);
        this.address = checkStrictPositive(address);
    }

    public int getAddress() {
        return address;
    }

    @Override
    public String toJajaCode() {
        return String.format("if(%d)", getAddress());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("address", address)
                .toString();
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == If.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof If)) return false;
        if (!super.equals(o)) return false;

        If anIf = (If) o;

        if (address != anIf.address) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + address;
        return result;
    }

    @Override
    public void eval(Interpreter interpreter) {
        boolean cond = interpreter.getMemory().pop().getValue() != 0;
        interpreter.setPC(cond ? address : interpreter.getPC() + 1);
    }

}
