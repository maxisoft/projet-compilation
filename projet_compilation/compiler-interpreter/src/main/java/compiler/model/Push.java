package compiler.model;

import com.google.common.base.MoreObjects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

/**
 * @author maxime
 */
public final class Push extends Instr {
    private final int value;


    public Push(AstObject astObject, int value) {
        super(astObject);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("value", value)
                .toString();
    }

    @Override
    public void eval(Interpreter interpreter) {
        interpreter.getMemory().pushValue(value);
        interpreter.setPC(interpreter.getPC() + 1);
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == Push.class;
    }

    @Override
    public String toJajaCode() {
        return String.format("push(%d)", getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Push)) return false;
        if (!super.equals(o)) return false;

        Push push = (Push) o;
        if (!push.canEqual(this)) return false;
        if (value != push.value) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + value;
        return result;
    }
}
