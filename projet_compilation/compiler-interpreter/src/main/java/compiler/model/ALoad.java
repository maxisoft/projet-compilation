package compiler.model;

import com.google.common.base.MoreObjects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;

/**
 * @author maxime
 */
public class ALoad extends Instr {

    private final String ident;

    public ALoad(AstObject astObject, String ident) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .toString();
    }

    @Override
    public String toJajaCode() {
        return String.format("aload(%s)", getIdent());
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == ALoad.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ALoad)) return false;
        if (!super.equals(o)) return false;

        ALoad load = (ALoad) o;
        if (!load.canEqual(this)) return false;
        if (!ident.equals(load.ident)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        return result;
    }

    public String getIdent() {
        return ident;
    }

    @Override
    public void eval(Interpreter interpreter) {
        int index = interpreter.getMemory().pop().getValue();
        Integer value = interpreter.getMemory().getHeap().getValueOf(ident, index);
        interpreter.getMemory().pushValue(value);
        interpreter.setPC(interpreter.getPC() + 1);
    }
}
