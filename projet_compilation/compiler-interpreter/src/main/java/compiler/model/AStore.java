package compiler.model;

import com.google.common.base.MoreObjects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;

/**
 * @author maxime
 */
public final class AStore extends Instr {

    private final String ident;

    public AStore(AstObject astObject, String ident) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .toString();
    }

    @Override
    public String toJajaCode() {
        return String.format("astore(%s)", getIdent());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AStore)) return false;
        if (!super.equals(o)) return false;

        AStore store = (AStore) o;
        if (!store.canEqual(this)) return false;
        if (!ident.equals(store.ident)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        return result;
    }

    public String getIdent() {
        return ident;
    }

    @Override
    public void eval(Interpreter interpreter) {
        Integer value = interpreter.getMemory().pop().getValue();
        int index = interpreter.getMemory().pop().getValue();
        interpreter.getMemory().getHeap().affectValue(ident, index, value);
        interpreter.setPC(interpreter.getPC() + 1);
    }
}
