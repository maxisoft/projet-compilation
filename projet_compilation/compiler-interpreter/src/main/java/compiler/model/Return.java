package compiler.model;

import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

/**
 * @author maxime
 */
public final class Return extends Instr {
    public Return(AstObject astObject) {
        super(astObject);
    }

    @Override
    public void eval(Interpreter interpreter) {
        int returnAddr = interpreter.getMemory().pop().getValue();
        interpreter.setPC(returnAddr);
    }

    @Override
    public String toJajaCode() {
        return "return";
    }
}
