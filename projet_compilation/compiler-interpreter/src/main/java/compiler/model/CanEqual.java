package compiler.model;

/**
 * @author maxime
 */
public interface CanEqual {
    public boolean canEqual(Object o);
}
