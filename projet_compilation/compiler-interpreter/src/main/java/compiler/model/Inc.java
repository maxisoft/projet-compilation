package compiler.model;

import com.google.common.base.MoreObjects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;
import memory.Quadruplet;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;

/**
 * @author maxime
 */
public final class Inc extends Instr {

    private final String ident;

    public Inc(AstObject astObject, String ident) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
    }

    public String getIdent() {
        return ident;
    }

    @Override
    public void eval(Interpreter interpreter) {
        Quadruplet quad = interpreter.getMemory().getLastSymbol(ident);
        quad.setValue(quad.getValue() + interpreter.getMemory().pop().getValue());
        interpreter.setPC(interpreter.getPC() + 1);
    }

    @Override
    public String toJajaCode() {
        return String.format("inc(%s)", getIdent());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .toString();
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == Inc.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Inc)) return false;
        if (!super.equals(o)) return false;

        Inc inc = (Inc) o;
        if (!inc.canEqual(this)) return false;
        if (!ident.equals(inc.ident)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        return result;
    }
}
