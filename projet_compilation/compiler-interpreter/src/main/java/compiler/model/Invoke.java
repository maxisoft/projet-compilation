package compiler.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;
import memory.Quadruplet;

import javax.annotation.Generated;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;

/**
 * @author maxime
 */
public final class Invoke extends Instr {

    private final String ident;

    public Invoke(AstObject astObject, String ident) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
    }

    public String getIdent() {
        return ident;
    }

    @Override
    public void eval(Interpreter interpreter) {
        int pc = interpreter.getPC();
        interpreter.getMemory().pushValue(pc + 1);
        Quadruplet quadMeth = interpreter.getMemory().getLastSymbol(getIdent());
        interpreter.setPC(quadMeth.getValue());
    }

    @Override
    public String toJajaCode() {
        return String.format("invoke(%s)", ident);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Invoke)) return false;
        if (!super.equals(o)) return false;

        Invoke invoke = (Invoke) o;
        if(!invoke.canEqual(this)) return false;
        if (!ident.equals(invoke.ident)) return false;

        return true;
    }

    public boolean canEqual(Object o){
        return o.getClass() == Invoke.class;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        return result;
    }
}
