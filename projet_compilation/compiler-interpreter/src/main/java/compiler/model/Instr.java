package compiler.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

import javax.annotation.Generated;

/**
 * @author maxime
 */
public abstract class Instr implements CanEqual {

    private final AstObject astObject;

    protected Instr(AstObject astObject) {
        this.astObject = astObject;
    }

    public abstract void eval(Interpreter interpreter);

    public abstract String toJajaCode();

    public AstObject getAstObject() {
        return astObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Instr)) return false;

        Instr instr = (Instr) o;
        if (!instr.canEqual(this)) return false;
        if (astObject != null ? !astObject.equals(instr.astObject) : instr.astObject != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return astObject != null ? astObject.hashCode() : 0;
    }

    @Override
    public boolean canEqual(Object o) {
        return o instanceof Instr;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", astObject)
                .toString();
    }
}
