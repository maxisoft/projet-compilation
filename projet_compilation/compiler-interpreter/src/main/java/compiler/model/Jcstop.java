package compiler.model;

import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;
import interpreter.StopInterpretation;

/**
 * @author maxime
 */
public final class Jcstop extends Instr {

    public Jcstop(AstObject astObject) {
        super(astObject);
    }

    @Override
    public void eval(Interpreter interpreter) {
        throw new StopInterpretation();
    }

    @Override
    public String toJajaCode() {
        return "jcstop";
    }
}
