package compiler.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import compiler.model.utils.Utils;
import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.JajaType;
import interpreter.Interpreter;
import memory.Obj;
import memory.Quadruplet;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;
import static java.util.Objects.requireNonNull;

/**
 * @author maxime
 */
public final class New extends Instr {

    private final String ident;
    private final JajaType type;
    private final Obj kind;
    private final int value;

    public New(AstObject astObject, String ident, JajaType type, Obj kind, int value) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
        this.type = requireNonNull(type);
        this.kind = requireNonNull(kind);
        this.value = value;
    }

    public String getIdent() {
        return ident;
    }

    public JajaType getType() {
        return type;
    }

    public Obj getKind() {
        return kind;
    }

    public int getValue() {
        return value;
    }


    @Override
    public String toJajaCode() {
        return String.format("new(%s,%s,%s,%d)",
                getIdent(), Utils.formatType(getType()), Utils.formatKind(getKind()), getValue());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .add("type", type)
                .add("kind", kind)
                .add("value", value)
                .toString();
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == New.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof New)) return false;
        if (!super.equals(o)) return false;

        New aNew = (New) o;
        if (!aNew.canEqual(this)) return false;
        if (value != aNew.value) return false;
        if (ident != null ? !ident.equals(aNew.ident) : aNew.ident != null) return false;
        if (kind != aNew.kind) return false;
        if (type != aNew.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (ident != null ? ident.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (kind != null ? kind.hashCode() : 0);
        result = 31 * result + value;
        return result;
    }

    @Override
    public void eval(Interpreter interpreter) {
        Quadruplet quad = new Quadruplet(getIdent(), getValue(), getKind(), getType());
        String quadIdent = quad.getIdent();
        boolean quadIsParam = quad.getValue() != 0 && quad.getObj() == Obj.VARIABLE;

        if (quadIdent != null && quad.getObj() != Obj.VCONSTANT) {
            int memValue;
            if (quadIsParam) {
                int index = interpreter.getMemory().getPile().size() - quad.getValue() - 1;
                //replace the stack quad with the new one but keep the old quad's value
                memValue = interpreter.getMemory().getPile().set(index, quad).getValue();
            } else {
                memValue = interpreter.getMemory().pop().getValue();
            }
            quad.setValue(memValue);
            interpreter.getMemory().addSymbol(quadIdent, quad);
        }

        if (!quadIsParam) {
            interpreter.getMemory().push(quad);
        }
        interpreter.setPC(interpreter.getPC() + 1);
    }
}
