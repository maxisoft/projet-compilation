package compiler.model;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Iterables;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;
import memory.Obj;
import memory.Quadruplet;

import java.util.Collections;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;

/**
 * @author maxime
 */
public final class Store extends Instr {

    private final String ident;

    public Store(AstObject astObject, String ident) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .toString();
    }

    @Override
    public String toJajaCode() {
        return String.format("store(%s)", getIdent());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Store)) return false;
        if (!super.equals(o)) return false;

        Store store = (Store) o;
        if (!store.canEqual(this)) return false;
        if (!ident.equals(store.ident)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        return result;
    }

    public String getIdent() {
        return ident;
    }

    @Override
    public void eval(Interpreter interpreter) {
        Quadruplet quad = Iterables.getLast(interpreter.getMemory().getPile());
        Integer value = quad.getValue();

        if (quad.getObj() == Obj.ARRAY){
            //special case array ptr copy
            interpreter.getMemory().getHeap().synonimArrays(ident, quad.getIdent());
            interpreter.getMemory().getPile().pop();
            value = 0;
        }else{
            interpreter.getMemory().pop();
        }

        Quadruplet newQuad = interpreter.getMemory().getLastSymbol(ident);
        newQuad.setValue(value);
        interpreter.setPC(interpreter.getPC() + 1);
    }
}
