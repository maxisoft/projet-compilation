package compiler.model;

import com.google.common.base.MoreObjects;
import fr.univfcomte.edu.ast.AstObject;
import interpreter.Interpreter;

import static compiler.model.utils.Utils.checkNonNullOrEmpty;

/**
 * @author maxime
 */
public final class AInc extends Instr {

    private final String ident;

    public AInc(AstObject astObject, String ident) {
        super(astObject);
        this.ident = checkNonNullOrEmpty(ident);
    }

    public String getIdent() {
        return ident;
    }

    @Override
    public void eval(Interpreter interpreter) {
        Integer value = interpreter.getMemory().pop().getValue();
        int index = interpreter.getMemory().pop().getValue();
        value += interpreter.getMemory().getHeap().getValueOf(ident, index);
        interpreter.getMemory().getHeap().affectValue(ident, index, value);
        interpreter.setPC(interpreter.getPC() + 1);
    }

    @Override
    public String toJajaCode() {
        return String.format("ainc(%s)", getIdent());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("astObject", getAstObject())
                .add("ident", ident)
                .toString();
    }

    @Override
    public boolean canEqual(Object o) {
        return o.getClass() == AInc.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AInc)) return false;
        if (!super.equals(o)) return false;

        AInc inc = (AInc) o;
        if (!inc.canEqual(this)) return false;
        if (!ident.equals(inc.ident)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ident.hashCode();
        return result;
    }
}
