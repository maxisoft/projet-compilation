package compiler;

import compiler.visitor.DataInterface;
import fr.univfcomte.edu.ast.AstObject;

/**
 * @author maxime
 */
public class Data implements DataInterface {

    private final AstObject parent;

    protected Data(AstObject parent) {
        this.parent = parent;
    }

    @Override
    public Integer getParamIndex() {
        return null;
    }

    @Override
    public AstObject getParent() {
        return parent;
    }
}
