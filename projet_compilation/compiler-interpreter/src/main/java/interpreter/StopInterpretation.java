package interpreter;

public class StopInterpretation extends RuntimeException {
    public StopInterpretation() {
    }

    public StopInterpretation(String message) {
        super(message);
    }

    public StopInterpretation(String message, Throwable cause) {
        super(message, cause);
    }

    public StopInterpretation(Throwable cause) {
        super(cause);
    }

    public StopInterpretation(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
