package interpreter;

import compiler.model.Instr;
import memory.Memory;

/**
 * @author maxime
 */
public interface Interpreter {

    int getPC();

    void setPC(int pc);

    Memory getMemory();

    Instr evalNextExpr();
}
