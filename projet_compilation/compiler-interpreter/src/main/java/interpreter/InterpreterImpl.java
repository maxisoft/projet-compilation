package interpreter;

import com.google.common.collect.ImmutableList;
import compiler.model.Instr;
import memory.Memory;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * @author maxime
 */
public class InterpreterImpl implements Interpreter {

    private final List<? extends Instr> instrs;
    private int pc;
    private Memory memory;

    public InterpreterImpl(List<? extends Instr> instrs) {
        pc = 1;
        memory = new Memory();
        this.instrs = ImmutableList.copyOf(requireNonNull(instrs));
    }

    @Override
    public int getPC() {
        return pc;
    }

    @Override
    public void setPC(int pc) {
        this.pc = pc;
    }

    @Override
    public Memory getMemory() {
        return memory;
    }

    @Override
    public Instr evalNextExpr() {
        Instr instr = instrs.get(getPC());
        instr.eval(this);
        return instr;
    }

}
