package interpreter;

import compiler.Compiler;
import fr.univfcomte.edu.analyzer.ParseException;
import fr.univfcomte.edu.ast.Analyzer;
import fr.univfcomte.edu.ast.JajaClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;

public final class Main {

    private Main() {


    }

    public static void main(String... args) throws URISyntaxException, FileNotFoundException, ParseException {
        URL resource = Main.class.getClassLoader().getResource("input.txt");
        if (resource == null) {
            throw new FileNotFoundException("no input.txt file");
        }
        File file = new File(resource.toURI());
        Analyzer parser = new Analyzer(file);
        JajaClass astRoot = parser.getASTRoot();

        Compiler compiler = new Compiler();
        astRoot.accept(compiler, null);
        Interpreter interpreter = new InterpreterImpl(compiler.getInstructions());
        boolean running = true;
        while (running) {
            try {
                interpreter.evalNextExpr();
            } catch (StopInterpretation e) {
                running = false;
            }
        }
    }
}
