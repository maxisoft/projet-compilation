package interpreter;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Created by maxime on 17/12/2014.
 */
public class StateHandler {
    final private BooleanProperty paused;
    final private BooleanProperty ended;
    final private IntegerProperty currentLine;

    public StateHandler() {
        this(false);
    }

    public StateHandler(boolean paused) {
        this.paused = new SimpleBooleanProperty(paused);
        ended = new SimpleBooleanProperty(false);
        currentLine = new SimpleIntegerProperty();
    }

    public synchronized boolean isPaused() {
        return this.paused.get();
    }

    public synchronized void setPaused(boolean paused) {
        this.paused.setValue(paused);
    }

    public BooleanProperty pausedProperty() {
        return this.paused;
    }

    public boolean isEnded() {
        return ended.get();
    }

    public synchronized void setEnded(boolean ended) {
        this.ended.setValue(ended);
    }

    public BooleanProperty endedProperty() {
        return this.ended;
    }

    public int getCurrentLine() {
        return currentLine.get();
    }

    public void setCurrentLine(int line) {
        currentLine.set(line);
    }

    public IntegerProperty currentLineProperty() {
        return currentLine;
    }

}
