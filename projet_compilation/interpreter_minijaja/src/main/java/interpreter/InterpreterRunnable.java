/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interpreter;

import fr.univfcomte.edu.ast.AstObject;
import memory.Memory;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author IkramAhmed
 */
public class InterpreterRunnable implements Runnable {
    
    private AstObject root;
    private final interpreter_minijaja  interpreter;
    private StateHandler stateHandler;
    
    
    public InterpreterRunnable(AstObject root){
        this.root=requireNonNull(root);
        interpreter = new interpreter_minijaja();
        stateHandler = new StateHandler();
        interpreter.setStateHandler(stateHandler);
    }
    
    @Override
    public  void run() {
        root.accept(interpreter, null);
        
    }

    public StateHandler getStateHandler() {
        return stateHandler;
    }

    public void continueInterpretation(){
        if (stateHandler.isPaused()){
            stateHandler.setPaused(false);
            synchronized (interpreter){
                interpreter.notify();
            }

        }
    }


    public Memory getMemory(){
        return interpreter.getMemory();
    }
     
    
}
