/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interpreter;

import fr.univfcomte.edu.ast.Analyzer;
import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.Compilateur;
import fr.univfcomte.edu.ast.JajaClass;
import java.io.File;
import java.net.URL;
import java.util.Scanner;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import type_controler.TypeControler;
import visitor.TypeException;


/**
 *
 * @author IkramAhmed
 */
public class test {
	
    public static void main(String args[]) throws Exception {
			
      URL resource = Compilateur.class.getClassLoader().getResource("test1.txt");
        if (resource == null) {
            throw new NullPointerException("no test1.txt file");
        }
        System.out.print(resource.toString());
        File file = new File(resource.toURI());
        Analyzer parser = new Analyzer(file);
        System.out.println("\n\n\n\n interpreteur minijaja: \n\n");
        AstObject astRoot = parser.getASTRoot();
        astRoot.accept(new interpreter_minijaja(), null);

        System.out.println("\n\n");
    }
}
