package interpreter;

import fr.univfcomte.edu.ast.*;
import fr.univfcomte.edu.ast.expr.*;
import memory.Memory;
import memory.Obj;
import memory.Pile;
import memory.Quadruplet;
import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author IkramAhmed
 *
 *
 */
public class interpreter_minijaja implements AstVisitor<Object, Object> {

    private Pile pile;
    private Memory memory;
    private List<JajaMeth> meths;
    private List<Quadruplet> retrait_entetes;
    private AstObject current;
    private int breakPoint;
    private boolean status;
    private boolean statis_exe;
    private ArrayList<String> stackTraces;

    private StateHandler stateHandler;

    public interpreter_minijaja() {
        this(new Memory());

    }

    public interpreter_minijaja(Memory memory) {
        this.memory = memory;
        this.pile = memory.getPile();
        meths = new ArrayList<JajaMeth>();
        retrait_entetes = new ArrayList<Quadruplet>();
        this.breakPoint = breakPoint;
        status = false;
        statis_exe = false;
        this.stackTraces = new ArrayList<String>();
    }

    @Override
    public Object visit(JajaAffect visitable, Object data) {
        pause(visitable);
        System.out.println(visitable.getLine());
        String id = visitable.getIdent();
        int value = (Integer) visitable.getExpr().accept(this, data);
        Quadruplet q = memory.getLastSymbol(id);
        q.setValue(value);
        return null;
    }

    @Override
    public Object visit(JajaCall visitable, Object data) {
        pause(visitable);
        String ident = visitable.getIdent();
        int nbParams = 0;
        if (visitable.getJajaParams() != null) {
            nbParams = visitable.getJajaParams().size();
        }
        
        if (visitable.getJajaMeth().getIdentifier().equals(visitable.getIdent()) && visitable.getJajaMeth().getParams().size() == nbParams) {
            for (int i = 0; i < nbParams; i++) {
                String id = visitable.getJajaMeth().getParams().get(i).getIdentifier();
                int value = (Integer) visitable.getJajaParams().get(i).accept(this, data);
                JajaType type = visitable.getJajaMeth().getParams().get(i).getType();
                Quadruplet q = new Quadruplet(id, value, Obj.VARIABLE, type);
                memory.addQuad(q);
                retrait_entetes.add(q);

            }
            visitable.getJajaMeth().getJajaVars().accept(this, data);
            visitable.getJajaMeth().getJajaInstrs().accept(this, data);
            System.out.println("mémoire avant retrait jajaCall : " + visitable.getJajaMeth().getIdentifier());
            System.out.println("=============================");
            visitable.getJajaMeth().getJajaVars().forEach(jajaVar -> { memory.pop(); });
            retrait_entetes.forEach(jajaVar -> {memory.pop();  });
            System.out.println("mémoire après retrait jajaCall");
            System.out.println("=============================");

        }
        return null;
    }

    @Override
    public Object visit(JajaClass visitable, Object data) {
        pause(1);
        String name = visitable.getName();
        System.out.println(name);
        Quadruplet q = new Quadruplet(name, 0, Obj.VARIABLE, null);
        memory.addQuad(q);
        stackTraces.add(memory.toString());
        visitable.getJajaDecls().accept(this, data);
        visitable.getJajaMain().accept(this, data);
        //visitable.getJajaDecls().forEach(JajaDecl -> {  memory.pop(); });
        //memory.pop();
        System.out.println("fin jajaClass (il doit rester uniquement la classe mémoire)");
        System.out.println(memory.toString());
        System.out.println("======================");
        if (stateHandler != null){
            stateHandler.setEnded(true);
        }

        return null;

    }

    @Override
    public Object visit(JajaDecls visitable, Object data) {
        for (JajaDecl decl : visitable) {
            decl.accept(this, data);
        }
        return null;
    }

    @Override
    public Object visit(JajaElse visitable, Object data) {
        visitable.getJajaInstrs().accept(this, data);
        return null;
    }

    @Override
    public Object visit(JajaIf visitable, Object data) {
        pause(visitable);
        int v = (Integer) visitable.getExpr().accept(this, data);
        JajaElse jajaElse = visitable.getJajaElse();
        if (v != 0) {
            if (jajaElse != null) {
                jajaElse.accept(this, data);
            }

        } else {
            visitable.getJajaInstrs().accept(this, data);

        }
        return null;
    }

    @Override
    public Object visit(JajaIncrement visitable, Object data) {
        pause(visitable);
        String id = visitable.getIdent();
        if (visitable.getOperator() == "++") { 
            Quadruplet q = memory.getLastSymbol(id);
            int value = q.getValue();
            value = value + 1;
            q.setValue(value);
        }

        return null;
    }

    @Override
    public Object visit(JajaMain visitable, Object data) {

        System.out.println("début JajaMain");
        System.out.println("======================");
        visitable.getJajaVars().accept(this, data);
        visitable.getJajaInstrs().accept(this, data);
       // visitable.getJajaVars().forEach(jajaVar -> { memory.pop(); });

        return null;
    }

    @Override
    public Object visit(JajaMeth visitable, Object data) {
        System.out.println("debut de la methode");
        meths.add(visitable);
        JajaType type = visitable.getType();
        String ident = visitable.getIdentifier();
        Quadruplet q = new Quadruplet(ident, 0, Obj.METHOD, type);
        memory.addQuad(q);

        return null;
    }

    @Override
    public Object visit(JajaParam visitable, Object data) {
        String id = visitable.getIdentifier();
        JajaType kind = visitable.getType();
        Quadruplet q = new Quadruplet(id, 0, Obj.VARIABLE, kind);
        memory.addQuad(q);
        return null;
    }

    @Override
    public Object visit(JajaReturn visitable, Object data) {
        return visitable.getExpr().accept(this, data);
    }

    @Override
    public Object visit(JajaVar visitable, Object data) {
        pause(visitable);
        System.out.println("début JajaVar");
        System.out.println("======================");
        JajaType type = visitable.getType();
        String id = visitable.getIdentifier();
        Object value = visitable.getValueExpr().accept(this, data);
        if (value != null) {
            Quadruplet q = new Quadruplet(id, (Integer) value, Obj.VARIABLE, type);
            memory.addQuad(q);

        } else {
            //DO VAR DONT YET VALUE
            Quadruplet q = new Quadruplet(id, null, Obj.VARIABLE, type);
            memory.addQuad(q);
        }
        return null;
    }

    @Override
    public Object visit(JajaVars visitable, Object data) {
        for (JajaVar var : visitable) {
            var.accept(this, data);
        }
        return null;
    }

    @Override
    public Object visit(JajaWhile visitable, Object data) {
        pause(visitable);
        System.out.println("début Jajawhile");
        while ((Integer) visitable.getExpr().accept(this, data) == 0) {
            if (visitable.getJajaInstrs().get(0) instanceof JajaIncrement) {
                JajaIncrement h = (JajaIncrement) visitable.getJajaInstrs().get(0);
                 pause(visitable);
                if (h.getOperator().equals("--")) {
                    break;
                }

            }
            visitable.getJajaInstrs().accept(this, data);

        }
        return null;
    }

    @Override
    public Object visit(JajaAnd jajaAnd, Object data) {
        int v1 = (Integer) jajaAnd.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaAnd.getSonsExprs().get(1).accept(this, data);
        if ((v1 & v2) != 0 | v1 != 1 & v2 != 0 | v1 != 0 & v2 != 1) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public Object visit(JajaDiv jajaDiv, Object data) {
        int v1 = (Integer) jajaDiv.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaDiv.getSonsExprs().get(1).accept(this, data);
        int res = 0;
        if (v2 != 0) {
            res = v1 / v2;
        } else {
            System.out.println("erreur division par zero impossible");
        }
        return res;
    }

    @Override
    public Object visit(JajaEq jajaEq, Object data) {
        int v1 = (Integer) jajaEq.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaEq.getSonsExprs().get(1).accept(this, data);
        if (v1 == v2) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public Object visit(JajaMinus jajaMinus, Object data) {
        int v1 = (Integer) jajaMinus.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaMinus.getSonsExprs().get(1).accept(this, data);
        int res;
        res = v1 - v2;
        return res;
    }

    @Override
    public Object visit(JajaMult jajaMult, Object data) {
        int v1 = (Integer) jajaMult.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaMult.getSonsExprs().get(1).accept(this, data);
        int res;
        res = v1 * v2;
        return res;
    }

    @Override
    public Object visit(JajaOr jajaOr, Object data) {
        int v1 = (Integer) jajaOr.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaOr.getSonsExprs().get(1).accept(this, data);
        if ((v1 & v2) != 1 | v1 != 1 & v2 != 0 | v1 != 0 & v2 != 1) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public Object visit(JajaPlus jajaPlus, Object data) {
        int v1 = (Integer) jajaPlus.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaPlus.getSonsExprs().get(1).accept(this, data);
        int res;
        res = v1 + v2;
        return res;
    }

    @Override
    public Object visit(JajaSup jajaSup, Object data) {
        System.out.println("début JajaSup");
        System.out.println(memory.getPile().toString());
        System.out.println("======================");
        int v1 = (Integer) jajaSup.getSonsExprs().get(0).accept(this, data);
        int v2 = (Integer) jajaSup.getSonsExprs().get(1).accept(this, data);
        if ((v1 > v2) == true) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public Object visit(JajaUnaryMinus jajaUnaryMinus, Object data) {
        int v = (Integer) jajaUnaryMinus.getSonsExprs().get(0).accept(this, data);
        v = -v;
        return v;
    }

    @Override
    public Object visit(JajaInstrs jajaInstrs, Object data) {
        for (JajaInstr instr : jajaInstrs) {
            System.out.println(jajaInstrs.getLine());
            instr.accept(this, data);
            pause(instr.getLine());
        }
        return null;
    }

    @Override
    public Object visit(JajaExprs jajaExprs, Object data) {
        for (JajaExpr expr : jajaExprs) {
            expr.accept(this, data);
        }
        return null;
    }

    @Override
    public Object visit(JajaNotSetExpr jajaNotSetExpr, Object data) {
        // DO NOTHING
        return null;
    }

    @Override
    public Object visit(JajaParams jajaParams, Object data) {
        for (JajaParam param : jajaParams) {
            param.accept(this, data);
        }
        return null;
    }

    @Override
    public Object visit(JajaBoolean jajaBoolean, Object data) {
        return jajaBoolean.getBooleanValue() ? 1 : 0;
    }

    @Override
    public Object visit(JajaIdent jajaIdent, Object data) {
        String id = jajaIdent.getValue();
        return memory.getLastSymbol(id).getValue();
    }

    @Override
    public Object visit(JajaNumber jajaNumber, Object data) {
        return jajaNumber.getIntegerValue();
    }

    @Override
    public Object visit(JajaVarArray jajaVarArray, Object data) {
        String ident = jajaVarArray.getIdentifier();
        JajaType type  = jajaVarArray.getType();
        Object value = jajaVarArray.getIndexExpr().accept(this, data);
        memory.getHeap().declareArray(ident,(Integer)value);
        return null;
    }

    @Override
    public Object visit(JajaAffectArray jajaAffectArray, Object data) {
        String ident =  jajaAffectArray.getIdent();
        int value = (Integer) jajaAffectArray.getExpr().accept(this, data);
        int indice = (Integer)jajaAffectArray.getIndexExpr().accept(this, data);
        memory.getHeap().affectValue(ident, indice, value);
        return null;
    }

    @Override
    public Object visit(JajaCallExpr visitable, Object data) {

        return null;
    }

    @Override
    public Object visit(JajaIdentArray jajaIdentArray, Object data){
       String ident = jajaIdentArray.getValue();
       int indice = (Integer) jajaIdentArray.getIndexExpr().accept(this, data);
       return memory.getHeap().getValueOf(ident, indice);
       
    }

    @Override
    public Object visit(JajaNot jaJaNot, Object data) {
        int v = (Integer) jaJaNot.getSonsExprs().get(0).accept(this, data);
        if (v != 0) {
            return 0;
        } else {
            return 1;
        }
    }
    public void pause(AstObject astObject){
        pause(astObject.getLine());
    }
    public synchronized void pause(Integer line)  {
        if (stateHandler != null){
            stateHandler.setPaused(true);
            if (line != null) {
                stateHandler.setCurrentLine(line);
            }else {
                System.err.println("Line is not set");
            }

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setStateHandler(StateHandler stateHandler) {
        this.stateHandler = stateHandler;
    }

    public Memory getMemory() {
        return memory;
    }
}
