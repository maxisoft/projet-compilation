/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interpreter;


import fr.univfcomte.edu.ast.AstObject;
import fr.univfcomte.edu.ast.JajaAffect;
import fr.univfcomte.edu.ast.JajaBoolean;
import fr.univfcomte.edu.ast.JajaCall;
import fr.univfcomte.edu.ast.JajaCallExpr;
import fr.univfcomte.edu.ast.JajaClass;
import fr.univfcomte.edu.ast.JajaDecl;
import fr.univfcomte.edu.ast.JajaDecls;
import fr.univfcomte.edu.ast.JajaElse;
import fr.univfcomte.edu.ast.JajaExpr;
import fr.univfcomte.edu.ast.JajaExprs;
import fr.univfcomte.edu.ast.JajaIdent;
import fr.univfcomte.edu.ast.JajaIdentArray;
import fr.univfcomte.edu.ast.JajaIf;
import fr.univfcomte.edu.ast.JajaIncrement;
import fr.univfcomte.edu.ast.JajaInstr;
import fr.univfcomte.edu.ast.JajaInstrs;
import fr.univfcomte.edu.ast.JajaMain;
import fr.univfcomte.edu.ast.JajaMeth;
import fr.univfcomte.edu.ast.JajaNumber;
import fr.univfcomte.edu.ast.JajaParam;
import fr.univfcomte.edu.ast.JajaParams;
import fr.univfcomte.edu.ast.JajaReturn;
import fr.univfcomte.edu.ast.JajaType;
import fr.univfcomte.edu.ast.JajaVar;
import fr.univfcomte.edu.ast.JajaVars;
import fr.univfcomte.edu.ast.JajaWhile;
import fr.univfcomte.edu.ast.expr.JajaAnd;
import fr.univfcomte.edu.ast.expr.JajaDiv;
import fr.univfcomte.edu.ast.expr.JajaEq;
import fr.univfcomte.edu.ast.expr.JajaMinus;
import fr.univfcomte.edu.ast.expr.JajaMult;
import fr.univfcomte.edu.ast.expr.JajaNot;
import fr.univfcomte.edu.ast.expr.JajaNotSetExpr;
import fr.univfcomte.edu.ast.expr.JajaOr;
import fr.univfcomte.edu.ast.expr.JajaPlus;
import fr.univfcomte.edu.ast.expr.JajaSup;
import fr.univfcomte.edu.ast.expr.JajaUnaryMinus;
import javafx.collections.ObservableList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import memory.Memory;
import memory.Obj;
import memory.Pile;
import memory.Obj;
import memory.Quadruplet;
import static org.junit.Assert.assertEquals;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;






/**
 *
 * @author IkramAhmed
 */
public class interpreter_minijajaTest {
    
    
    private interpreter_minijaja interpreter;
    @Mock
    private Memory memory;
  
 
 
   
  @Before
    public void setUp() throws Exception {
       MockitoAnnotations.initMocks(this);
       interpreter = new interpreter_minijaja(memory);
       
       
      
    }
    /*  
   @Test
    public void testVisitJajaAffect() throws Exception {
         fail("not yet implemented");
      
        }
    
    
  @Test
  public void testVisitJajaCall() throws Exception{
      
      fail("not yet implemented");
      
  }
      
  }

  @Test
  public void testVisitJajaIncrement() throws Exception{
     
        fail("not yet implemented");
      
   }
  
    /*
  @Test
  public void testVisitJajaClass() throws Exception{
      
      fail("not yet implemented");
        
    
       }
  
  @Test
  public void testVisitJajaVar() throws Exception{
       JajaVar var = new JajaVar();
       var.setIdentifier("f");
       JajaNumber value = new JajaNumber();
       value.setValue(9);
       var.setValueExpr(value);
       Mockito.doReturn(new Quadruplet("f",9,Obj.VARIABLE,JajaType.INT)).when(memory).addQuad(null);
       
       
 }*/
  
  @Test
  public void testVisitJajaDiv() throws Exception{
      JajaDiv div = new JajaDiv();
      div.setOperator("/");
      JajaExprs number1 = new JajaExprs();
      div.setSonsExprs(number1);
      number1.add(new JajaNumber(4));
      number1.add(new JajaNumber(2));
      
      Assert.assertEquals(2,div.accept(interpreter, null));
      
     
      
  }
  
  @Test
  public void testVisitJajaPlus() throws Exception{
      JajaPlus plus = new JajaPlus();
      plus.setOperator("+");
      JajaExprs number1 = new JajaExprs();
      plus.setSonsExprs(number1);
      number1.add(new JajaNumber(4));
      number1.add(new JajaNumber(1));
      
      
      Assert.assertEquals(5,plus.accept(interpreter, null));
      
      
  }
  
  @Test
  public void testVisitJajaMinus() throws Exception{
     JajaMinus minus = new JajaMinus();
      minus.setOperator("-");
      JajaExprs number1 = new JajaExprs();
      minus.setSonsExprs(number1);
      number1.add(new JajaNumber(4));
      number1.add(new JajaNumber(3));
      
      Assert.assertEquals(1,minus.accept(interpreter, null));
    
   }
  
  @Test
  public void testVisitJajaMult() throws Exception{
      JajaMult mul = new JajaMult();
      mul.setOperator("*");
      JajaExprs number1 = new JajaExprs();
      mul.setSonsExprs(number1);
      number1.add(new JajaNumber(3));
      number1.add(new JajaNumber(4));
      
      Assert.assertEquals(12,mul.accept(interpreter, null));
    
    }
  
  @Test
  public void testVisitJajaIdent() throws Exception{
     JajaIdent ident = new JajaIdent();
     ident.setIdent("x");
     Mockito.doReturn(new Quadruplet(null,3,null,null)).when(memory).getLastSymbol("x");
     Assert.assertEquals(3, ident.accept(interpreter, null));
   
  }
  
  @Test
  public void testVisitJajaUnaryMinus() throws Exception{
     JajaUnaryMinus number = new JajaUnaryMinus();
    JajaExprs num= new JajaExprs();
      number.setSonsExprs(num);
      num.add(new JajaNumber(3));
      Assert.assertEquals(-3,number.accept(interpreter, null));
    
      
  }
  
  @Test
  public void testVisitJajaNumber() throws Exception{
      JajaNumber ident = new JajaNumber();
      ident.setValue("4");
      Assert.assertEquals(4, ident.accept(interpreter, null));
      
  }

    
}