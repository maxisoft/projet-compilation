package tp_gl.robotv1;


public enum Instruction {
    TURNLEFT,
    BACKWARD,
    TURNRIGHT,
    FORWARD
}
