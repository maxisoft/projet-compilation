package visitor;

/**
 * Generic class that define how to implement Visitable; used for visitor pattern.<p>
 * Note : due to java's type erasure, this interface can't be used more than once on the same class.<br>
 * See <a href="http://programmers.stackexchange.com/questions/219427/implementing-multiple-generic-interfaces-in-java">
 *     implementing-multiple-generic-interfaces-in-java</a> for more.
 * @param <R> the return type of this visitor's methods.  Use {@link
 *            Void} for visitors that do not need to return results.
 * @param <P> the type of the additional parameter to this visitor's
 *            methods.  Use {@link Void} for visitors that do not need an
 *            additional parameter.
 *
 * @author maxime
 */
public interface AstVisitable<R,P> {
    R accept(AstVisitor<? extends R, P> visitor, P data);
}
