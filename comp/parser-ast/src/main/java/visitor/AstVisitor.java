package visitor;

import fr.univfcomte.edu.ast.*;

/**
 * @param <R> the return type of this visitor's methods.  Use {@link
 *            Void} for visitors that do not need to return results.
 * @param <P> the type of the additional parameter to this visitor's
 *            methods.  Use {@link Void} for visitors that do not need an
 *            additional parameter.
 *
 * @author maxime
 */
public interface AstVisitor<R,P> {
    R visit(JajaAffect visitable, P data);
    R visit(JajaCall visitable, P data);
    R visit(JajaClass visitable, P data);
    R visit(JajaDecl visitable, P data);
    R visit(JajaDecls visitable, P data);
    R visit(JajaElse visitable, P data);
    R visit(JajaExpr visitable, P data);
    R visit(JajaIf visitable, P data);
    R visit(JajaIncrement visitable, P data);
    R visit(JajaInstr visitable, P data);
    R visit(JajaLogicExpr visitable, P data);
    R visit(JajaMain visitable, P data);
    R visit(JajaMeth visitable, P data);
    R visit(JajaMeths visitable, P data);
    R visit(JajaObject visitable, P data);
    R visit(JajaParam visitable, P data);
    R visit(JajaReturn visitable, P data);
    R visit(JajaTypeMeth visitable, P data);
    R visit(JajaVar visitable, P data);
    R visit(JajaVars visitable, P data);
    R visit(JajaWhile visitable, P data);
}
