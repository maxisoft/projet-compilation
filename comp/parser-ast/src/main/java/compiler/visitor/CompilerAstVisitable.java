package compiler.visitor;

import visitor.AstVisitor;

/**
 * An interface that define an accept method according to the compiler's visitor pattern.<br>
 * Based on {@link visitor.AstVisitable}
 * @author maxime
 */
public interface CompilerAstVisitable{
    void accept(AstVisitor<? extends Void, String> visitor, String data);
}
