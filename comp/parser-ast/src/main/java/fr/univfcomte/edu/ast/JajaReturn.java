/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaReturn extends JajaInstr {

    private JajaExpr expr;

    public JajaReturn() {
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public void setExpr(JajaExpr expr) {
        this.expr = expr;
    }
}
