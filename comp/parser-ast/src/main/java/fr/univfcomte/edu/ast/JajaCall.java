/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.List;

/**
 *
 * @author olivier
 */
public class JajaCall extends JajaInstr {

    private List<JajaExpr> jajaParams;

    public JajaCall() {
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public List<JajaExpr> getJajaParams() {
        return jajaParams;
    }

    public void setJajaParams(List<JajaExpr> jajaParams) {
        this.jajaParams = jajaParams;
    }
}
