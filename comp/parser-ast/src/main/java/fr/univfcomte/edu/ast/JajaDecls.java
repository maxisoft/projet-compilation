/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author olivier
 */
public abstract class JajaDecls extends AstObject{

    private List<JajaDecl> jajaDecls;

    /**
     * ****************
     * CONSTRUCTORS ***************
     */
    public JajaDecls() {
        jajaDecls = new ArrayList<JajaDecl>();
    }


    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }
    /**
     * ****************
     * GETTERS ***************
     */
    public List<JajaDecl> getJajaDecls() {
        return jajaDecls;
    }

    /**
     * ****************
     * SETTERS ***************
     */
    public void setJajaDecls(List<JajaDecl> jajaDecls) {
        this.jajaDecls = jajaDecls;

    }

    @Override
    public String toString() {

        String res = "";
        Iterator<JajaDecl> iterator = jajaDecls.iterator();
        while (iterator.hasNext()) {
            res += iterator.next().toString();
        }

        return res;
    }
}
