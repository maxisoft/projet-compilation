/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import java.util.ArrayList;
import java.util.List;
import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaMeth extends JajaDecl {

    private List<JajaVar> params;
    private List<JajaInstr> jajaInstrs;
    private List<JajaVar> jajaVars;

    public JajaMeth() {
        params = new ArrayList<JajaVar>();
        jajaInstrs = new ArrayList<JajaInstr>();
        jajaVars = new ArrayList<JajaVar>();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public List<JajaVar> getParams() {
        return params;
    }

    public List<JajaInstr> getJajaInstrs() {
        return jajaInstrs;
    }

    public List<JajaVar> getJajaVars() {
        return jajaVars;
    }

    public void setParams(List<JajaVar> params) {
        this.params = params;
    }

    public void setJajaInstrs(List<JajaInstr> jajaInstrs) {
        this.jajaInstrs = jajaInstrs;
    }

    public void setJajaVars(List<JajaVar> jajaVars) {
        this.jajaVars = jajaVars;
    }
}
