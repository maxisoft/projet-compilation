/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olivier
 */
public class JajaWhile extends JajaInstr {

    private JajaExpr expr;
    private List<JajaInstr> jajaInstrs;

    public JajaWhile() {

        expr = new JajaExpr() {
            @Override
            public void accept(AstVisitor<? extends Void, String> visitor, String data) {
                throw new IllegalStateException("should not be called");
            }
        };
        jajaInstrs = new ArrayList<JajaInstr>();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaExpr getExpr() {
        return expr;
    }

    public List<JajaInstr> getJajaInstrs() {
        return jajaInstrs;
    }

    public void setExpr(JajaExpr expr) {
        this.expr = expr;
    }

    public void setJajaInstrs(List<JajaInstr> jajaInstrs) {
        this.jajaInstrs = jajaInstrs;
    }
}
