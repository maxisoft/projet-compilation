/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaVar extends JajaDecl {

    private JajaExpr valueExpr;
    private String type;

    public JajaVar() {
        valueExpr = new JajaExpr() {
            @Override
            public void accept(AstVisitor<? extends Void, String> visitor, String data) {
                throw new IllegalStateException("should not be called");
            }
        };
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public JajaExpr getValueExpr() {
        return valueExpr;
    }

    public String getType() {
        return type;
    }

    public void setValueExpr(JajaExpr valueExpr) {
        this.valueExpr = valueExpr;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        String res = super.toString();
        return res + " \tvalueExpr=\n" + valueExpr.toString() + '}';
    }
}
