/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaInstr extends AstObject{

    private String ident;
    private String operator;
    private JajaExpr expr;

    public JajaInstr() {
        expr = new JajaExpr() {
            @Override
            public void accept(AstVisitor<? extends Void, String> visitor, String data) {
                throw new IllegalStateException("should not be called");
            }
        };
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public String getIdent() {
        return ident;
    }

    public String getOperator() {
        return operator;
    }

    public JajaExpr getExpr() {
        return expr;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setExpr(JajaExpr expr) {
        this.expr = expr;
    }

}
