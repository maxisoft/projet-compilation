/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

/**
 *
 * @author olivier
 */
public abstract class JajaObject extends AstObject {
    
    private String name;

    /******************
     * CONSTRUCTORS
     *****************/
    
    public JajaObject() {
    }
    
    /******************
     * GETTERS
     *****************/

    public String getName() {
        return name;
    }

    /******************
     * SETTERS
     *****************/
    
    public void setName(String name) {
        this.name = name;
    }
}
