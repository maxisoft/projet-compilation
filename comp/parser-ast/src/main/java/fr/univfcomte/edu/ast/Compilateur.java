/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import fr.univfcomte.edu.analyzer.ParseException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 *
 * @author olivier
 */
public class Compilateur {

    /**
     * @param args the command line arguments
     * @throws fr.univfcomte.edu.analyzer.ParseException
     */
    public static void main(String[] args) throws ParseException, IOException, URISyntaxException {
        // TODO code application logic here
        URL resource = Compilateur.class.getClassLoader().getResource("input.txt");
        if (resource == null){
            throw new NullPointerException("no input.txt file");
        }
        File file = new File(resource.toURI());
        Analyzer parser = new Analyzer(file);
        parser.Start();
    }

}
