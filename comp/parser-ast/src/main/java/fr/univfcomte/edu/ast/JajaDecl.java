/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public abstract class JajaDecl extends AstObject{
    protected String identifier;
    protected JajaTypeMeth jajaTypeMeth;
    private boolean finalDecl;

    public JajaDecl() {
        jajaTypeMeth = new JajaTypeMeth();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public String getIdentifier() {
        return identifier;
    }

    public JajaTypeMeth getJajaTypeMeth() {
        return jajaTypeMeth;
    }

    public boolean isFinalDecl() {
        return finalDecl;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setJajaTypeMeth(JajaTypeMeth jajaTypeMeth) {
        this.jajaTypeMeth = jajaTypeMeth;
    }

    public void setFinalDecl(boolean finalDecl) {
        this.finalDecl = finalDecl;
    }

    @Override
    public String toString() {
        boolean var = this instanceof JajaVar;
        return "JajaDecl{" + "line=" + line + ", column=" + column + ", identifier=" + identifier + ", jajaTypeMeth=" + jajaTypeMeth + ",    " + var + "}\n";
    }
}
