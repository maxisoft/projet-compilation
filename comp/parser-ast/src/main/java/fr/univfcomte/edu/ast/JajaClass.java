/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

/**
 *
 * @author olivier
 */
public class JajaClass extends JajaObject {

    private JajaMain jajaMain;
    private JajaDecls jajaDecls;

    /**
     * ****************
     * CONSTRUCTORS ***************
     */
    public JajaClass() {
        jajaDecls = new JajaDecls() {
            @Override
            public void accept(AstVisitor<? extends Void, String> visitor, String data) {
                throw new IllegalStateException("should not be called");
            }
        };
    }


    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    /**
     * ****************
     * GETTERS ***************
     */
    public JajaMain getJajaMain() {
        return jajaMain;
    }

    public JajaDecls getJajaDecls() {
        return jajaDecls;
    }

    /**
     * ****************
     * SETTERS ***************
     */
    public void setJajaMain(JajaMain jajaMain) {
        this.jajaMain = jajaMain;
    }

    public void setJajaDecls(JajaDecls jajaDecls) {
        this.jajaDecls = jajaDecls;
    }
}
