/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olivier
 */
public class JajaIf extends JajaInstr {

    private List<JajaInstr> jajaInstrs;
    private JajaElse jajaElse = null;

    public JajaIf() {
        jajaInstrs = new ArrayList<JajaInstr>();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public List<JajaInstr> getJajaInstrs() {
        return jajaInstrs;
    }

    public JajaElse getJajaElse() {
        if (jajaElse == null) {
            jajaElse = new JajaElse();
        }
        return jajaElse;
    }

    public void setJajaInstrs(List<JajaInstr> jajaInstrs) {
        this.jajaInstrs = jajaInstrs;
    }

    public void setJajaElse(JajaElse jajaElse) {
        this.jajaElse = jajaElse;
    }

}
