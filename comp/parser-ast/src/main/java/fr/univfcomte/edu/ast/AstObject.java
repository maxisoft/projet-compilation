package fr.univfcomte.edu.ast;

import compiler.visitor.CompilerAstVisitable;
import visitor.AstVisitable;

/**
 * @author maxime
 */
public abstract class AstObject implements CompilerAstVisitable{
    // TODO change Integer -> int when every subclasses has line property
    protected Integer line;
    // TODO change Integer -> int when every subclasses has column property
    protected Integer column;

    public Integer getLine() {
        return line;
    }

    public void setLine(int line) {
        if (line < 0){
            throw new IllegalArgumentException("line argument must me greater than 0");
        }
        this.line = line;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(int column) {
        if (column < 0){
            throw new IllegalArgumentException("column argument must me greater than 0");
        }
        this.column = column;
    }
}
