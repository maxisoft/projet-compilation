/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olivier
 */
public class JajaMain extends AstObject{

    private JajaVars jajaVars;
    private List<JajaInstr> jajaInstrs;

    /**
     * ****************
     * CONSTRUCTORS ***************
     */
    public JajaMain() {
        jajaVars = new JajaVars();
        jajaInstrs = new ArrayList<JajaInstr>();
    }


    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    /**
     * ****************
     * GETTERS ***************
     */

    public JajaVars getJajaVars() {
        return jajaVars;
    }

    public List<JajaInstr> getJajaInstrs() {
        return jajaInstrs;
    }

    /**
     * ****************
     * SETTERS ***************
     */

    public void setJajaVars(JajaVars jajaVars) {
        this.jajaVars = jajaVars;
    }

    public void setJajaInstrs(List<JajaInstr> jajaInstrs) {
        this.jajaInstrs = jajaInstrs;
    }
}
