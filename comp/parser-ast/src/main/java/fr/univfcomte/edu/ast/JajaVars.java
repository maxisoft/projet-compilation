/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olivier
 */
public class JajaVars extends JajaDecls {

    private List<JajaVar> jajaVars;

    public JajaVars() {
        jajaVars = new ArrayList<JajaVar>();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public List<JajaVar> getJajaVars() {
        return jajaVars;
    }

    public void setJajaVars(List<JajaVar> jajaVars) {
        this.jajaVars = jajaVars;
    }
}
