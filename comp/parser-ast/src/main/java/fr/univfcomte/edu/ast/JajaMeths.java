/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olivier
 */
public class JajaMeths extends JajaDecls {

    private List<JajaMeth> jajaMeths;

    public JajaMeths() {
        jajaMeths = new ArrayList<JajaMeth>();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public List<JajaMeth> getJajaMeths() {
        return jajaMeths;
    }

    public void setJajaMeths(List<JajaMeth> jajaMeths) {
        this.jajaMeths = jajaMeths;
    }
}
