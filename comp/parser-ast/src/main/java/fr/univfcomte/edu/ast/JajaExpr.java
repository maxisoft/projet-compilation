/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univfcomte.edu.ast;

import visitor.AstVisitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author olivier
 */
public abstract class JajaExpr extends AstObject{

    protected List<JajaExpr> sonsExprs;
    protected String value;
    protected String operator;
    private int beginLine, beginColumn;

    public JajaExpr() {
        sonsExprs = new ArrayList<JajaExpr>();
    }

    @Override
    public void accept(AstVisitor<? extends Void, String> visitor, String data) {
        visitor.visit(this, data);
    }

    public String getValue() {
        return value;
    }

    public List<JajaExpr> getSonsExprs() {
        return sonsExprs;
    }

    public String getOperator() {
        return operator;
    }

    public int getBeginLine() {
        return beginLine;
    }

    public int getBeginColumn() {
        return beginColumn;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setSonsExprs(List<JajaExpr> sonsExprs) {
        this.sonsExprs = sonsExprs;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setBeginLine(int beginLine) {
        this.beginLine = beginLine;
    }

    public void setBeginColumn(int beginColumn) {
        this.beginColumn = beginColumn;
    }

    @Override
    public String toString() {
        String res = "";

        if (value != null && !value.equals("")) {
            return value;
        }

        Iterator<JajaExpr> iterator = this.sonsExprs.iterator();
        while (iterator.hasNext()) {
            res += iterator.next().toString() + " " + this.operator + "\n";
        }
        return res + "\n";
    }
}
