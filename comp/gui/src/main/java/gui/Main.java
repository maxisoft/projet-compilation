package gui;

import com.google.common.io.CharStreams;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {

    private static Main instance;

    private Controller controller;
    private FXMLLoader fxmlLoader;
    private File file;
    private ScheduledExecutorService executor;
    private JavaScriptInterface jsInterface;

    public Main() {
        instance = this;
        executor = Executors.newSingleThreadScheduledExecutor();
        jsInterface = new JavaScriptInterface();
    }

    public static Main getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage, File file) throws Exception {
        this.file = file;
        start(primaryStage);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL fxmlLocation = getClass().getClassLoader().getResource("main.fxml");
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(fxmlLocation);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = fxmlLoader.load();

        controller = fxmlLoader.getController();

        WebView codeWebView = controller.getCodeWebView();
        codeWebView.addEventFilter(KeyEvent.KEY_PRESSED, controller::codeWebViewCopyPaste);
        codeWebView.getEngine().load(getClass().getClassLoader().getResource("ace/editor.html").toExternalForm());

        initWebView();

        primaryStage.setTitle("Some random title");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private void initWebView() {
        WebEngine engine = controller.getCodeWebView().getEngine();
        engine.getLoadWorker().stateProperty().addListener(
                (ObservableValue<? extends State> ov, State oldState,
                 State newState) -> {
                    if (newState == State.SUCCEEDED) {
                        injectInterface();

                        if (file != null) {
                            try (Reader fr = new FileReader(file)) {
                                String code = CharStreams.toString(fr);
                                loadCodeIntoEditor(code);
                            } catch (IOException e) {
                                showErrorDialog(e);
                            }
                        } else {
                            loadCodeIntoEditor("");
                        }
                    }
                });
    }

    private void showErrorDialog(Throwable throwable) {
        Logger.getAnonymousLogger().log(Level.WARNING, "", throwable);
        //TODO show a error dialog
    }

    /**
     * TODO
     * lazily cast the return value
     *
     * @param code
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    private <T> T runJavascriptCode(String code) {
        return (T) controller.getCodeWebView().getEngine().executeScript(code);
    }

    private void injectInterface() {
        JSObject win = runJavascriptCode("window");
        win.setMember("$ji", jsInterface);
    }

    private void loadCodeIntoEditor(String code) {
        jsInterface.setCode(code);
        runJavascriptCode("editor.setValue($ji.getCode())");
    }

    private String getEditorCode() {
        runJavascriptCode("$ji.setCode(editor.getValue())");
        return jsInterface.getCode();
    }
}
