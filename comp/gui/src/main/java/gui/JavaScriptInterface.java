package gui;

/**
 * Class that define shared objects between java and javascript.<p>
 * Used by the code web view.
 *
 * @author maxime
 */
public class JavaScriptInterface {
    private String code;

    public JavaScriptInterface(String code) {
        this.code = code;
    }

    public JavaScriptInterface() {
        this("");
    }

    public synchronized String getCode() {
        return code;
    }

    public synchronized void setCode(String code) {
        this.code = code;
    }
}
