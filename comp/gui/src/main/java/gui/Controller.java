package gui;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller {
    @FXML
    private Button saveBtn;
    @FXML
    private WebView codeWebView;
    @FXML
    private Button consolleClearBtn;

    @FXML
    void save(ActionEvent event) {
        Logger.getAnonymousLogger().log(Level.INFO, "clicked !");
    }

    @FXML
    private void consolleClearBtnMouseEnter(MouseEvent event) {
        FadeTransition ft = new FadeTransition(Duration.millis(250), getConsolleClearBtn());
        ft.setToValue(0.9);
        ft.play();
    }


    @FXML
    private void consolleClearBtnMouseExit(MouseEvent event) {
        FadeTransition ft = new FadeTransition(Duration.millis(450), getConsolleClearBtn());
        ft.setToValue(0.6);
        ft.play();
    }

    @FXML
    private void fileDragDrop(DragEvent event) throws Exception {
        Dragboard dragboard = event.getDragboard();
        boolean success = false;
        if (dragboard.hasFiles()) {
            success = true;
            for (File file : dragboard.getFiles()) {
                Main.getInstance().start(new Stage(), file);
                break;
            }
        }
        event.setDropCompleted(success);
        event.consume();
    }

    @FXML
    private void fileDragOver(DragEvent event) {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        } else {
            event.consume();
        }
    }

    public void codeWebViewCopyPaste(KeyEvent event) {
        WebEngine engine = getCodeWebView().getEngine();
        if (event.getCode().equals(KeyCode.C) || event.getCode().equals(KeyCode.X) && event.isControlDown()) {
            String selectedText = (String) engine.executeScript(
                    "editor.session.getTextRange(editor.getSelectionRange())");
            if (selectedText == null || selectedText.isEmpty()) {
                return;
            }

            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final ClipboardContent content = new ClipboardContent();
            content.putString(selectedText);
            clipboard.setContent(content);
        } else if (event.getCode().equals(KeyCode.V) && event.isControlDown()) {
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            String content = (String) clipboard.getContent(DataFormat.PLAIN_TEXT);
            if (content != null) {
                engine.executeScript(String.format("editor.insert(\"%s\")", content));
            }
        }
    }

    public WebView getCodeWebView() {
        return codeWebView;
    }

    public Button getSaveBtn() {
        return saveBtn;
    }

    public Button getConsolleClearBtn() {
        return consolleClearBtn;
    }
}
