import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class Yolo {
    private List<Integer> ints = new ArrayList<>(Arrays.asList(1,2,3));
    public void keepImpair(){
        ints.removeIf(i -> i % 2 == 0);
    }

    public List<Integer> getInts() {
        return Collections.unmodifiableList(ints);
    }

    public static void main(String...args){
        Yolo yolo = new Yolo();
        yolo.keepImpair();
        System.out.println(yolo.getInts());
    }
}
